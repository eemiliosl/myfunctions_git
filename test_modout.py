#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      eemiliosl
#
# Created:     02.03.2016
# Copyright:   (c) eemiliosl 2016
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import os, time, pdb, numpy as np, datetime, shutil, subprocess as sp, platform
import dir_manip as dm, grid_manip as gm, gen_fields as gf, kalman as klm


def rmodout(myfile, mytimes, drawdown = True):
    """
    Support for the <getmodout> function (to extract model measurements)
        Arguments:
        ----------
    myfile:     str, fullpath with the name of the file to be loaded
    mytimes:    np array,  float or int array with times of interest
        Returns:
        --------
    mydata:     np.array, with model outputs at specific times (nmeas x 1)
    """
    try:
        #Observation point HGS format
        myTime, mymodmeas = np.loadtxt(myfile, skiprows = 2, usecols = (0,1), unpack =True)
    except:
        # Observation well HGS format:
        mymodmeas = []
        idx_line, myTime = dm.str_infile(myfile, 'SOLUTIONTIME=', index = True)
        with open(myfile, 'r') as f:
            mylines = f.readlines()

        for yy in range(0, len(idx_line), 1):
            if yy < len(idx_line)-1:
                myvalues = np.asarray(mylines[idx_line[yy]+1 : idx_line[yy+1]])
            elif yy == len(idx_line)-1:
                myvalues = np.asarray(mylines[idx_line[yy]+1 :])

            mymodmeas.append(mean([float(cur_line.split(' ')[2]) for cur_line in myvalues]))
##        for n in np.arange(0,len(myTime)):
##            # Fix this numbers, they should be automatically defined!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
##            allmeas = np.genfromtxt(myfile, dtype = 'float', skip_header = 3+n*14, skip_footer = (len(myTime)-1-n)*14, usecols=(0,))
##            mymodmeas.append(np.mean(allmeas))
        mymodmeas = np.asarray(mymodmeas)

    try: # If it is a single integer, add a dimension
        len(mytimes)
    except TypeError:
        mytimes = np.expand_dims(mytimes,1)

    # Mask to get only the elements of interest:
    if type(mymodmeas) != np.ndarray:
        mymodmeas = np.expand_dims(mymodmeas, axis = 0)

    mymodmeas = mymodmeas[np.where(np.in1d(myTime, mytimes))]
    if drawdown == True:
       mymodmeas = np.subtract(mymodmeas[0],mymodmeas)

    return mymodmeas

def getmodout(mypath, mode ='', outputtime = '', drawdown = True):
    """
    Read model output from a file at specific output times.
    Time values will be integers. Corrects negative concentrations when
    working in tranport mode.
        Arguments:
        ----------
    mypath:         str, directory where model outputs are located
    mode:           Str, 'fl_' or 'tr_'
    outputtime:     output times of interest
        Returns:
        --------
    all_modout:   2D np array, observed values at defined times
                   size of the array = [No times x No Obs Points]. Fortran order
    """
    #assert outputtime != '', 'No output times provided'
    if not mode:
        print('Model outputs not extracted. No running mode provided')
    elif mode == 'fl_':
        mystrs = '_flow'
    elif mode == 'tr_':
        mystrs = '_conc'

    # Get a list of the files of all observation points:
    files, dummydir = dm.getdirs(mypath, mystr = mystrs, fullpath = True, onlylast = False)

    # Start collecting them in a single 2D array
    all_modout = np.array([])

    for ii in files:
        selMeas = rmodout(ii, outputtime, drawdown = drawdown)
        #selMeas = rmodout(ii, outputtime.astype(np.int))
        all_modout = np.append(all_modout, selMeas) # All selected heads

    all_modout = np.reshape(all_modout, (-1, len(files)), order = 'F')

    # If transport mode, correct negative concentrations:
    if mode == 'tr_':
        # If the model gives negative concentrations, set all them to 0
        zero_id = np.where(all_modout < 0)
        all_modout[zero_id[0],zero_id[1]] = 0

    return all_modout


def main():
    mypath = r'C:\Users\eemiliosl\Documents\phd\_codedev\kalmanfilter_git\Reference_model\Flow'
    mymode = 'fl_'
    outtimes = np.loadtxt(r'C:\Users\eemiliosl\Documents\phd\_codedev\kalmanfilter_git\Model_data\_outputtimesfl_.dat')

    all_modout = getmodout(mypath, mode = mymode, outputtime = outtimes)

    print(all_modout.shape)


if __name__ == '__main__':
    main()
