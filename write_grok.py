""" Script to write GROK File with specific instructions """
import os, numpy as np
import sys
funcPath = os.path.dirname(__file__)
sys.path.append(os.path.abspath(os.path.join(funcPath,'..','myfunctions_git')))
import hgs_grok as grokhgs

def main():

    #%% MAIN DIRECTORIES:
    kalman_path = r'C:\PhD_Emilio\Emilio_PhD\_CodeDev_bitbucket\kalmanfilter_git'
    myInDir = os.path.join(kalman_path, 'Model_data')
    myOutDir = os.path.join(kalman_path, 'fl_ToCopy')
    #myOutDir = r'C:\PhD_Emilio\Emilio_PhD\_CodeDev_bitbucket\KalmanFilter\tr_ToCopy'

    myoutFile = 'fl_sim.grok'                    # Output GROK file name
                                                    # Transpsim.grok            Flowsim.grok
    probdescFile = '_problemDescfl_.dat'          # File containing description of the project
                                                    # _problemDescTrans.dat     _problemDescFlow.dat
    #%% GRID GENERATION:
    grid_coordFile = '_gridcoordinatesfl_.dat'    #   _gridcoordinatesFlow.dat        _gridcoordinatesTrans.dat
    gridmethod ='HGS_grade'                         # How to generate the grid? HGS_grade   HGS_uniform     gridbuilder

    #%% SIMULATION PARAMETERS:
    Transient = True
    Transport = False
    Echo2Output = False
    FD = False
    CV = False
    porousFile = 'lauswiesen.mprops'
    kFile = 'kkkGaus.dat'
    TimeStepCtrl = True

    #%% Initial and boundary conditions:
    heads_init = ['onevalue', 306.0]             # e.g ['txt','finalheadsFlow.dat'] or ['onevalue',number]
    transport_init = ['onevalue', 0]
    heads_bc = [0, 50]       # e.g [0,100]  X planes to apply head bc or ['all', 'head equals initial']

    #%% Pumping wells:
    wells = True

    #%% Output times:
    outputtimes = True

    #%% Observation points:
    observation_wells = True

#-------------------------------------------------------------------------------#
# Modify lines below only if you know what you are doing...

    # For wells:
    if wells == True:
        well_propsFile = 'well.wprops'
        pumpwellsFile = '_wellcoordinates.dat'
    if wells == False:
        well_propsFile = ''
        pumpwellsFile = ''

    # For output times:
    if outputtimes == True:
        if Transport == True:
            outputimesFile = '_outputtimestr_.dat'
        elif Transport == False:
            outputimesFile = '_outputtimesfl_.dat'
    elif outputtimes == False:
        outputimesFile = ''

    # For observation points:
    if observation_wells == True:
        obswellsFile ='_obswellscoord.dat'
    elif observation_wells == False:
        obswellsFile = ''

    # For tranport:
    if Transport == False:
        transport_init = ['']

    # Tracer injection:
    if Transport == True:
        tracer_injFile = '_tracerinjcoordinates.dat'
    elif Transport == False:
        tracer_injFile = ''

    try:
        grokhgs.writeinGrok(myInDir, myOutDir, myoutFile, probdescFile, grid_coordFile, gridmethod,
    Transient, Transport, Echo2Output, FD, CV, porousFile, kFile, heads_init, transport_init, heads_bc,
    TimeStepCtrl, well_propsFile, pumpwellsFile, tracer_injFile, outputimesFile, obswellsFile)

    except:
        raise

if __name__ == '__main__':
    main()
