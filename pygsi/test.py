# -*- coding: utf-8 -*-
#!/usr/bin/env python3
import pygsi.pygsiCaller as gsi
import numpy as np
import time
import dir_manip as dm
import matplotlib.pylab as plt
#import parmap as pm
import pdb

def main():

    # Just if rotation of control points is needed!!!
##    XY_well = np.loadtxt(r'C:\PhD_Emilio\Emilio_PhD\Field_Data\allwells_xy.txt', usecols = (1,2))
##    xyrot = gm.rotmesh2d(XY_well, myplot = True, forward = False, interactive = True, clockwise = False, rotate = True)
##    np.savetxt(r'C:\PhD_Emilio\Emilio_PhD\Field_Data\allwells_xyrot.txt', np.transpose((xyrot[:,0]+35., xyrot[:,1]+60.)), fmt = '%11.10e', header = 'wellid, X,Y', comments = '#')

    # Define some initial settings:
    mycontrolfile = r'C:\PhD_Emilio\Emilio_PhD\Field_Data\18112015_xyrot.txt'
    #gridfile = r'C:\PhD_Emilio\Emilio_PhD\_CodeDev_bitbucket\myfunctions_git\pygsi\grokgrid_2d\my_meshfile.txt'
    grideco_file = r'C:\PhD_Emilio\Emilio_PhD\_CodeDev_bitbucket\myfunctions_git\pygsi\grokgrid_2d\mygrido.eco'
    controls = np.loadtxt(mycontrolfile, usecols = (1,2))
    controlvalues = np.loadtxt(mycontrolfile, usecols = (-1,))

    # Node coordinates: grid
    idx, dummy =dm.str_infile(grideco_file, 'Node coordinates', index = True)
    nXnodes = int(dm.str_infile(grideco_file, 'NX'))
    nYnodes = int(dm.str_infile(grideco_file, 'NY'))
    mygrid = np.genfromtxt(grideco_file, skip_header = idx+1, max_rows = nXnodes*nYnodes, usecols = (1,2))

    controls = controls[:, np.newaxis, :]
    myrstfile = 'C:\PhD_Emilio\Emilio_PhD\Field_Data\myraster_18112015.asc'
    n_cond = 10


    # Variogram parameters:
    variogram_paras = [0.01,   # Nug
                       0.99,   # 1.0-Nug
                       70.00,   # range
                       0.5]   # Matern-Koeff.
    # Proper order for of the grid for raster creation
##    for i in [0,1]:
##            indices = np.argsort(mygrid[:,i]*-1, kind='mergsort', axis = -1)
##    mygrid = mygrid[indices]
    x0 = 0
    y0 = 0
##    x0 = 0
##    x1 = 5
##    y0 = 0
##    y1 = 5
##    nXnodes = 20
##    nYnodes = 20
##    # Create the grid nodes with proper shape:
##    x = np.linspace(x0, x1, nXnodes)
##    y = np.linspace(y0, y1, nYnodes)[::-1]
##    xv, yv = np.meshgrid(x,y)
##    mygrid = np.empty((len(x)*len(y),2))
##    mygrid[:,0] = xv.flatten('C')
##    mygrid[:,1] = yv.flatten('C')
##
##    # Get the control points (i.e. coordinates and actual value (e.g. measurements) )
##        # location of measurements
##    #controls = np.loadtxt(mycontrolfile, usecols = (0,1))
##    controls = np.array([[0., 0.],
##                         [5., 0.],
##                         [5., 5.],
##                         [0., 5.]])
##        # measurement values
##    #controlvalues = np.loadtxt(mycontrolfile, usecols = (-1,))
##    controlvalues = np.array([0., 3., 5., 3.])

    # Start working:
    init_time = time.clock()
    myrst = gsi.caller(mygrid, variogram_paras, controls, controlvalues, nXnodes, nYnodes, n_cond, parallel = True, ncpu = 5, verbose = True)

    tot_time = (time.clock()-init_time)
    print(tot_time/60.)
    plt.imshow(myrst.reshape((nXnodes,-1), order = 'F').T, extent = (0,100,0,140), vmin=np.min(myrst), vmax=np.max(myrst))
    plt.colorbar()
    plt.show()

    # Save Raster file:
    np.savetxt(myrstfile, myrst.T, fmt = '%9.8e', delimiter = ' ', header = 'ncols\t%s\nnrows\t%s\nxllcorner\t%s\nyllcorner\t%s\ncellsize\t%5.4f\nNODATA_value\t-9999\n'%(nXnodes, nYnodes,x0, y0, np.abs(mygrid[0,0]-mygrid[1,0])), comments = '')

if __name__ == '__main__':
    main()

