import numpy as np
import scipy.spatial
import block as block

"""
a python class for Kriging
  - OK: Ordinary Kriging
  - EDK: External Drift Kriging

Claus Haslauer and Thomas Pfaff
2015
claus.haslauer@uni-tuebingen.de
"""


def krigmatrix_ok(controls, variogram):
    """ Calculates the Kriging-Matrix for Ordinary Kriging.
    >>> vario = ExponentialVariogram()
    >>> vario.setParameters({'range':2,'sill':3,})
    >>> points = np.array([ [[1.,1.]],[[-1.,-1.]],[[0.5,-1.1]],[[-0.8,0.9]]])
    >>> krigmatrix_ok(points, vario)
    array([[ 0.        ,  2.2706498 ,  1.98055269,  1.78198258,  1.        ],
           [ 2.2706498 ,  0.        ,  1.58525759,  1.84585059,  1.        ],
           [ 1.98055269,  1.58525759,  0.        ,  2.08978437,  1.        ],
           [ 1.78198258,  1.84585059,  2.08978437,  0.        ,  1.        ],
           [ 1.        ,  1.        ,  1.        ,  1.        ,  0.        ]])
    """

    var_matrix = block.calcBlockVariogramMatrix(controls, variogram)

    # ok_matrix is the kriging matrix (which is being constructed here
    # add ones in the bottom row of the distance matrix
    horz_vec = [np.ones(len(controls))]
    glue_pt1 = np.vstack((var_matrix, horz_vec))
    # add on the right side a vector with an additional one
    vert_vec_a = np.transpose((horz_vec))
    vert_vec_b = np.vstack((vert_vec_a, [[0]]))
    ok_matrix = np.hstack((glue_pt1, vert_vec_b))

    return ok_matrix


def krigmatrix_edk(controls, control_extdrift, variogram):
    """ Calculates the Kriging-Matrix for External Drift Kriging.
    >>> vario = ExponentialVariogram()
    >>> vario.setParameters({'range':2,'sill':3,})
    >>> points = np.array([ [[1.,1.]],[[-1.,-1.]],[[0.5,-1.1]],[[-0.8,0.9]]])
    >>> krigmatrix_edk(points, [4., 3., 5., 3.], vario)
    array([[ 0.        ,  2.2706498 ,  1.98055269,  1.78198258,  1.        ,
             4.        ],
           [ 2.2706498 ,  0.        ,  1.58525759,  1.84585059,  1.        ,
             3.        ],
           [ 1.98055269,  1.58525759,  0.        ,  2.08978437,  1.        ,
             5.        ],
           [ 1.78198258,  1.84585059,  2.08978437,  0.        ,  1.        ,
             3.        ],
           [ 1.        ,  1.        ,  1.        ,  1.        ,  0.        ,
             0.        ],
           [ 4.        ,  3.        ,  5.        ,  3.        ,  0.        ,
             0.        ]])
    """
    # Zusammensetzen der EDK Matrix
    #  erster Baustein: OK Matrix
    #  daran unten den EDK Vektor mit einer Null ganz rechts bauen

    ok_matrix = krigmatrix_ok(controls, variogram)
    edk_a = np.hstack((control_extdrift.flatten(), [0]))
    edk_b = np.vstack((ok_matrix, edk_a))
    edk_c = np.hstack((edk_a, np.array([0.0])))
    edk_matrix = np.hstack((edk_b, edk_c[:, np.newaxis]))

    return edk_matrix


def krigrhs_ok(controls, target, variogram):
    """ Calculates the right-hand-side of the Kriging system for Ordinary Kriging.
    >>> vario = ExponentialVariogram()
    >>> vario.setParameters({'range':2,'sill':3,})
    >>> controls = np.array([ [[1.,1.]],[[-1.,-1.]],[[0.5,-1.1]],[[-0.8,0.9]]])
    >>> target = np.array([[0.,0.]])
    >>> krigrhs_ok(controls, target, vario)
    array([ 1.52079393,  1.52079393,  1.36038741,  1.35698567,  1.        ])
    """
    # TODO: DANGER !!!
    # print controls.shape, target.shape
    if controls.shape[0]<2:
        controls = controls[0,:,:,:]

    nc = len(controls)
    rhs = np.ones(nc+1)

    # TODO: the newaxis in target might not be ideal (and necessary to change if block kriging included)
    for i in range(nc):
        rhs[i] = block.calcMeanVariogram(variogram, controls[i], target[np.newaxis, :])

    return rhs


def krigrhs_edk(controls, target, target_extdrift, variogram):
    """ Calculates the right-hand-side of the Kriging system for External Drift Kriging.
    >>> vario = ExponentialVariogram()
    >>> vario.setParameters({'range':2,'sill':3,})
    >>> controls = np.array([ [[1.,1.]],[[-1.,-1.]],[[0.5,-1.1]],[[-0.8,0.9]]])
    >>> target = np.array([[0.,0.]])
    >>> krigrhs_edk(controls, target, 6., vario)
    array([ 1.52079393,  1.52079393,  1.36038741,  1.35698567,  1.        ,  6.        ])
    """
    if controls.shape[0]<2:
        controls = controls[0,:,:,:]
    rhs_ok = krigrhs_ok(controls, target, variogram)
    rhs = np.hstack((rhs_ok, target_extdrift[0]))

    return rhs

def krige_ok(target, controls, controlvalues, variogram, n, verbose):
    """ 'Organizer function' for Ordinary Kriging.
    This function sets up the Kriging system and solves it for each given target
    >>> controls = np.array([ [[1.,1.]],[[-1.,-1.]],[[0.5,-1.1]],[[-0.8,0.9]]])
    >>> targets = np.array([[[ 0., 0.]]])
    >>> controlvalues = np.array([ 3.1,  4.,   2.,   5. ])
    >>> vario = ExponentialVariogram()
    >>> vario.setParameters({'range':2,'sill':3,})
    >>> krige_ok(controls, targets, controlvalues, vario, verbose = True)
    ok matrix
    [[ 0.          2.2706498   1.98055269  1.78198258  1.        ]
     [ 2.2706498   0.          1.58525759  1.84585059  1.        ]
     [ 1.98055269  1.58525759  0.          2.08978437  1.        ]
     [ 1.78198258  1.84585059  2.08978437  0.          1.        ]
     [ 1.          1.          1.          1.          0.        ]]
    weights
    [ 0.22870749  0.18633342  0.29091373  0.29404537 -0.0024577 ]
    interpolation result
    3.50638117069

    """
    # preparation:
    #   include only the n closest points
    #   relative to point where estimation is carried out
    # print "start kdTree"
    k = min(n, len(controls))
    ctrlcentroids = block.calcCentroids(controls)           # centers (centroids) of the blocks
    controltree = scipy.spatial.cKDTree(ctrlcentroids)      # kdTree between all ctrlcentroids

    # up to here everything depends on measurements only (controls)
    # from here on: estimation for every individual point (target)
    # print "percent done: ",

    for cur_tar in target:
        # preparation for closest points
        qtree = controltree.query(cur_tar, k)     # find the k closest points in the controltree relative to the current target
        # tcontrols = controls[qtree[1][0],...]  # qtree[1][0] returns the indices of the k closest points in target
        tcontrols = controls[qtree[1]]           # tcontrols then contains the x,y coordinates of those points

        weight, interpol, estvar = solve_ok_equationSystem(tcontrols, controlvalues, cur_tar, variogram, qtree)

        yield weight, interpol, estvar
    del weight
    del interpol
    del estvar

##    elif parallel == True:
##        # preparation for closest points
##        qtree = controltree.query(target, k)     # find the k closest points in the controltree relative to the current target
##        # tcontrols = controls[qtree[1][0],...]  # qtree[1][0] returns the indices of the k closest points in target
##        tcontrols = controls[qtree[1]]           # tcontrols then contains the x,y coordinates of those points
##
##        weight, interpol, estvar = solve_ok_equationSystem(tcontrols, controlvalues, target, variogram, qtree)
##
##        return weight, interpol, estvar


def solve_ok_equationSystem(  tcontrols,
                              controlvalues,
                              target,
                              variogram,
                              qtree):
    # prepare kriging system
    ok_matrix = krigmatrix_ok(tcontrols, variogram)
    rhs = krigrhs_ok(tcontrols, target, variogram)

    # TODO clean this up!
    # solution for
    #    weights (lambdas),
    #    estimation (interpolated value)
    #    estimation variance at interpolated point (estvar)
    weight = np.linalg.solve(ok_matrix, rhs)

    # interpolated value = SUM(weight_i * measuredValue_i)
    # this is either
    #    np.sum(weight[:-1]*(controlvalues[qtree[1][0]].flatten()))
    # or
    # interpol = np.add.reduce(weight[:-1]*controlvalues[qtree[1][0]]).mean()
    interpol = (weight[:-1]*controlvalues[qtree[1]]).sum()

    # todo add term for block variance in estvar
    # estvar = np.add.reduce(weight[:-1]*rhs[:-1]) + weight[-1]
    # estvar = 1.0 -  np.sum(weight[:-1]*rhs[:-1]) + weight[-1]  # Eq. 12.15 in Isaaks
    # estvar = np.sum(weight[:-1]*rhs[:-1])  + weight[-1]  # Eq. 12.15 in Isaaks+


     # new attempt 20140724

    # die lambdas (weights ohne mus)
    lam = weight[:-1][np.newaxis, :]

    # variogram matrix der LHS
    var_lhs = ok_matrix[:-1, :-1]

    # variogram array der rhs
    var_rhs = rhs[:-1]

#     print "var_lhs: ", var_lhs
#     print "var_rhs: ", var_rhs

    # 1. summand der estimation variance
    # [n,n].sum() -- (n*n) Elemente
    part_1 = (lam * lam.T * var_lhs).sum()

    # 2. summand der estimation variance
    # [1,n].sum() -- n Elemente
    part_2 = (lam * var_rhs).sum()

    estvar = (-1.0 * part_1) + (2.0 * part_2)

    return weight, interpol, estvar


def krige_edk(controls,
              targets,
              controlvalues,
              control_extdrift,
              targets_extdrift,
              variogram,
              n,
              verbose=True,
              counters=[0, 0, 0]
              ):
    """ 'Organizer function' for External Drift Kriging.
    This function sets up the Kriging system and solves it for each given target
    >>> vario = ExponentialVariogram()
    >>> vario.setParameters({'range':2,'sill':3,})
    >>> controls = np.array([ [[1.,1.]],[[-1.,-1.]],[[0.5,-1.1]],[[-0.8,0.9]]])
    >>> targets = np.array([[[ 0., 0.]]])
    >>> controlvalues = np.array([ 3.1,  4.,   2.,   5. ])
    >>> control_extdrift = np.array([ 4.,  3.,  5.,  3.])
    >>> targets_extdrift = np.array([ 6.,  3.])
    >>> krige_edk(controls, targets, controlvalues, control_extdrift, targets_extdrift, vario, verbose = True)
    edk matrix
    [[ 0.          2.2706498   1.98055269  1.78198258  1.          4.        ]
     [ 2.2706498   0.          1.58525759  1.84585059  1.          3.        ]
     [ 1.98055269  1.58525759  0.          2.08978437  1.          5.        ]
     [ 1.78198258  1.84585059  2.08978437  0.          1.          3.        ]
     [ 1.          1.          1.          1.          0.          0.        ]
     [ 4.          3.          5.          3.          0.          0.        ]]
    weights
    [ 0.35579101 -0.5169096   1.3221045  -0.1609859  -5.43188571  1.4486942 ]
    interpolation result
    0.874593200643

    """
    # temporarily fixed number of nearest controls until the final structure
    # can be put into the api

    # KDTree returns weird data, if more points are requested than are available
    # therefore limit the number if there are not enough points
    k = min(n, len(controls))
    # calculate the centroids of the blocks (if there are any). This should
    # be robust, if only points are given (as long as the points are passed
    # in via a 3-D (nblocks x npoints x dimension) array and npoints==1).
    ctrlcentroids = block.calcCentroids(controls)
    # set up the KDTree using the centroids
    controltree = scipy.spatial.cKDTree(ctrlcentroids)

    # bis hierher haengt alles nur von messwerten (controls) ab
    # ab jetzt: fuer jeden interpolationspunkt (target) interpolieren

    for target, target_extdrift in zip(targets, targets_extdrift):
        # query the controls-tree for the k nearest points to the target
        qtree = controltree.query(target, k)

        # initialize temporary controls and external drift lists by
        # selecting the points returned by the tree query
        tcontrols = controls[qtree[1]]
        tcontrol_extdrift = control_extdrift[qtree[1]]

        # ----------------------------------------------------------------------
        # Check, if drift is
        # a) equal at all locations (no additional information)
        #    ==> OK
        # b) at measLoc outside range of drift at all other points (extrapolation)
        #    ==> OK
        # ----------------------------------------------------------------------

        if len(set(tcontrol_extdrift.flatten())) == 1:  # use Ordinary Kriging
            print("no ED")
            counters[0] += 1
            weight, interpol, estvar = solve_ok_equationSystem(tcontrols,
                                                              controlvalues,
                                                              target,
                                                              variogram,
                                                              qtree)

        elif np.logical_or((np.min(tcontrol_extdrift) > target_extdrift), (np.max(tcontrol_extdrift) < target_extdrift)):
            print("ED outside range")
            counters[1] += 1
            weight, interpol, estvar = solve_ok_equationSystem(tcontrols,
                                                              controlvalues,
                                                              target,
                                                              variogram,
                                                              qtree)

        else:
           counters[2] += 1
           weight, interpol, estvar = solve_edk_equationSystem(tcontrols,
                                                               tcontrol_extdrift,
                                                               controlvalues,
                                                               target,
                                                               target_extdrift,
                                                               variogram,
                                                               qtree)

        yield weight, interpol, estvar, counters

    del weight
    del interpol
    del qtree
    del tcontrols
    del tcontrol_extdrift


def solve_edk_equationSystem(tcontrols,
                             tcontrol_extdrift,
                             controlvalues,
                             target,
                             target_extdrift,
                             variogram,
                             qtree):

    edk_matrix = krigmatrix_edk(tcontrols, tcontrol_extdrift, variogram)
    rhs = krigrhs_edk(tcontrols, target, target_extdrift, variogram)

    try:
        weight = np.linalg.solve(edk_matrix, rhs)
    except:
        # TODO: FIX THIS!
        print('Crazy Kriging Matrix --> Check Out Why!!!')
        raise Exception
        # print 'multiplying edk_matrix'
        rd_mat = np.ones_like(edk_matrix)
        rds = np.random.random(tcontrol_extdrift.shape[0]+2) * 0.05 + 0.95
        rd_mat[:, -1] = rds
        rd_mat[-1, :] = rds
        edk_matrix = edk_matrix * rd_mat
        weight = np.linalg.solve(edk_matrix, rhs)

    # interpol = np.add.reduce(weight[:-2]*controlvalues[qtree[1][0]]).mean()
    interpol = (weight[:-2]*controlvalues[qtree[1]]).sum()

    # TODO dangerous! Clean this up! Only via similarity
    # estvar = np.sum(weight[:-1]*rhs[:-1])  + weight[-1]

    # new attempt 20140724

    # die lambdas (weights ohne mus)
    lam = weight[:-2][np.newaxis, :]

    # variogram matrix der LHS
    var_lhs = edk_matrix[:-2, :-2]

    # variogram array der rhs
    var_rhs = rhs[:-2]

    # 1. summand der estimation variance
    # [n,n].sum() -- (n*n) Elemente
    part_1 = (lam * lam.T * var_lhs).sum()

    # 2. summand der estimation variance
    # [1,n].sum() -- n Elemente
    part_2 = (lam * var_rhs).sum()

    estvar = (-1.0 * part_1) + (2 * part_2)

    return weight, interpol, estvar


def readVariogram(filename):
    """reads Variogram definitions and parameters from an ini-style configuration file
    """
    from configparser import SafeConfigParser
    import variogram

    parser = SafeConfigParser()
    parser.read(filename)

    vario = variogram.CompositeVariogram()

    for section in parser.sections():
        if section in variogram.__dict__:
            # the only way I found to create classes from their name
            variotmp = variogram.__dict__[section]()
            paramdict = {}

            for name, value in parser.items(section):
                paramdict[name]=float(value)

            variotmp.setParameters(paramdict)
            vario.addVariogram(variotmp)
        else:
            raise 'Variogram type not known: ' + section + \
                  '\nfirst, make sure that this isn''t due to a typo' + \
                  '\nthen check the available variogram types in variogram.py'

    return vario


def edk(  locmeasurements_xy
        , loctointerpolate_xy
        , measurements
        , exdriftmeasurements
        , exdriftatinterpolpts
        , compVar
        , weightfilename = 'Edk_weight_out.dat'
        , interpolfilename = 'Edk_out.dat'
        , estvarfilename = 'Edk_estvar_out.dat'
        , returnVals = False
        , n=10
        , counters=[0,0,0]):
    """ File I/O wrapper for External Drift Kriging.
    Reads files and prepares data for the External Drift Kriging organizer
    function
    """
    #------------------------------------------------------------
    #                                main routine for exdrkriging
    #------------------------------------------------------------ed
    # define names of input files
    #locmeasurements_xy      = "Edk_Input.dat"
    #loctointerpolate_xy     = "Target_Input.dat"
    #measurements            = "Measurements.dat"
    #exdriftmeasurements     = "Exdriftmeasurements.dat"
    #exdriftatinterpolpts    = "Exdriftatinterpolpts.dat"

    # read input files
    controls = locmeasurements_xy
    targets = loctointerpolate_xy
    controlvalues = measurements
    control_extdrift = exdriftmeasurements
    targets_extdrift = exdriftatinterpolpts

    variogram = compVar # readVariogram(variogramfile)

    # run external drift kriging

##    inputstring = '# input files:\n' \
##                   + '# controls:           ' + locmeasurements_xy  + '\n' \
##                   + '# targets:            ' + loctointerpolate_xy  + '\n' \
##                   + '# control values:     ' + measurements  + '\n' \
##                   + '# control ext drifts: ' + exdriftmeasurements  + '\n' \
##                   + '# target ext drifts:  ' + exdriftatinterpolpts  + '\n'


    # open output file
    interpolfile = open(interpolfilename, 'w')
    interpolfile.write( '# interpolation result file for external drift kriging\n')

    weightfile = open(weightfilename, 'w')
    weightfile.write( '# interpolation weights file for external drift kriging\n')

    estvarfile = open(estvarfilename, 'w')
    estvarfile.write( '# estimation variance file for ordinary kriging\n')
    for weights, interpol, estvar, counters in krige_edk(          controls,
                                                         targets,
                                                         controlvalues,
                                                         control_extdrift,
                                                         targets_extdrift,
                                                         variogram,
                                                         n,
                                                         verbose=False,
                                                         counters=counters):
        interpolfile.write('%6.3f\n' % (interpol))
        weightfile.write('\t'.join(['%6.4f' %(item) for item in weights]) + '\n')
        estvarfile.write('%10.5g \n' % (estvar))


    interpolfile.close()
    weightfile.close()
    estvarfile.close()

    if returnVals == True:
        return  weights, interpol, estvar, counters
    else:
        pass

    print("done")
    del weights
    del interpol


def ok(locmeasurements_xy, loctointerpolate_xy, measurements, variogram
       , n
       , weightfilename = 'Ok_weight_out.dat'
       , interpolfilename = 'Ok_out.dat'
       , estvarfilename = "Ok_estvar_out.dat"
       , returnVals = False):
    """ File I/O wrapper for Ordinary Kriging.
    Reads files and prepares data for the Ordinary Kriging organizer function
    """
    #------------------------------------------------------------
    #                                         main routine for OK
    #------------------------------------------------------------
    #print "Executing Ordinary Kriging Program \n"
    # define names of input files
    #locmeasurements_xy      = "Ok_Input.dat"
    #loctointerpolate_xy     = "Target_Input.dat"
    #measurements            = "Measurements.dat"

    # read input files
    #print "... reading input files"
    controls = locmeasurements_xy # readBlockCoordinates(locmeasurements_xy)
    targets = loctointerpolate_xy #readBlockCoordinates(loctointerpolate_xy)
    controlvalues = measurements #read_file(measurements)

    variogram = variogram #readVariogram(variogramfile)

    #print "executing kriging"

##    inputstring = '# input files:\n' \
##                   + '# controls:           ' + locmeasurements_xy  + '\n' \
##                   + '# targets:            ' + loctointerpolate_xy  + '\n' \
##                   + '# control values:     ' + measurements  + '\n'

    # open output file
    interpolfile = open(interpolfilename, 'w')
    interpolfile.write( '# interpolation result file for ordinary kriging\n')
    #interpolfile.write( inputstring )

    weightfile = open(weightfilename, 'w')
    weightfile.write( '# interpolation weights file for ordinary kriging\n')
    #weightfile.write( inputstring )

    estvarfile = open(estvarfilename, 'w')
    estvarfile.write( '# estimation variance file for ordinary kriging\n')
    #estvarfile.write( inputstring )

    for weights, interpol, estvar in krige_ok(controls, targets, controlvalues
                                      , variogram, n, verbose=False):
        #if returnVals == False:
        interpolfile.write('%10.5g \n' % (interpol))
        weightfile.write('\t'.join(['%6.6f' %(item) for item in weights]) + '\n')
        estvarfile.write('%10.5g \n' % (estvar))

    interpolfile.close()
    weightfile.close()
    estvarfile.close()

    if returnVals == True:
        return weights, interpol, estvar
    else:
        pass
    print("done")


def read_file(filename):
    """ Simple file input function.
    Reads lines from a file, separates columns by whitespace and returns
    the results as a one- or two-dimensional numpy ndarray.
    """
    f = open(filename, 'r')
    outarray = []
    for line in f.readlines():
        split = line.strip().split()
        outarray.append([ float(item) for item in split ])
    f.close()
    return np.array(outarray).squeeze()


def _test():
    import doctest
    doctest.testmod()


if __name__ == "__main__":
    print('this is a pure module file')
    print('running doctests on module')
    _test()
    print('doctests finished')
