# -*- coding: utf-8 -*-
#!/usr/bin/env python3
#from functools import partial #import parmap as pm
from itertools import repeat
import multiprocessing as mp
import matplotlib.pylab as plt
import os, datetime, numpy as np#, pdb
import kriging as kri
import variogram as vario
"""
This is an example script to demonstrate the use of
    - ordinary Kriging (OK)
Claus Haslauer (modified by Emilio Sanchez, January 2016)
claus.haslauer@uni-tuebingen.de
2015
"""
def caller(targets, variogram_paras, controls, controlvalues, nXnodes, nYnodes, n_cond, parallel = False, ncpu = '', verbose = True):
    # Operate in sequence
    if parallel is False:
        myinterp = mykrig(targets, variogram_paras, controls, controlvalues, n_cond,verbose = verbose)

    # Operate in parallel
    if parallel == True:
        mypool = mp.Pool(ncpu)
        myinterp = mypool.starmap(mykrig, zip(targets, repeat(variogram_paras), repeat(controls), repeat(controlvalues), repeat(n_cond)))
        myinterp = np.asarray(myinterp).squeeze()

    #myrst = np.reshape(np.asarray(myinterp)[:,0], (nXnodes, nYnodes), order = 'C').round(decimals = 6)
    myrst = myinterp[:,0]
    return myrst

def mykrig(targets, variogram_paras,controls, controlvalues, n_cond, verbose = True, talk_to_me = True):
    #pdb.set_trace()
    try:
        targets.shape[1]
    except:
        targets = targets[np.newaxis,:]
#    targets = np.array([x,y], dtype= np.float)
#    print(type(targets), len(targets), targets, targets[0])
    # --------------------------------------------------------------------------------
    #                                       INPUT
    # --------------------------------------------------------------------------------
    # number conditioning points
    #n_cond = 4
    #+print (a)
    # verbose output
    # True or False
# --------------------------------------------------------------------------------
#                              preparation
# --------------------------------------------------------------------------------
    plt.ioff()
    print("===== START ===== \n")
    var_sample = np.var(controlvalues)
    ok_storage_z_means = []
    ok_storage_z_vars = []

    # Variogram Parameters
    nug = vario.NuggetVariogram({'sill': variogram_paras[0],
                                 'variance': var_sample})
    mat = vario.MaternVariogram({'sill': variogram_paras[1],
                                 'range': variogram_paras[2],
                                 'shape': variogram_paras[3],
                                 'variance': var_sample})
    fitted_variogram = vario.CompositeVariogram()
    fitted_variogram.addVariogram(nug)
    fitted_variogram.addVariogram(mat)
# --------------------------------------------------------------------------------
#  OK
# --------------------------------------------------------------------------------
    print("--- doing OK ----")
    ok_results = []
    start = datetime.datetime.now()

    # here's the meat
##    if parallel == True:
##        mypool = mp.Pool(2)
##        ok_result = mypool.starmap(kri.krige_ok, zip(targets, repeat(controls), repeat(controlvalues), repeat(fitted_variogram), repeat(n_cond), repeat(verbose), repeat(parallel)))
##        print('Interpolation performed in parallel!')
##    elif parallel == False:

    ok_result = kri.krige_ok( targets
                            , controls
                            , controlvalues
                            , fitted_variogram
                            , n_cond
                            , verbose
                            )
    # the results
    #print ('ID \t x \t\t y \t\t est(z)')
    for i, result in enumerate(ok_result):
##            print('{0:2d}\t{1:4.3f}\t{2:4.3f}\t{3:4.3f}'.format(i, targets[i][0], targets[i][1], result[1]))
            # the weights are stored in results[0] but not saved here
            ok_results.append([result[1], result[2]])

    end = datetime.datetime.now()
    ok_res = np.array(ok_results)
    ok_means = ok_res[:, 0]
    ok_vars = ok_res[:, 1]

    if talk_to_me is True:
        print("\nOK took: ", end-start)
        print("n OK results:", len(ok_results))
        print("mean ok res: {0:3.2f}".format(ok_means.mean()))
        print("mean ok var: {0:3.2f} ".format(ok_vars.mean()))

    ok_storage_z_means.append(ok_means)
    ok_storage_z_vars.append(ok_vars)

    print("\n=== END ===")
    #return ok_storage_z_means, ok_storage_z_vars, ok_results
    return ok_results

#if __name__ == '__main__':
#    main()
