# import numpy as np, os
# import hgs_grok as hg
# import grid_manip as gm
# import gen_fields as gf
#
# xlen, ylen, zlen, nxel, nyel, nzel, xlim, ylim, zlim, Y_model, Y_var, beta_Y, lambda_x, lambda_y, lambda_z = gf.readParam(r'C:\PhD_Emilio\Emilio_PhD\_CodeDev_bitbucket\kalmanfilter_git\Model_data\_RandFieldsParameters.dat')
#
# small_grid_nodes, small_grid_elem = gm.sel_grid(r'C:\PhD_Emilio\Emilio_PhD\_CodeDev_bitbucket\kalmanfilter_git\fl_ToCopy\meshtecplot.dat', xlim, ylim, zlim)
#
# mymeas = hg.getstates(r'C:\PhD_Emilio\Emilio_PhD\_CodeDev_bitbucket\kalmanfilter_git', 4, 'fl_', 36057, 30000, np.arange(1,30001,1), elem_ind_fromorig, inner_gr_nodes = new_nodes, Y_i_upd = '', genfield = True, plot = True)
#
# modout_tr = getmodout('C:\PhD_Emilio\Emilio_PhD\_CodeDev_bitbucket\kalmanfilter_git\Reference_model\Transport', mode ='tr_', outputtime = np.array([]))

import pandas as pd
import time

mytable = pd.read_excel(r'C:\PhD_Emilio\Emilio_PhD\Field_Data\TracerSampling_042016.xls', sheetname='XX.XX.XX', skiprows = 6)

mytime_computer = '%s:%s:%s' % (time.localtime().tm_hour, time.localtime().tm_min, time.localtime().tm_sec)

print('\a')

import winsound
Freq = 1500 # Set Frequency To 2500 Hertz
Dur = 1000 # Set Duration To 1000 ms == 1 second
winsound.Beep(Freq,Dur)

print('\a')