""" Functions to evaluate Statistics of a data set and transform data """
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as sst
import scipy.interpolate as si
import time
import scipy


def absolute_error(x, x_true):
    """ Compute the absolute error between two arrays.
        Author: Jeremiah Lant,      Email: jlant@usgs.gov
    Arguments:
        x:       array of data
        x_true : array of true data
    Return:
        error : array of error

    Example:
    >>> import numpy as np
    >>> model_data = np.array([55.5, 62.1, 65.3, 64.4, 61.2])
    >>> observed_data = np.array([55.7, 62.0, 65.5, 64.7, 61.1])
    >>> absolute_error(x = model_data, x_true = observed_data)
    array([-0.2,  0.1, -0.2, -0.3,  0.1])
    """
    # compute error
    error = x - x_true
    return np.abs(error)


# noinspection PyTypeChecker
def mean_squared_error(x, x_true):
    """ Compute the mean square error between two arrays
        Author: Jeremiah Lant,      Email: jlant@usgs.gov
    Arguments:
        x : array of data
        x_true : array of true data
    Return:
        mse : value of mean square error

    Example:
    >>> import numpy as np
    >>> model_data = np.array([55.5, 62.1, 65.3, 64.4, 61.2])
    >>> observed_data = np.array([55.7, 62.0, 65.5, 64.7, 61.1])
    >>> mean_squared_error(x = model_data, x_true = observed_data)
    0.038000000000000228
    """
    # compute error
    error = x - x_true
    mse = np.mean(error ** 2)
    return mse


def relative_error(x, x_true):
    """ Compute the relative change between two arrays
        Author: Jeremiah Lant,      Email: jlant@usgs.gov
    Arguments:
        x : array of data
        x_true : array of true data
    Return:
        error : array of relative change

    Example:
    >>> import numpy as np
    >>> model_data = np.array([55.5, 62.1, 65.3, 64.4, 61.2])
    >>> observed_data = np.array([55.7, 62.0, 65.5, 64.7, 61.1])
    >>> relative_error(x = model_data, x_true = observed_data)
    array([-0.00359066,  0.0016129 , -0.00305344, -0.00463679,  0.00163666])
    """
    # compute relative error
    error = (x - x_true) / x_true
    return error


def percent_error(x, x_true):
    """ Compute the percent error between two arrays
        Author: Jeremiah Lant,      Email: jlant@usgs.gov
    Arguments:
        x : array of data
        x_true : array of true data
    Return:
        error : array of error
    Example:
    >>> import numpy as np
    >>> model_data = np.array([55.5, 62.1, 65.3, 64.4, 61.2])
    >>> observed_data = np.array([55.7, 62.0, 65.5, 64.7, 61.1])
    >>> percent_error(x = model_data, x_true = observed_data)
    array([-0.35906643,  0.16129032, -0.30534351, -0.46367852,  0.16366612])
    """
    # compute percent error
    error = relative_error(x, x_true) * 100
    return error


def percent_difference(x, x_true):
    """ Compute the percent difference between two arrays
        Author: Jeremiah Lant,      Email: jlant@usgs.gov
    Arguments:
        x : array of data
        x_true : array of true data
    Return:
        percent_diff : array of error
    Example:
    >>> import numpy as np
    >>> model_data = np.array([55.5, 62.1, 65.3, 64.4, 61.2])
    >>> observed_data = np.array([55.7, 62.0, 65.5, 64.7, 61.1])
    >>> percent_difference(x = model_data, x_true = observed_data)
    array([-0.35971223,  0.16116035, -0.3058104 , -0.464756  ,  0.1635323 ])
    """
    # compute percent difference
    avg = np.average((x, x_true), axis=0)
    percent_diff = ((x - x_true) / avg) * 100
    return percent_diff


def r_squared(modeled, observed):
    """ Compute the Coefficient of Determination. Used to indicate how well
        data points fit a line or curve. Use numpy.coeff for computation.
        Author: Jeremiah Lant,      Email: jlant@usgs.gov
    Arguments:
        modeled : array of modeled values
        observed : array of observed value
    Return:
        coefficient : model efficiency coefficient
    *Example:*
    >>> import numpy as np
    >>> model_data = np.array([55.5, 62.1, 65.3, 64.4, 61.2])
    >>> observed_data = np.array([55.7, 62.0, 65.5, 64.7, 61.1])
    >>> r_squared(modeled = model_data, observed = observed_data)
    0.99768587638100936
    """
    r = np.corrcoef(modeled, observed)[0, 1]
    coefficient = r ** 2
    return coefficient


def nash_sutcliffe(modeled, observed):
    """ Compute the Nash-Sutcliffe (model efficiency coefficient). Used to
        assess the predictive power of hydrological models.
        E = 1 - sum((observed - modeled) ** 2)) / (sum((observed - mean_observed)**2 )))
        Author: Jeremiah Lant,      Email: jlant@usgs.gov
    Arguments:
        observed:   array of observed (field) values
        modeled:    array of modeled values
    Return:
        coefficient : model efficiency coefficient

    Example:
    >>> import numpy as np
    >>> model_data = np.array([55.5, 62.1, 65.3, 64.4, 61.2])
    >>> observed_data = np.array([55.7, 62.0, 65.5, 64.7, 61.1])
    >>> nash_sutcliffe(modeled = model_data, observed = observed_data)
    0.99682486631016043
    """

    # compute mean value of the observed array
    mean_observed = np.median(observed)

    # compute numerator and denominator
    numerator = sum((observed - modeled) ** 2)
    denominator = sum((observed - mean_observed) ** 2)

    # compute coefficient
    coefficient = 1 - (numerator / denominator)

    return coefficient


def one_d_stats(a):
    """ Purpose: Define 1D statistics of the dataset. Taken from Claus Haslauer 2014.
    Arguments:
        a: Float array, numpy array containing the dataset
     Returns:
        oned_stats: List, containing [nPts, mean, var, stdd, skew,
        kurt, cv, mi, ma, median]  """
    nPts = a.shape[0]
    mean = np.mean(a)
    var = np.var(a)
    stdd = np.sqrt(np.var(a))
    skew = sst.skew(a)
    kurt = sst.kurtosis(a)
    cv = stdd / mean
    mi = a.min()
    ma = a.max()
    median = np.median(a)

    oned_stats = [nPts, mean, var, stdd, skew, kurt, cv, mi, ma, median]

    return oned_stats


def print_one_d_stats(oned_stats):
    """ Purpose: Print 1D statistics of the dataset. Taken from Claus Haslauer 2014.
    Arguments:
        oned_stats:     list, list of the stats estimated with function < one_d_stats >
    Returns:
        No value returned. Prints results in a screen"""

    print('nPts: \t   %i' % oned_stats[0])
    print('mean: \t %11.9e' % oned_stats[1])
    print('var: \t %11.9e' % oned_stats[2])
    print('stdD: \t %11.9e' % oned_stats[3])
    print('skew: \t %11.9e (gauss=0)' % oned_stats[4])
    print('curt: \t %11.9e' % oned_stats[5])
    print('CV: \t %11.9e' % oned_stats[6])
    print('min: \t %11.9e' % oned_stats[7])
    print('max: \t %11.9e' % oned_stats[8])
    print('median:  %11.9e' % oned_stats[9])


def fitDistMoments(a, moments):
    """ Purpose: Fit a normal and exponential distribution to the data, using
    the method of moments (i.e. using mean and stddev from dataset. Taken from
    Claus Haslauer 2014.
    Arguments:
        a: Float, array containing the dataset
        moments: Tuple, mean (first) and standard deviation (sencond) of the dataset
    Returns:
        norm_pdf: array, probability values for a normal distribution
        exp_pdf: array, probability values for an exponential distribution
    """
    xe = np.linspace(np.min(a), np.max(a), 500)
    norm_pdf = sst.norm.pdf(xe, loc=moments[0], scale=moments[1])
    exp_pdf = sst.expon.pdf(xe, scale=moments[0])

    return norm_pdf, exp_pdf, xe


def Pearson(X, Y):
    """ Purpose: Compute Pearson correlation coefficient.
    Arguments:
        X: Float, array containing X data
        Y: Float, array containing Y data
    Returns:
        Pears: Pearson?s correlation coefficient
    """
    Pears = sst.pearsonr(X, Y)

    return Pears


def poly_fit(Xdata, Ydata, orderPol=1, Plot=False):
    """ Purpose: Perform a linear regression using polyfit (with Least squares).
    Arguments:
        Xdata:      float, array containing X data
        Ydata:      float, array containing Y data
        orderPol:   int, degree of the polynomial function to fit
        Plot:       bool, plot results or not
    Returns:
        pearson_coef: Pearsons correlation coefficient
    """
    Fit_line = np.polyfit(Xdata, Ydata, orderPol)
    p = np.poly1d(Fit_line)
    pearson_coef = Pearson(Xdata, Ydata)

    # line_XY45 = np.arange(np.min(Xdata)-0.1,np.max(Xdata)+0.1,0.001)
    line_XY45 = np.linspace(np.min(Xdata) - 0.1, np.max(Xdata), 1000)

    if Plot is True:
        plt.plot(Xdata, Ydata, 'wo', markersize=4)
        # plt.plot(line_XY45,line_XY45,'b-',linewidth = 2, label = 'perfect fit line')
        # plt.plot(Xdata,Xdata*Fit_line[0]+Fit_line[1],'k-',alpha = 1, label = 'regression line')
        plt.plot(line_XY45, p(line_XY45), 'k-', alpha=1, label='regression line')
        plt.grid()
        plt.xlim([np.min(Xdata) - 0.01, np.max(Xdata) + 0.01])
        plt.ylim([np.min(Xdata) - 0.01, np.max(Xdata) + 0.01])
        plt.ylabel('Modeled heads [m]')
        plt.xlabel('Field heads[m]')
        # plt.legend(numpoints=1)
        plt.show()
        plt.close()

        print("Correlation factor (Pearson's): %5.4e" % (pearson_coef[0]))
        # print Fit_line
    return pearson_coef[0]


# noinspection PyTypeChecker,PyTypeChecker,PyTypeChecker
def GausAnam(data, jj, myplot=True, oneDstats=True):
    """
    Apply Gaussian Anamorphosis (or Rank transformation) to a given
    dataset. Mean = 0, Std = 1.
        Arguments:
    data:       Array_like, array to transform. Dim = (NoMeas x 1)
    jj:         Str or Int, name or number of the dataset (just for plotting)
    myplot:     Boolean, plot or not the histograms, anamorphosis function,
                and probability plot of original and transformed data
    oneDstats:  Boolean, estimate 1d - statistics of original and transformed
                data. If true, also a fitting to the exponential and normal
                distribution is plotted using the method of moments.
        Returns:
    transfdata[backsortid_data]:    np.array, transformed data in the proper order
    Transform_function:             scipy.interpolate object, Anamorphosis function
        Example:
    >>> import numpy as np
    >>> ydata = np.load(r'..\ModMeas_tr_iter001_timestep002.npy')
    >>> jj = 13
    >>> data = mydata[jj, :]
    >>> transfdata, Transform_function = mysst.GausAnam(data, jj, myplot=True, oneDstats=True)
    """
    #    scipy.random.seed()
    # Construction of the empirical anamorphosis function:
    # ----------------------------------------------------

    # 1.) Calculation of ranks:
    # Sort and back-sort ids for the original data:
    sortid_data = np.argsort(data)
    backsortid_data = np.argsort(sortid_data)
    # data = data[sortid_data][backsortid_data]   # data back sorted, just as a test...

    # Ranks for the data:
    quantilerange = 1.0 / len(data)  # Range of a quantile is 1 point value
    ranks_norm_data = np.arange(quantilerange, 1.0 + quantilerange, quantilerange)
    stdranks_data = ranks_norm_data - (0.5 * quantilerange)

    # Get the Gaussian distribution values:
    Gaus = np.random.standard_normal(size=(len(data)) * 1000)  # (mean=0, stdev=1)
    sortid_Gaus = np.argsort(Gaus)
    # backsortid_Gaus = np.argsort(sortid_Gaus)
    Gaus = Gaus[sortid_Gaus]

    #  Ranks for Gaussian Distribution:
    quantilerange_Gauss = 1.0 / len(Gaus)
    ranks_norm_Gauss = np.arange(quantilerange_Gauss, 1.0 + quantilerange_Gauss, quantilerange_Gauss)
    stdranks_gaus = ranks_norm_Gauss - (0.5 * quantilerange_Gauss)

    # 2.) Add 0.9*min(data) and 1.1*max(data) and their corresponding 0 and 1 rank values
    #   data = np.append(1.1 * np.min(data), data)
    #   data = np.append(data, 0.9 * np.max(data) )

    #   stdranks_data = np.append(0.0, stdranks_data)
    #   stdranks_data = np.append(stdranks_data, 1.)

    stdranks_gaus = np.append(0.0, stdranks_gaus)
    stdranks_gaus = np.append(stdranks_gaus, 1.0)

    Gaus = np.append(0.9 * np.min(Gaus), Gaus)  # np.append(-4.0,Gaus)
    Gaus = np.append(Gaus, 1.1 * np.max(Gaus))  # np.append(Gaus,4.0)#

    # 3.) Interpolation of the empirical anamorphosis function:
    # ------------------------------------------------------
    InterpGaus = si.interp1d(stdranks_gaus, Gaus)
    transfdata = InterpGaus(stdranks_data)
    #   transfdata = transfdata[1:-1]

    # 4.) Get the Anamorphosis function, able to extrapolate:
    # up to here everything is calculated over sorted arrays...
    Transform_function = si.InterpolatedUnivariateSpline(data[sortid_data], transfdata, k=1)

    # 5.) Additional options:
    if oneDstats is True:
        stats_data = one_d_stats(data)
        print('\nStatistics for original data:')
        print_one_d_stats(stats_data)

        stats_transfdata = one_d_stats(transfdata)
        print('\nStatistics of transformed data:')
        print_one_d_stats(stats_transfdata)

        moments = [stats_data[1], stats_data[3]]
        norm_pdf, exp_pdf, xe = fitDistMoments(data, moments)

    if myplot is True:
        fig = plt.figure(1)
        ax1 = fig.add_subplot(221)
        ax1.hist(data, bins=11)
        ax1.set_title('Raw data: Realization %s' % jj)
        plt.grid(True)

        ax2 = fig.add_subplot(222)
        ax2.plot(data, Transform_function(data), 'ro')
        ax2.set_title('Anamorphosis Function')
        #            ax2.plot( data[1:-1],Gaus[1:-1],'k.')
        ax2.set_xlabel('Raw data')
        ax2.set_ylabel('Transformed data')
        plt.grid(True)

        ax3 = fig.add_subplot(223)
        ax3.hist(transfdata, bins=11, histtype='step')
        ax3.set_title('Transformed data')
        plt.grid(True)

        ax4 = fig.add_subplot(224)
        sst.probplot(transfdata, plot=plt)
        ax4.set_title('Probability plot')
        plt.grid(True)

        # Plot the fitted distributions:
        fig2 = plt.figure(2)
        ax11 = fig2.add_subplot(111)
        ax11.hist(data, normed=True, bins=11)
        # ax1.hist(data, bins=11)
        ax11.set_title('Fit a normal and exp distribution')
        ax11.plot(xe, norm_pdf, '.-', c='red', label='normal')
        ax11.plot(xe, exp_pdf, '.-', c='black', label='exp')
        plt.grid(True)
        plt.legend(loc='best')

        plt.show(2)
        plt.show(1)
        plt.close(1)
        plt.close(2)

    return transfdata[backsortid_data], Transform_function


def plotCopulas():
    print('under construction')
