﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

::em1::eduardoemilio.sanchez-leon@student.uni-tuebingen.de
::em2::murphy18061984@gmail.com
::cdkalman::C:\PhD_Emilio\Emilio_PhD\_CodeDev_bitbucket\kalmanfilter_git
::cdfunc::C:\PhD_Emilio\Emilio_PhD\_CodeDev_bitbucket\myfunctions_git