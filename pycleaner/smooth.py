#-------------------------------------------------------------------------------
# Name:        smooth
# Purpose:     smooth time series and signal dataseries using filters and moving windows
# Author:      eesl
# Created:     02-07-2015
# Copyright:   (c) IRTG-eesl 2015
#-------------------------------------------------------------------------------
import numpy as np, matplotlib.pyplot as plt
import scipy.stats as sst
import scipy, pdb
import scipy.signal as ssg
from .myplot import xyplot
from . import menu

class filters(object):
    """ Class with several methods to smooth signal using filters
    Arguments:
        mytime:                 numpy array with x-axis values of the data series
        myData:                 numpy array with the y-axis values of the data series
    """
    def __init__(self, mytime, yData):
        self.mytime = mytime
        self.yData = yData

    def kalman(self, meas_std_dev = 0.1, out_spread = False, mytitles = '', plotting  = False):
        """ Smooth a data series using the kalman filter. Function do not support
        different measurement errors for the same dataset
        Arguments:
            meas_std_dev:         Float, value of the measurement standard deviation
            out_spread(opt.):     Boolean, True: return also stdev of the data from the mean
            mytitle(opt.):        Str, name of the dataset (for plotting options)
            plotting(opt.):       Boolean, plot results or not
        Returns:
            mytime:               1-D numpy array with the x values
            yData_mod:            1-D numpy array with the smoothed dataset
            dataspread:           1-D numpy array, stdev of each point (around mean). Is None if
                                  out_spread == False
        """
        if mytitles != '':
            mytitles = '%s\n%s. Std dev = %s ' %(mytitles,'Kalman Filter', meas_std_dev)
        #%% Process variance, if the same model error is to be applied, no need for a full vector:
        n_iter = len(self.yData)
        sz = (n_iter,) # size of array
        Q = np.ones(n_iter)*.001
        #Q = 0.1

        #%% allocate space for arrays
        xhat=np.zeros(sz)      # a posteri estimate of x
        P=np.ones(n_iter) *10.        # a posteri error estimate
        xhatminus=np.zeros(sz) # a priori estimate of x
        Pminus=np.zeros(sz)    # a priori error estimate
        K=np.zeros(sz)         # gain or blending factor

        #%% Estimate of measurement variance (stdev**2), if the same value is to be applied, no need for a vector
        R = meas_std_dev**2
        # intial guesses
        xhat[0] = self.yData[0]
        #P[0] = 0
        error = np.zeros(sz)

        for k in range(1,n_iter):
            # time update
            xhatminus[k] = xhat[k-1]
            Pminus[k] = P[k-1] + Q[k-1]

            # measurement update
            K[k] = Pminus[k] / ( Pminus[k] + R )
            xhat[k] = xhatminus[k] + K[k] * ( self.yData[k]  - xhatminus[k] )
            P[k] = (1 - K[k]) * Pminus[k]
            error[k] = np.abs(self.yData[k] - xhat[k])

        spread = np.std(error, ddof = 500*.95)
        # Correct the offset:
        offset = np.floor(-1/np.log(1-np.mean(K)))
        mynewtime = self.mytime[0:-1-offset]
        xhat = xhat[offset:-1]
        #pdb.set_trace()
        if plotting == True:
            xyplot(self.mytime, self.yData, xData_new = mynewtime, yData_new = xhat, mytitle = mytitles)
        #pdb.set_trace()
        if out_spread == False:
            return mynewtime, xhat, None
        elif out_spread == True:
            return mynewtime, xhat, spread

    def butter(self, n =0.3, dim = 2, mytitles = '', btype = 'low', plotting  = False):
        """ Butter filter from Scipy. Low-pass filters remove short-term fluctuations,
        and leave the longer-term trend, pass signals with a frequency lower than a
        certain cutoff frequency and attenuates signals with frequencies higher than the cutoff.
        Arguments:
            n:          float, scalar (or array) giving the critical frequencies. Default is 0.3
            dim:        int, order of the filter. Default is 2
            btype:      str, the type of filter. {?lowpass?, ?highpass?, ?bandpass?, ?bandstop?}
                        Default is lowpass
            mytitle(opt.):        Str, name of the dataset (for plotting options)
            plotting(opt.):       Boolean, plot results or not
        Return:
            mytime:               1-D numpy array with the x values
            yData_mod:            1-D numpy array with the smoothed dataset
        """
        if mytitles != '':
            mytitles = '%s\n%s. Critical Freq=%s Hz, Dim=%s, Type=%s' %(mytitles,'Butterworth Filter', n, dim, btype)
        #%% Change from time-domain to frequency-domain:
        X_mags = np.abs(np.fft.fft(self.yData))      # change to frequency domain
        abs_Xmags = X_mags/len(X_mags)/2        # Absolute values of magnitudes

        # Plot the Magnitude in frequency domain:
    ##    if plotting == True:
    ##        plt.figure(2)
    ##        plt.plot(X_mags, label = 'Double-sided Magnitude Spectrum')
    ##        plt.title('Magnitude Spectrum')
    ##        plt.xlabel('Frequency (DFT Bins)') # To change to Hz: we use half of the sample frequency
    ##        plt.ylabel('Magnitude')
    ##        plt.grid(True)

        num_bins = len(X_mags)
        #pdb.set_trace()
        # Plotting just half frequencies
        if plotting == True:
            plt.figure(2)
            try:
                plt.plot(np.arange(0,num_bins/2,1), X_mags[0:num_bins/2], 'g-', label = 'Single-sided Magnitude Spectrum')
            except:
                plt.plot(np.arange(0,np.floor(num_bins/2),1), X_mags[0:num_bins/2], 'g-', label = 'Single-sided Magnitude Spectrum')
            plt.xlabel('Frequency (DFT Bins)') # To change to Hz: we use half of the sample frequency
            plt.ylabel('Magnitude')
            plt.legend()

        b,a= ssg.butter(dim,n,btype)
        #b,a= ssg.butter(1,[0.02,0.40],btype = 'bandstop')
        w, H = ssg.freqz(b,a, int(np.floor(num_bins/2.)))

        if plotting == True:
            plt.figure(3)
            #pdb.set_trace()
            try:
                plt.plot(np.arange(0,1 +1./(num_bins/2-1),1./(num_bins/2-1)),np.abs(H), 'r-', label = 'Freq. response of filter')
                plt.plot(np.arange(0,1 +1./(num_bins/2-1),1./(num_bins/2-1)), abs_Xmags[0:num_bins/2], 'b-', label = 'Abs. val Freq. magn.')
            except:
                plt.plot(np.arange(0,1 +1./(num_bins/2-1), 1./(num_bins/2-1))[:-1] , np.abs(H), 'r-', label = 'Freq. response of filter')
                plt.plot(np.arange(0,1 +1./(num_bins/2-1), 1./(num_bins/2-1))[:-1] , abs_Xmags[0:num_bins/2], 'b-', label = 'Abs. val Freq. magn.')

            plt.title('Frequency response of filter(normalised frequency)')
            plt.xlabel('Normalized frequency')
            plt.ylabel('Absolute Magnitude')
            plt.grid(True)
        plt.legend()
        # Filtering the signal:
        yData_mod = ssg.lfilter(b, a, self.yData)

        if plotting == True:
            xyplot(self.mytime, self.yData, xData_new = self.mytime, yData_new = yData_mod, mytitle = mytitles)

        return self.mytime, yData_mod

class windows(object):
    """ Class with several methods to smooth signal using moving windows
    Arguments:
        mytime:                 numpy array with x-axis values of the data series
        myData:                 numpy array with the y-axis values of the data series
    """
    def __init__(self, mytime, yData, window):
        self.mytime = mytime
        self.yData = yData
        self.window = window
    #@staticmethod
    def reg_window(self, window_len, mytitles = '', plotting = False, iter_args = ''):
        """ Use a window with requested size. Based on the convolution of a scaled window with the signal.
            The signal is prepared by introducing reflected copies of the signal (with the window size)
            in both ends so that transient parts are minimized in the begining and end part of
            the output signal.
        Arguments:
            window:         type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
                            flat window will produce a moving average smoothing.
            window_len:     dimension of the smoothing window; should be an odd integer
            mytitle(opt.):        Str, name of the dataset (for plotting options)
            plotting(opt.):       Boolean, plot results or not
        Returns:
            mytime:               1-D numpy array with the x values
            y:            1-D numpy array with the smoothed dataset

            example:
            t=np.linspace(-2,2,0.1)
            x=sin(t)+randn(len(t))*0.1
            y=smooth(x)

            see also:
            numpy.hanning, numpy.hamming, numpy.bartlett, numpy.blackman, numpy.convolve
            scipy.signal.lfilter

            TODO: the window parameter could be the window itself if an array instead of a string
            NOTE: length(output) != length(input), to correct this: return y[(window_len/2-1):-(window_len/2)] instead of just y.
        """

        if mytitles != '':
            mytitles = '%s\n%s. Length=%s, Type=%s' %(mytitles, 'Moving window', window_len, self.window)

        if self.yData.ndim != 1:
            raise ValueError("smooth only accepts 1 dimension arrays.")

        if self.yData.size < window_len:
            raise ValueError("Input vector needs to be bigger than window size.")

        if window_len<3:
            return self.mytime, self.yData

        if not self.window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman', 'gaus']:
            raise ValueError("Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'")

        s=np.r_[self.yData[window_len-1:0:-1], self.yData, self.yData[-1:-window_len:-1]]
        #print(len(s))
        if self.window == 'flat': #moving average
            w=np.ones(window_len, 'd')
        elif self.window == 'gaus':

            w = ssg.general_gaussian(window_len, iter_args[0], iter_args[1], sym = True)
        else:
            w = eval('np.'+ self.window + '(window_len)')

        y = np.convolve(w/w.sum(),s,mode='valid')
        y = y[(window_len/2-1):-(window_len/2)]

        assert len(y) == len(self.mytime), 'Time and ydata dimensions do not match!'

        if plotting == True:
            xyplot(self.mytime, self.yData, xData_new = self.mytime, yData_new = y, mytitle = mytitles)

        return self.mytime, y

    #@staticmethod
    def rec_window(self, numIter, ini_wind_len, win_len_step, mytitles = '', plotting= False, statistics = False, plotHist = False, iter_argum = ''):
        """ Remove outliers using the smooth window function in an iterative approach. Only those values that
            deviate larger than a mean deviation from the original signal minus the Smoothed signal, are replaced by the smoothed
            values. This keeps better the original shape of the data.
        Arguments:
           numIter:               Int, number of "smoothing" iterations
           ini_wind_len:          Int, initial length of the window
           win_len_step:          Int, step increment for the window length
           plotting(opt.):        Boolean, if True, the modified signal is presented each iteration
           statistics(opt.):      Boolean, if True, KS test is performed and returned
           plotHist(opt.):        Boolean, if True, a histogram of the original and processed data is shown
        Returns:
            mytime:               1-D numpy array with the x values
            yData_mod:            1-D numpy array with the smoothed dataset
        """
        if any(np.isnan(self.yData) == True):
            print('Warning! Nan found, the window containing them will take initial values...')
        mynewData =np.copy(self.yData)
        if mytitles != '':
            mytitles = '%s\n%s.\nNoIter=%s, Length$_{ini}$=%s, Delta length=%s' %(mytitles, 'Moving window (recursive)', numIter, ini_wind_len, win_len_step)

        for cur_it in range(0,numIter+1):
            if cur_it == 0:
                new_wind_length = ini_wind_len
            #if plotting is True:symbol = symbols.next()
            else:
                new_wind_length = new_wind_length + win_len_step

            mytime, self.yData = windows(self.mytime,self.yData, self.window).reg_window(new_wind_length, mytitles = '', plotting = False, iter_args = iter_argum)

            diff = (mynewData - self.yData)**2
            diff = np.sqrt(diff)
            med_abs_deviation = np.median(diff)

            deviationFromWindow = np.abs(mynewData-self.yData)

            IDX = np.where(deviationFromWindow > med_abs_deviation)

            filteredData = np.copy(mynewData)

            filteredData[IDX] = self.yData[IDX]     # Here just the values with a larger deviation than the mean
                                                    # are getting replaced by smoothed signal
        if plotting == True:
            xyplot(self.mytime, mynewData, xData_new = mytime, yData_new = filteredData, mytitle = mytitles)

        finalDeviation = np.abs(mynewData-filteredData)

        if statistics == True:
            D, pValue = sst.ks_2samp(mynewData,filteredData)
            print('%s'%(DataLabel))
            print('Kolmogorov-Smirnov test:')
            print('KS statistic = %.3e' %(D))

            if plotHist == True:
                plt.figure('KS- test')
                plt.hist(mynewData, bins = 20,normed = 1, histtype = 'step', color = 'blue', cumulative = True, label = 'original data')
                plt.hist(filteredData,bins = 20, normed = 1, histtype = 'step', color = 'red', cumulative = True, label = 'smoothed data')
                plt.legend(loc = 'best')
                plt.title('K-S test \n%s' %(DataLabel))
                plt.text(-35,0.8,'KS statistic= %.3e' %(D))
            return mytime, filteredData, D, finalDeviation

        else:
            assert len(filteredData) == len(mytime), 'Time and ydata dimensions do not match!'
            return mytime, filteredData, None, None

