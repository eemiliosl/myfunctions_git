#   -------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      IRTG
#
# Created:     01-07-2015
# Copyright:   (c) IRTG 2015
# Licence:     <your licence>
#   -------------------------------------------------------------------------------
import os
import numpy as np
import matplotlib.pyplot as plt
import platform
import sys
sys.path.append(os.path.abspath(os.path.join(os.curdir, '..')))
import grid_manip as gm


def xyplot(xData_old, yData_old, xData_new='', yData_new='', mytitle=''):
    if platform.system() == 'Linux':
        plt.ion()

    plt.figure(figsize=(7, 5))

    if (xData_new is '') and (yData_new is ''):
        plt.plot(xData_old, yData_old, 'b.', ms=5, label='Starting plot')
    else:
        plt.plot(xData_old, yData_old, '.b', ms=5, label='Starting plot')
        plt.plot(xData_new, yData_new, '.r', ms=3, label='Modified plot')
    # plt.plot(points, 'ro', new_points,'bx')
    if mytitle != '':
        plt.title(mytitle)
    plt.xlabel('X value')
    plt.ylabel('Y value')
    plt.grid(True)
    plt.legend(loc=0)
    plt.show()

    if platform.system() == 'Windows':
        plt.close()


def plot2dsections(grid_filedir, myvalues = '', typedata='elemental', xlim='', ylim='', zlim='', dx=1., dy=1.,
                   scalemin=0.0002, scalemax=0.01,
                   saveim=True, image_output='',
                   savetxt=False, ascii_output=''):
    """ Plot cross sections of the model based on x y and z values of the nodes. It uses the sel_grid function of
        dir_manip sub-module, hence it must be accounted for in the limits if it is wanted to plot elemental properties.
    Arguments:
        grid_filedir:       str, directory of the grid file of the model
        myvalues:           1D np array, values to interpolate and plot e.g. K values
        xlim, ylim, zlim:   2D np.arrays, x,y,z boundaries of the slice. If nodal, only one has to be given
        dx:                 float, element size of what will be X axis
        dy:                 float, element size of what will be Y axis
        typedata:           str, elemental or nodal property
        scalemin:           float, lower limit value of myvalues
        scalemax:           float, upper limit value of myvalues
        saveim:             bool, save image. default True
        image_output:       str, directory and output file name for the image
        savetxt:            bool, save ascii file with plotted values as raster matrix. default False
        ascii_output:       str, directory and output file name for the ascii file
    Returns:
        sel_values:             np.array in matrix form with the values selected for the slice
        if saveim or savetxt == True, stores either a png or an ascii file in the given directory
    Example:
        kkk = np.loadtxt(os.path.join(modeldir, 'Reference_model', '%s' % mymode, 'kkkGaus.dat'), usecols=(1,))
        sel_values = pcplt.plot2dsections(os.path.join(mydir, 'meshtecplot.dat'), myvalues=kkk, typedata='elemental',
                                  xlim=np.array([0, 10]),
                                  ylim=np.array([5.3, 5.6]), zlim=np.array([0, 10]), dx=1, dy=1,
                                  scalemin=0.0004, scalemax=0.006,
                                  saveim=False, image_output='interp1.png',
                                  savetxt=False, ascii_output='interp1.txt')

    """
    import matplotlib as mpl
    # First thing is to read grid data:
    nnodes, nelements, nodes_coord, element_nodes = gm.readmesh(grid_filedir, fulloutput=True)

    if typedata is 'nodal':
        # Find the maximum coordinate values in the x, y and z directions:
        xmax = np.max(nodes_coord[:, 0])
        ymax = np.max(nodes_coord[:, 1])
        zmax = np.max(nodes_coord[:, 2])

        # Number of nodes in each grid direction:
        xf_id = np.where(nodes_coord[:, 0] == xmax)[0][0]
        yf_id = np.where(nodes_coord[:, 1] == ymax)[0][0]
        zf_id = np.where(nodes_coord[:, 2] == zmax)[0][0]

        nodes_perlayer = yf_id + xf_id + 1

        nx_nodes = xf_id + 1  # Number of x nodes == xmax +1
        ny_nodes = nodes_perlayer / nx_nodes
        nz_nodes = nnodes / (nx_nodes * ny_nodes)

        if xlim is not '':
            x0_id = np.where(nodes_coord[:, 0] == xlim)[0][0]  # node id of the first node at slice position
            reshaped_values = np.flipud(np.reshape(myvalues[x0_id::nx_nodes], (-1, ny_nodes)))
            extent = (0, ymax, 0, zmax)

        if ylim is not '':
            y0_id = np.where(nodes_coord[:, 1] == ylim)[0][0]
            y_idx = np.array([])
            for ii in range(int(nz_nodes)):
                cur_idx = np.arange(y0_id + nodes_perlayer * ii, y0_id + nx_nodes + nodes_perlayer * ii, 1)
                y_idx = np.append(y_idx, cur_idx)

            reshaped_values = np.flipud(np.reshape(myvalues[y_idx.astype('int')], (-1, nx_nodes)))
            extent = (0, xmax, 0, zmax)

        elif zlim is not '':
            z0_id = np.where(nodes_coord[:, 2] == zlim)[0][0]
            reshaped_values = np.flipud(np.reshape(myvalues[z0_id:z0_id + nodes_perlayer], (ny_nodes, -1)))
            extent = (0, xmax, 0, ymax)

    if typedata is 'elemental':
        # Investigate the orientation of the slice: x = 0, y = 1, z = 2
        diff_coord = [np.diff(xlim), np.diff(ylim), np.diff(zlim)]
        axis_slice = min(enumerate(diff_coord), key=lambda x: x[1])
        # Select the elements of the slice
        sel_nodes, sel_elements = gm.sel_grid(grid_filedir, xlim, ylim, zlim)
        assert len(sel_nodes) and (len(sel_elements)) > 0, 'No nodes/elements selected, check X,Y or Z limits!'

        if axis_slice[0] == 0:  # means x is fixed
            range1 = ylim
            range2 = zlim
        elif axis_slice[0] == 1:  # means y is fixed
            range1 = xlim
            range2 = zlim
        elif axis_slice[0] == 2:  # means z is fixed
            range1 = xlim
            range2 = ylim

        # Define number of elements in x and y axes
        x_el = np.diff(range1) / dx
        y_el = np.diff(range2) / dy

        sel_values = myvalues[sel_elements]
        reshaped_values = np.flipud(np.reshape(sel_values, (x_el, y_el), order='F').T)
        # sel_elements_reshaped = np.flipud(np.reshape(sel_elements, (x_el, y_el), order='F').T)
        extent = (range1[0], range1[1], range2[0], range2[1])

    # Plot and/or save figures:
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    im1 = ax1.imshow(reshaped_values, interpolation='nearest', extent=extent)
    # l = ax1.plot(np.arange(0, 3, 1), np.arange(0, 3, 1), 'b-')
    norm = mpl.colors.Normalize(vmin=scalemin, vmax=scalemax)
    im1.set_norm(norm)
    fig.colorbar(im1)

    if saveim is True:
        plt.savefig(image_output, format='png', dpi=180)
    if savetxt is True:
        np.savetxt(ascii_output, reshaped_values, fmt='%6.5f')

    plt.show()

    return reshaped_values
