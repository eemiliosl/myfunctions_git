function p=shif_gen_inv_G_pdf(x,a,b,nu,xshift,yshift,scale)
% shifted_generalized_inverse_Gaussian_pdf(x,a,b,p,shift)
% shifted generalized inverse Gaussian distribution
% p(x) propto (x-shift)^(nu-1)*exp(-1/2*(a*(x-shift)+b/(x-shift)))*scale + yshift

xs=x-xshift;
xs(xs<0)=0;

p=(a/b)^(nu/2)/2/besselk(nu,sqrt(a*b))*xs.^(nu-1).*exp(-1/2*(a*xs+b./xs))*scale;
p(x<=xshift)=0;
p=p+yshift;