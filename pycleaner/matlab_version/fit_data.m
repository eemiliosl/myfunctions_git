clear all
close all

data=load('raw_b3_19112015.txt');
t=data(:,1);
V=data(:,end)+2e-3;
clear data

Vsmooth=V;
relax=0.9975;
Vsmooth(1:1000)=mean(V(1:1000));
for ii=2:length(V)
    Vsmooth(ii)=Vsmooth(ii-1)*relax+(1-relax)*Vsmooth(ii);
end
offset=floor(-1/log(relax));

shifty_0=mean(V(1:1000));
shiftx_0=0;
nu_0=-0.5;

m0=sum(V-shifty_0);
m1=sum((V-shifty_0).*t)/m0;
m2c=sum((V-shifty_0).*(t-m1).^2)/m0;

scale_0=m0*(t(2)-t(1));
a_0=m1/m2c;
b_0=m1^3/m2c;

par0=[a_0,b_0,nu_0,shiftx_0,scale_0];

[par,resnorm]=lsqcurvefit(@(par,x) shif_gen_inv_G_pdf(x,par(1),par(2),par(3),...
    par(4),shifty_0,par(5)),par0,t,V,[0,0,-10,0,0],[inf,inf,inf,max(t),scale_0*10]);

fitt=[0:60:43200];
fitV=shif_gen_inv_G_pdf(fitt,par(1),par(2),par(3),par(4),shifty_0,par(5));

a=par(1);
b=par(2);
nu=par(3);
t_shift=par(4);

tmean=sqrt(b/a)*besselk(nu+1,sqrt(a*b))...
      /besselk(nu,sqrt(a*b))+t_shift;
tpeak=((nu-1)+sqrt((nu-1)^2+a*b))/a+t_shift;

disp(sprintf('a:        %10.4g s^-1',par(1)))
disp(sprintf('b:        %10.4g s',par(2)))
disp(sprintf('nu:       %10.4g',par(3)))
disp(sprintf('t_shift:  %10.4g s',par(4)))
disp(sprintf('t_mean:   %10.4g s',tmean))
disp(sprintf('t_peak:   %10.4g s',tpeak))
disp(sprintf('V_shift:  %10.4g V',shifty_0))
disp(sprintf('scale:    %10.4g Vs',par(5)))
disp(sprintf('RMSE:     %10.4g V',sqrt(resnorm/length(t))))

figure(1)
%subplot(2,1,1)
plot(t/86400,V,'.','color',[.5 .5 .5])
hold on
plot(fitt/86400,fitV,'k-','linewidth',2)
plot(fitt/86400,fitV-shifty_0,'r-','linewidth',2)
plot([1 1]*tmean/86400,[0 0.01],'r--','linewidth',2)
plot([1 1]*tpeak/86400,[0 0.01],'b--','linewidth',2)
hold off
xlabel('time [h] since start of injection')
ylabel('U [V]')
ylim([0 0.01])
xlim([0 0.5])
set(gca,'xtick',[0:7200:43200]/86400,'xticklabel',...
   {'00:00','02:00','04:00','06:00','08:00','10:00','12:00'})
set(gcf,'paperunits','centimeters','paperposition',[0 0 18 9])
print -djpeg100 -r300 well3_fitted.jpg

figure(2)
plot(t/86400,V,'.','color',[.5 .5 .5])
hold on
plot(t(1:end-offset)/86400,Vsmooth(offset+1:end),'k-','linewidth',2)
plot(t(1:end-offset)/86400,Vsmooth(offset+1:end)-shifty_0,'r-','linewidth',2)
hold off
xlabel('time [h] since start of injection')
ylabel('U [V]')
ylim([0 0.01])
xlim([0 0.5])
set(gca,'xtick',[0:7200:43200]/86400,'xticklabel',...
   {'00:00','02:00','04:00','06:00','08:00','10:00','12:00'})
set(gcf,'paperunits','centimeters','paperposition',[0 0 18 9])
print -djpeg100 -r300 well3_smoothed.jpg

drawnow

% Genetic Algoritm is not better

% options=gaoptimset('populationsize',1000,'generations',100,'display','iter');
% [par_ga,res_ga] = ga(@(par) norm(shif_gen_inv_G_pdf(t,par(1),par(2),par(3),...
%     par(4),par(5),par(6))-V)^2,6,[],[],[],[],[par(1)/2,par(2)/2,par(3)*2,par(4)/2,par(5)/2,par(6)/2],...
%     [par(1)*2,par(2)*2,par(3)/2,par(4)*2,par(5)*2,par(6)*2],[],options);
% 
% fitV_ga=shif_gen_inv_G_pdf(t,par_ga(1),par_ga(2),par_ga(3),par_ga(4),par_ga(5),par_ga(6));
% 
% plot(t,V,'.',t,fitV,'-',t,fitV_ga,'-','linewidth',2)
% 
% disp(sprintf('a:        %10.4g s^-1',par_ga(1)))
% disp(sprintf('b:        %10.4g s',par_ga(2)))
% disp(sprintf('nu:       %10.4g',par_ga(3)))
% disp(sprintf('t_shift:  %10.4g s',par_ga(4)))
% disp(sprintf('V_shift:  %10.4g V',par_ga(5)))
% disp(sprintf('scale:    %10.4g Vs',par_ga(6)))
% disp(sprintf('RMSE:     %10.4g V',sqrt(res_ga/length(t))))
