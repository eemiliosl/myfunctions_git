# Define initial settings:
import numpy as np, os
import pycleaner.miscel as miscel
from .myplot import xyplot
import pycleaner.smooth as mysmooth
import pycleaner.rm_outlier as rmout
import pycleaner.menu as menu
import pycleaner.fitfun as fitfun
import pdb

def procData(mytime, y, cur_dataset, my_log_info, filepath):
    #%% Start processing the data:
    flag = 'no'
    time = np.copy(mytime)     # Make a copy to keep safe original times
    value = np.copy(y)      # Make a copy to keep safe original values

    # create logfile
    #logfile=open('logfile.txt','a')
    while (flag == 'no') or (flag == 'n'):

        mytime = np.copy(time)
        yData = np.copy(value)
        # Plot initial stage of data:
        xyplot(time, value, xData_new = '', yData_new = '', mytitle = cur_dataset)
        # Show Process menu:
        recordDevice = menu.procmenu()
        print('(Working with >> %s) ' %(cur_dataset))

        if recordDevice == 0:
            flag = 'yes'
        # Start list of functions, they have the same order as in the menu...
        #--------------#
        # 1) Kalman Filter
        if recordDevice == 1:
            mytsdev = menu.default_answer('Measurement Standard deviation (Default 0.1): ', 0.1, float)

            myobj = mysmooth.filters(mytime, yData)
            tr_time, tr_yData, error = myobj.kalman(meas_std_dev = mytsdev, out_spread = False, mytitles = cur_dataset, plotting = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData, mytime, yData)
            my_dict = 'Kalman Filter(1) \n'+'Measurement Standard deviation (mytsdev) = %s' %str(mytsdev) + '\n'
        ###############################################################################################

        #--------------#
        # 2) Remove signal noise using Butter filter
        elif recordDevice == 2:
            #x = value
            m = menu.default_answer('Critical frequency (Default 0.3): ', 0.3, float)
            myobj = mysmooth.filters(mytime, yData)
            tr_time, tr_yData = myobj.butter(n = m, mytitles = cur_dataset, plotting = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData,mytime, yData)
            my_dict = 'Butterworth filter, low pass(2) \nCritical frequency (m) = %s \n' % m
        ###############################################################################################

        #--------------#
        # 3) Smoothing using regular window
        elif recordDevice == 3:
            window = menu.check_userinput('Which window? (flat, hanning, hamming, bartlett, blackman, gaus): ', myoptions = ['flat', 'hanning', 'hamming', 'bartlett', 'blackman', 'gaus'])
            if window == 'gaus':
                p = menu.default_answer('Shape parameter.\n1:identical to gaussian\n0.55:Laplace distribution\n(Default = 1): ', 1, float)
                sigma = menu.default_answer('Standard deviation (Default = 1): ', 1, float)
                myargs = [p, sigma]
            else:
                myargs = ''

            window_len = menu.default_answer('Length of smoothing window (Default 4):', 4, int)

            myobjs = mysmooth.windows(mytime, yData, window)
            tr_time, tr_yData = myobjs.reg_window(window_len, mytitles = cur_dataset, plotting = True, iter_args = myargs)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData, mytime, yData)
            if window == 'gaus':
                my_dict = 'Smoothing with regular window(3) \ntype: %s \nLength of window (window_len) = %s\nShape parameter (p) = %s \nStandard deviation (sigma) = %s \n' % (window, window_len, p, sigma)
            else:
                my_dict = 'Smoothing with regular window(3) \ntype: %s \nLength of window (window_len) = %s\n' %(window, window_len)

        ###############################################################################################

        #--------------#
        # 4) Smoothing using recursive window
        elif recordDevice == 4:
            window = menu.check_userinput('Which window? (flat, hanning, hamming, bartlett, blackman, gaus): ', myoptions = ['flat', 'hanning', 'hamming', 'bartlett', 'blackman', 'gaus'])
            if window == 'gaus':
                p = menu.default_answer('Shape parameter.\n1:identical to gaussian\n0.55:Laplace distribution\n(Default = 1): ', 1, float)
                sigma = menu.default_answer('Standard deviation (Default = 1): ', 1, float)
                myargs = [p, sigma]
            else:
                myargs = ''

            numIter = menu.default_answer('Number of iterations (Default 1): ', 1, int)
            ini_wind_len = menu.default_answer('Initial window length (Default 4): ', 4, int)
            win_len_step = menu.default_answer('Step increment for the window length (Default 2): ', 2, int)
            myobjs = mysmooth.windows(mytime, yData, window)
            tr_time, tr_yData, D, error = myobjs.rec_window(numIter, ini_wind_len, win_len_step, mytitles = cur_dataset, plotting = True, iter_argum = myargs)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData,mytime, yData)
            if window=='gaus':
                my_dict = 'Smoothing with recursive window(4)\ntype: gaus\nShape parameter (p) = %s\nStandard deviation (sigma) = %s\n' %(p, sigma)
            else:
                my_dict = 'Smoothing with recursive window(4)\ntype: %s\nNumber of iterations (numIter) = %s\nInitial window length (ini_wind_len) = %s\nStep increment for window length (win_len_step) = %s\n' %(window, numIter, ini_wind_len, win_len_step)
        ###############################################################################################

        #--------------#
        # 5) Remove outliers using the distance of a point to a fitted function using splines
        elif recordDevice == 5:
            mymultiplier = menu.default_answer('Give a factor for the distance. The smaller the more strict in \ndefining a point as an outlier (default 3.0): ', 3, float)
            sporder = menu.default_answer('Order of the spline (between 1 and 5, default 3): ', 3, int)
            myobjss = rmout.outlier_detection(mytime, yData)
            tr_time, tr_yData = myobjss.dist2_fit(order = sporder, multiplier = mymultiplier, mytitles = cur_dataset, plotting = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData,mytime, yData)
            my_dict = 'Outlier removal by distance from spline fit(5)\nStrictness (mymultiplier) = %s\nOrder of spline (sporder) = %s\n' %(mymultiplier, sporder)
        ###############################################################################################

        #--------------#
        # 6) Remove outliers using the distance of a point to a its previous and consecutive points
        elif recordDevice == 6:
            mymultiplier = menu.default_answer('Give a factor for the distance. The smaller the more strict in \ndefining a point as an outlier (default = 3.): ', 3, float)
            myobjss = rmout.outlier_detection(mytime, yData)
            tr_time, tr_yData = myobjss.dist2_yval(multiplier = mymultiplier, mytitles = cur_dataset, plotting = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData,mytime, yData)
            my_dict = 'Outlier removal by distance from points(6)\nStrictness (mymultiplier) = %s\n' %mymultiplier
        ###############################################################################################

        #--------------#
        # 7) Distance to mean values (step-wise)
        elif recordDevice == 7:
            mymultiplier = menu.default_answer('Give a factor for the distance. The smaller the more strict in \ndefining a point as an outlier (default = 3.): ', 3, float)
            mystepsize = menu.default_answer('How big should the stepsize be? Default = Max = len(ydata): ', len(yData), int)

            myobjss = rmout.outlier_detection(mytime, yData)
            tr_time, tr_yData = myobjss.dist2_meanstd(multiplier = mymultiplier, step_size = mystepsize, mytitles = cur_dataset, plotting = True )
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData,mytime, yData)
            my_dict = 'Outlier removal by distance from mean values(7)\nStrictness (ymultiplier) = %s\nStepsize (mystepsize) = %s\n' %(mymultiplier, mystepsize)
        ###############################################################################################

        #--------------#
        # 8) Distance to modified Zscores (step-wise)
        elif recordDevice == 8:
            mystepsize = menu.default_answer('How big should the stepsize be? Default = Max = len(ydata): ', len(yData), int)
            mythresh = menu.default_answer('Define threshold to remove outliers. Default = 3.5: ', 3.5, float)

            myobjss = rmout.outlier_detection(mytime, yData)
            tr_time, tr_yData = myobjss.mod_Zscore(step_size = mystepsize, thresh = mythresh, mytitles = cur_dataset, plotting = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData,mytime, yData)
            my_dict = 'Distance to modified Zscores(8)\nStepsize (mystepsize) = %s\nThreshold (mythresh) = %s\n' %(mystepsize, mythresh)
        ###############################################################################################

        #--------------#
        # 9) Fit the generalized inverse gaussian distribution
        elif recordDevice == 9:
            tr_time, tr_yData, a, b, p, shiftx, shifty, scale, mean_inv, mode_inv, var_inv, m2c, m0, m1, tmean = fitfun.call_invgaus(mytime, yData, mytitles = cur_dataset, plotting = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData,mytime, yData)
            ans = menu.check_userinput('Store optimized parameters and statistics in a separate file (y/n)?: ', myoptions = ['y', 'n'])
            if ans == 'y':
                np.savetxt(os.path.join(filepath, cur_dataset.split('.')[0] + '_inv_gaus_res.txt'), np.transpose((m0, m1, tmean, m2c, mean_inv, mode_inv, var_inv)), fmt ='%6.5f' , header = 'zeroth_m, first_m, mean_t, seconc_cm, mean_fit, mode_fit, variance_fit')
            my_dict = 'Fit generalized inverse gaussian distribution(9)\nParameter a (a) = %s\nParameter b (b) = %s\nParameter p (p) = %s\nShift x (shiftx)= %s\nShift y (shifty)= %s\nScale (scale)= %s\nMean of fit (mean_inv)= %s\nMode of fit (mode_inv)=%s\nVar of fit (var_inv)= %s\n' %(a, b, p, shiftx, shifty, scale, mean_inv, mode_inv, var_inv)
            my_dict = my_dict + 'Zeroth temporal moment (m0) = %s\nFirst temporal moment (m1) = %s\nMean arrival time (tmean) = %s\nSecond central moment (m2c) = %s\n' % (m0, m1, tmean, m2c)
        ###############################################################################################

        #--------------#
        # 10) Smooth using spline interpolation and arbitrary anchor points: Seems that it does not work properly with pick events.....
        elif recordDevice == 10:
            s = menu.default_answer('Smoothing factor (Default 0.25)\nCloser to 0 will interpolate closer to data points: ', 0.25, float)

            #Positive smoothing factor used to choose the number of knots. Number of knots will be increased until the smoothing condition is satisfied:
            #sum((w[i] * (y[i]-spl(x[i])))**2, axis=0) <= s
            tr_time, tr_yData = fitfun.spline_interpArbPoints(mytime, yData, s, mytitles = cur_dataset, plotting = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData,mytime, yData)
            my_dict = 'Smoothing with spline-interpolation(10)\nSmoothing factor (s) = %s\n' % s

        ###############################################################################################

        #--------------#
        # 11) Get evenly distributed data using splines:
        elif recordDevice == 11:
            smooth = menu.default_answer('Smoothing factor (0 = no smoothing, Default 0): ', 0.0, float)
            degree = menu.default_answer('Degree of the smoothing spline (Default 3). \nMust be <= 5: ', 3, int)
            time_array = input('New time series? \nNo=empty; yes=[initial,final+timestep, timestep]:  ')

            try:
                time_array = np.arange(float(time_array.split(',')[0]), float(time_array.split(',')[1]), float(time_array.split(',')[2]))
            except:
                print ('Using input time series...')
                time_array = ''
            tr_time, tr_yData = fitfun.get_evendata(mytime, yData, myknots = time_array, s = smooth, k = degree, mytitles = cur_dataset, plotting = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData,mytime, yData)
            if time_array=='':
                my_dict = 'Get evenly distributed data(11)\nSmoothing factor (smooth)= %s\nOrder of spline (degree)= %s\nTime series (time_array) = input array was used\n ' %(smooth, degree)
            else:
                my_dict = 'Get evenly distributed data(11)\nSmoothing factor (smooth) = %s\nOrder of spline (degree) = %s\nTime series (time_array) = %s\n ' %(smooth, degree, time_array)

        ###############################################################################################

        #--------------#
        # 12) Use Jacknife resampling scheme:
        elif recordDevice == 12:
            tr_time, tr_yData = fitfun.jackknife(mytime, yData, mytitles = cur_dataset, plotting = True)
            #myfactor = menu.default_answer('Percentage of deviation (Default = 0.1), \nif fit improves more than factor, point will be outlier: ', 0.1, float)
            #tr_time, tr_yData = fitfun.jackknife(mytime, yData, multiplier = myfactor, mytitles = cur_dataset, plotting = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData,mytime, yData)
            #my_dict = 'Jackknife(12)\nStrictness (myfactor) = %s\n' %myfactor
            my_dict = 'Jackknife(12)\nCriteria = ynew[np.where(rootmeansquares < (rmsefull - stdev))[0]] = np.nan\n'

        ###############################################################################################

        #--------------#
        # 13) Set nan
        elif recordDevice == 13:
            tr_time, tr_yData, xindices, byrange = miscel.ReplaceForNAN(mytime, yData, mytitles = cur_dataset, plotting = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData,mytime, yData)
            my_dict = 'Set nan(13)\nIndices of values set nan (xindices) = %s\nRange set nan? (byrange) = %s\n' %(xindices, byrange)

        ###############################################################################################

        #--------------#
        # 14) Remove data points
        elif recordDevice == 14:
            tr_time, tr_yData, xindices, byrange = miscel.remove_datapairs(mytime, yData, mytitles = cur_dataset, plotting = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData,mytime, yData)
            my_dict = 'Remove data points(14)\nIndices of removed datapoints (xindices) = %s\nRange removed? (byrange) = %s' %(xindices, byrange)

        ###############################################################################################


        #--------------#
        # 15) Trim the dataset
        elif recordDevice == 15:
            #mydata = value
            ini_time = menu.default_answer('Initial time [seconds], Default = to: ', mytime[0], float)
            final_time = menu.default_answer('Final time [seconds], Default = tf: ', mytime[-1], float)
            ##time_step = menu.default_answer('Time step [seconds], Default = tf - (tf-1): ', mytime[-1] - mytime[-2], float)

            tr_DATE, tr_time, tr_yData = miscel.trimData(mytime, yData, ini_time, final_time, time_date = '', mytitles = cur_dataset, plotting  = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData, mytime, yData)
            my_dict = 'Trim dataset(15)\nTime to start trimming (ini_time) = %s\nTime to stop trimming (final_time) = %s\n' %(ini_time, final_time)

        ###############################################################################################

        #--------------#
        # 16) Add/subtract a specified float on Y values
        elif recordDevice == 16:
            #print 'Pick events do not work properly with Ipython notebook, do it in a different way...'
            #mydata = np.c_[time,value]
            tr_time, tr_yData, xindices, myvalue = miscel.add_floatvalue(mytime, yData, mytitles = cur_dataset, plotting  = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData,mytime, yData)
            my_dict = 'Add/subtract a float(16) to Y values\nFirst and last indices of time series to get added/subtracted (xindices) = %s\nAdded/subtracted value (myvalue) = %s\n' %(xindices, myvalue)


        ###############################################################################################

        #--------------#
        # 17) Add/subtract a specified float on X values
        elif recordDevice == 17:
            #print 'Pick events do not work properly with Ipython notebook, do it in a different way...'
            #mydata = np.c_[time,value]
            tr_time, tr_yData, xindices, myvalue = miscel.shift_onx(mytime, yData, mytitles = cur_dataset, plotting  = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData,mytime, yData)
            my_dict = 'Add/subtract a float(17) to X values\nFirst and last indices of time series to get added/subtracted (xindices) = %s\nAdded/subtracted value (myvalue) = %s\n' %(xindices, myvalue)

        #--------------#
        # 18) Get Mean Value and standard deviation
        elif recordDevice == 18:
            print('The standard deviation can be used as an error measurement value...')
            my_mean, my_std, xindices = miscel.get_mean_std(mytime, yData)
            keep = 'y'
            #flag, time, value = DF.conf_menu(tr_time, tr_yData,mytime, yData)
            my_dict = 'Mean value and standard deviation(18)\nSelected range (xindices) = %s\nMean value (my_mean) = %s\nStandard deviation (my_std) = %s\n' %(xindices, my_mean, my_std)

        ###############################################################################################

        #--------------#
        # 19) Select representative points from the whole dataset, using either normal or log time scaling
        elif recordDevice == 19:
            tr_time, tr_yData, logscale, npoints, myid = miscel.get_reppoints(mytime, yData, mytitles = cur_dataset, plotting = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData, mytime, yData)
            if logscale=='':
                my_dict = 'Select representative points(19)\nIndices of points = %s\n' %myid
            else:
                my_dict = 'Select representative points(19)\nLogscale used (logscale) = %s\nNumber of points (npoints) = %s\n' %(logscale, npoints)
        ###############################################################################################

        #--------------#
        # 20) Get Cumulative distributions
        elif recordDevice == 20:
            norm =bool( input('Normalized data?\n -empty: no, \n1: yes') )
            tr_time, tr_yData = miscel.cumsum(mytime, yData, mytitles = cur_dataset, normalized = norm, plotting = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData, mytime, yData)
            my_dict = 'Get cumulative distribution(20)\nNormalized data: %s\n' %(norm)

        ###############################################################################################

        #--------------#
        # 21) Remove linear trend
        elif recordDevice == 21:
            tr_time, tr_yData, xindices = miscel.detrend_lin(mytime, yData, mytitles = cur_dataset, plotting = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData, mytime, yData)
            my_dict = 'Remove linear trend(21)\nSelected range (xindices) = %s\n' %xindices

        ###############################################################################################

        #--------------#
        # 22) Invert Y data with respect to first data point
        elif recordDevice == 22:
            tr_time, tr_yData = miscel.flipYdata(mytime, yData, mytitles = cur_dataset, plotting = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData, mytime, yData)
            my_dict = 'Invert Y data (22) with respect to Y[0]\n'

        if keep == 'y':
            my_log_info = my_log_info + '\n\n' + my_dict
            #logfile.write(30*'-'+'\n')
            #logfile.write(my_dict)
        else:
            pass

        if (flag == 'yes') or (flag == 'y'):
                joint_dataset = np.c_[time, value]
    #logfile.close()
    return joint_dataset, recordDevice, my_log_info