#-------------------------------------------------------------------------------
# Name:        miscel
# Purpose:     miscelaneus of functions not categorized or simply defined as def
# Author:      eesl
# Created:     02-07-2015
# Copyright:   (c) IRTG-eesl 2015
#-------------------------------------------------------------------------------
import warnings, matplotlib.cbook
warnings.filterwarnings('ignore', category = matplotlib.cbook.mplDeprecation)
#import sys
import os, numpy as np, matplotlib.pyplot as plt, platform
#import scipy.stats as sst
#import scipy
#import scipy.interpolate as si
#import scipy.signal as ssg
#import time
import h5py
#import Tkinter as tki
from .myplot import xyplot
from . import menu
import matplotlib.mlab as mlab

def pick_points(mydata, mytitle = '', rotatemarks = False):
    """
    Pick points within a plot with X,Y data, by clicking on
    the corresponding points. Supports both Windows and Linux systems
        Arguments:
        ----------
    mydata:     np array, two dim array (X,Y)
    mytitle:    str, title for the plot (name of the dataset)
        Returns:
        --------
    List with the indices of the selected points
    """
    how2sel = menu.check_userinput('(1) Select full dataset. (2) Select points (or range) of interest: ', myoptions = ['1', '2'])
    if how2sel == '1':
        xindices = [0, len(mydata)-1]

    if how2sel == '2':
        print('Select points and close window to continue...')
        xindices = []

        def onpick(event):

            thisline = event.artist
            xdata = thisline.get_xdata()
            ydata = thisline.get_ydata()
            ind = event.ind

            xindices.append(ind[-1])
            if (len(xindices) % 2 is not 0): # if len xindices is odd
                print ('1st. X = %s, Y = %s (idx:%s)' % (xdata[ind[-1]], ydata[ind[-1]], ind[-1]))

            elif (len(xindices) % 2 is 0): # if len xindices is even
                print ('2nd. X = %s, Y = %s (idx:%s)' % (xdata[ind[-1]],ydata[ind[-1]],ind[-1]))

            if platform.system() == 'Windows':
                fig.canvas.draw()
        fig = plt.figure('Pick the points')
        # Create figure:
        if platform.system() == 'Linux':

            plt.grid(True)
            fig.canvas.mpl_connect('pick_event',onpick)
            pt, = plt.plot(mydata[:,0],mydata[:,1],'b.',markersize = 5, mew = 0.1 ,picker = 2., label = 'Raw Data')

            plt.show()
            print('You have 20 seconds...')
            plt.pause(20)

        elif platform.system() == 'Windows':
            ax = fig.add_subplot(111)
            plt.grid(True)

            # Plot stuff:
            line, = ax.plot(mydata[:,0],mydata[:,1],'b.',markersize = 5, mew = 0.1 ,picker = 2., label = 'Raw Data')
            if rotatemarks == True:
                symbol = 'bx'
            elif rotatemarks == False:
                symbol = 'b--'
            ax.plot(mydata[:,0],mydata[:,1],symbol)
            ax.set_title(mytitle)

            line.figure.canvas.mpl_connect('pick_event',onpick)
            plt.show()

    return xindices

#    if platform.system() == 'Linux':
#        def onpick_linux(event):
#            thisline = event.artist
#            xdata = thisline.get_xdata()
#            ydata = thisline.get_ydata()
#            ind = event.ind
#            #pdb.set_trace()
#            xindices.append(ind[-1])
#            if (len(xindices) % 2 is not 0): # if len xindices is odd
#                print ('1st. X = %s, Y = %s (idx:%s)' % (xdata[ind[-1]], ydata[ind[-1]], ind[-1]))
#
#            elif (len(xindices) % 2 is 0): # if len xindices is even
#                print ('2nd. X = %s, Y = %s (idx:%s)' % (xdata[ind[-1]],ydata[ind[-1]],ind[-1]))
#
#        #pdb.set_trace()
#        fig.canvas.mpl_connect('pick_event',onpick_linux)
#
#        pt, = plt.plot(mydata[:,0],mydata[:,1],'b.',markersize = 5, mew = 0.1 ,picker = 2., label = 'Raw Data')
#
#        plt.show()
#
#        plt.pause(15)
#
#    if platform.system() == 'Windows':
#
#        ax = fig.add_subplot(111)
#        plt.grid(True)
#
#        # Plot stuff:
#        line, = ax.plot(mydata[:,0],mydata[:,1],'b.',markersize = 5, mew = 0.1 ,picker = 2., label = 'Raw Data')
#        ax.plot(mydata[:,0],mydata[:,1],'b--')
#        ax.set_title(mytitle)
#
#
#        # Handle the picking event. Return indices rather than actual X value:
#        def onpick(event):
#            thisline = event.artist
#            xdata = thisline.get_xdata()
#            ydata = thisline.get_ydata()
#            ind = event.ind
#
#            xindices.append(ind[-1])
#            if (len(xindices) % 2 is not 0): # if len xindices is odd
#                print ('1st. X = %s, Y = %s (idx:%s)' % (xdata[ind[-1]], ydata[ind[-1]], ind[-1]))
#
#            elif (len(xindices) % 2 is 0): # if len xindices is even
#                print ('2nd. X = %s, Y = %s (idx:%s)' % (xdata[ind[-1]],ydata[ind[-1]],ind[-1]))
#
#            fig.canvas.draw()
#
#        line.figure.canvas.mpl_connect('pick_event',onpick)
#        plt.show()
#        #plt.close()

def trimData(time, mydata, ini_time, final_time, time_date = '', mytitles = '', plotting  = True):
    """ Trim a dataset using specified initial and final times. If Time in date format is given,
        it returns an additional array that contains the date (hour,minutes,seconds) that correspond
        to the new first initial data point till the very last one.
    Arguments:
        time:       Array with the time span of the input dataset
        mydata:     Array, dataset to be trimmed
        ini_time:   Float, time (NOT date format) to start trimming the dataset
        final_time: Float, time (NOT date format) to finish trimming the dataset
        time_date(Optional):  Array with the time span (same dimensions as time) in date format
    Return:
        trimed_time_date:   If provided it contains the trimmed time series in date format
        trimed_time:        Trimmed time series
        trimed_data:        Trimed dataset (e.g. drawdown)
    """
    # Trim time series using initial and final time values:
    trimed_time = time[(time >= ini_time) & (time <= final_time )]
    # Reset time series:
##    if trimed_time[0] == 0:
##        trimed_time = np.arange(0,len(trimed_time)*time_step,time_step)

    # Trim time series in date format:
    try:
        trimed_time_date = time_date[(time >= ini_time) & (time <= final_time )]
    except:
        trimed_time_date = 'Notprovided'
        pass

    # Trim the dataset:
    trimed_data = mydata[(time >= ini_time) & (time <= final_time )]
##    # Correct dimensions if they do not match:
##    if (len(reset_time) != len(trimed_data)) and (len(reset_time) > len(trimed_data)):
##        reset_time = reset_time[0:len(trimed_data)]
##
##    if (len(reset_time) != len(trimed_data)) and (len(reset_time) < len(trimed_data)):
##        trimed_data = trimed_data[0:len(trimed_data)]

    if plotting == True:
        xyplot(time, mydata, xData_new = trimed_time, yData_new = trimed_data, mytitle = mytitles)

    return trimed_time_date, trimed_time, trimed_data

def remove_datapairs(mytime, mydata, mytitles = '', byrange = False, plotting = False):
    """ Remove either a single data pair or  data pairs within a range, from the dataset.
        It handles only either a single point or a range!!!
    Arguments:
        mydata:     2D - Array, dataset to work with (X and Y)
        mytitle:    Str, dataset identifier
    Return:
        2-D numpy array. New data set with the selected points removed.
    """
    if mytitles != '':
        mytitles = '%s\n%s' %(mytitles, 'Remove points from data series')
    byrange = menu.check_userinput('Replace a whole range? (n/y)\nIf no, single points will be replaced:', myoptions = ['y', 'n'])
    #print 'You have selected 4. Remove data points'
    print('The data pairs within selected range will be removed from data series, select just one range at a time')
    temp = np.copy(mydata)
    timetemp = np.copy(mytime)
    xindices = pick_points(np.transpose((timetemp, temp)), mytitles)

    if byrange == 'n':
        timetemp = np.delete(timetemp, xindices)
        temp = np.delete(temp,xindices)
    elif (byrange == 'y'):
        timetemp = np.delete(timetemp, np.arange(xindices[0],xindices[-1]+1))
        temp = np.delete(temp, np.arange(xindices[0],xindices[-1]+1))

    if plotting == True:
        xyplot(mytime, mydata, xData_new = timetemp, yData_new = temp, mytitle = mytitles)
    return timetemp, temp, xindices, byrange

def ReplaceForNAN(mytime, mydata, mytitles = '', plotting = False):
    """ Replace either a single data pair or data pairs within a range, with NAN.
        It handles only either a single point or a range!!!
    Arguments:
        mydata:     2D - Array, dataset to work with (X and Y)
        mytitles:    Str, dataset identifier
    Return:
        2-D numpy array. New data set with the selected points changed to NAN.
    """
    if mytitles != '':
        mytitles = '%s\n%s' %(mytitles, 'Replace points for NaN')

    byrange = menu.check_userinput('Replace a whole range? (n/y)\nIf no, single points will be replaced:', myoptions = ['y', 'n'])
    #mymethod = bool(raw_input('Replace whole range?\n->empty: no (single points)\n->1: yes'))
    print('All selected points will be substituted with a np.nan element')
    temp = np.copy(mydata)
    xindices = pick_points(np.transpose((mytime,mydata)), mytitles)

    if (len(xindices) > 1) and (byrange == 'y'):
        temp[xindices[0]:xindices[-1]+1] = np.nan
    elif byrange == 'n':
        temp[xindices] = np.nan

    data_mod = np.array(temp)

    if plotting == True:
        xyplot(mytime, mydata, xData_new = mytime, yData_new = data_mod, mytitle = mytitles)

    return mytime, data_mod, xindices, byrange

def detrend_lin(mytime, mydata, mytitles = '', plotting = False):
    """ Remove a linear trend from a data series
    Arguments:
        mytime:     2D Array, X values
        mydata:     2D - Array, Y values
        mytitles:    Str, dataset identifier
        plotting:   Boolean, plot or not
    """
    tempData = np.copy(mydata)
    flag = 'repeat'
    while flag == 'repeat':
        xindices = pick_points(np.transpose((mytime,tempData)), mytitles)

        if len(xindices) > 2:
            print('Select EXACTLY two points!!')
        if len(xindices) < 2:
            print('Select EXACTLY two points')
        if len(xindices) == 2:
            flag ='norepeat'

    if len(xindices) == 2:
        tempData[xindices[0]:xindices[-1]+1] = mlab.detrend_linear(tempData[xindices[0]:xindices[-1]+1])
    if plotting == True:
        xyplot(mytime, mydata, xData_new = mytime, yData_new = tempData, mytitle = mytitles)

    return mytime, tempData, xindices

def shift_onx(mytime, mydata, mytitles = '', plotting = False):
    """ shift the dataset along the x axis
    """
    tempData = np.copy(mytime)
    flag = 'repeat'
    while flag == 'repeat':
        xindices = pick_points(np.transpose((mytime,mydata)), mytitles)
        if len(xindices) > 2:
            print('Select MAXIMUM two points!!')
        if len(xindices) <= 2:
            flag ='norepeat'
        if len(xindices) == 0:
            print('Select at least one point!')
    myvalue = input('Enter the value to shift the data over the x axis, \ntype -999 to bring data to 0: ')
    try:
        myvalue = float(myvalue)
        if myvalue == -999:
            shift_time = tempData[xindices[0]:xindices[-1]+1] - tempData[xindices[0]]
        else:
            shift_time = tempData[xindices[0]:xindices[-1]+1] - myvalue
    except:
        print('Not a valid entry, the points will be shifted to 0...')
        shift_time = tempData[xindices[0]:xindices[-1]+1] - tempData[xindices[0]]
        myvalue = tempData[xindices[0]]

    tempData[xindices[0]:xindices[-1]+1] = shift_time

    if plotting == True:
        xyplot(mytime, mydata, xData_new = tempData, yData_new = mydata, mytitle = mytitles)

    return tempData, mydata, xindices, myvalue

def add_floatvalue(mytime, mydata, mytitles = '', plotting  = False):
    """ Add or subtract a specified value to a subset of the original dataset
        Note: select only a range of data at a time (i.e. pick just one pair
        of points)
    Arguments:
        mydata: 2D - Array, dataset to work with (X and Y)
    Return:
        2-D numpy array with the provided value added to the selected subset.
    """
    tempData = np.copy(mydata)
    flag = 'repeat'
    while flag == 'repeat':
        xindices = pick_points(np.transpose((mytime,tempData)), mytitles)
        if len(xindices) > 2:
            print('Select MAXIMUM two points!!')
        if len(xindices) <= 2:
            flag ='norepeat'
        if len(xindices) == 0:
            print('Select at least one point!')

    myvalue = float(input('Enter the value to add(+) or subtract(-) to Y values.\ntype -999 to set it automatically: '))
    if myvalue == -999:
        mynum = 20
        aux = tempData[xindices[0]-mynum:xindices[0]]
        aux2 =tempData[xindices[1]:xindices[1]+mynum]
        baseMean =np.mean(np.r_[aux,aux2])
        shiftMean = np.mean(tempData[xindices[0]:xindices[1]])
        myvalue = baseMean - shiftMean

    if len(xindices) == 1:
        tempData[xindices[0]] = tempData[xindices[0]] + myvalue
    if len(xindices) == 2:
        tempData[xindices[0]:xindices[-1]+1] = tempData[xindices[0]:xindices[-1]+1] + myvalue
    if plotting == True:
        xyplot(mytime, mydata, xData_new = mytime, yData_new = tempData, mytitle = mytitles)

    return mytime, tempData, xindices, myvalue

def get_mean_std(mytime, mydata, mytitles = ''):
    """ Return the mean value of a certain range of the graph
    Arguments:
        mydata:     2D - Array, dataset to work with (X and Y)
        mytitles:    Str, dataset identifier
    Return:
        2-D numpy array.
    """
    print('Select a range from where to estimate mean value')
    temp = np.copy(mydata)
    xindices = pick_points(np.transpose((mytime,mydata)), mytitles)

    my_mean = np.mean(temp[xindices[0]:xindices[-1]+1])
    my_std = np.std(temp[xindices[0]:xindices[-1]+1])
    print('Mean value = %s, Std Dev = %s' %(my_mean, my_std))
    return my_mean, my_std, xindices

def cumsum(mytime, mydata, mytitles = '', normalized = False, plotting = False):
    """ Estimate cumulative distribution of values. Function is useful to transform btc...
    It is not advisable to use it with drawdown. Proper way of normalize non-cumsum data:
    normalized = (x-min(x))/(max(x)-min(x))
    Arguments:
        mytime, mydata:     1D - Array, dataset to work with (X and Y)
        mytitles:           Str, dataset identifier
        normalized:         Bool, whether to normalize the cdf or not. Default False
        plotting:           Bool, plot results or not. Default False
    Return:
        2-D numpy array. Return the cumulative sum of the elements along a given axis.
    """
    ynew = np.copy(mydata)
    mycumsum = np.cumsum(ynew)

    if normalized == True:
        mycumsum = mycumsum/np.sum(mycumsum)

    if plotting == True:
        xyplot(mytime, (ynew-np.min(ynew))/(np.max(ynew)-np.min(ynew)), xData_new = mytime, yData_new = mycumsum, mytitle = mytitles)

    return mytime, mycumsum

def tempmoments(mytime, mydata, degree = 0, central = False, full_output = False):
    """ Calculate temporal moments of the curve
    Arguments:
        mytime:     np.array, X values
        mydata:     np.array, Y values
        degree:     int, degree of the central moment to calculate. discarded if central is False
        central:    bool, estimate central moments or not
        full_output:    bool, return full output or not
    Returns:
        if full_output is True
                        mom:centralmoment
                        mom_0:zeroth temp moment
                        mom_1:first temp moment
                        t_mean: mean arrival time
        if full_output is False
                        temporal moment of defined degree
    """
##    moment_i = np.zeros(len(mytime))
##    for ii in range(0, len(mytime)):
##        moment_i[ii] = (mytime[ii]**degree)*mydata[ii]
    if central is False:
        mom = np.sum((mytime**degree)*mydata)*(mytime[1]-[0])
        return mom
    elif central is True:
        mom_0 = np.sum((mytime**0)*mydata)*(mytime[1]-mytime[0])
        mom_1 = np.sum((mytime**1)*mydata)*(mytime[1]-mytime[0])
        t_mean = mom_1 / mom_0
        mom = np.sum(((mytime - t_mean)**degree)*mydata)*(mytime[1]-mytime[0])

        if full_output is False:
            return mom

        if full_output is True:
            return mom, mom_0, mom_1, t_mean

def get_reppoints(mytime, mydata, mytitles = '', plotting = False):
    """ Get representative datapoints of the given curve. Default number of points = 100. The selection
        can be done based on a log scale of time axis, in order to get log spacing selection.
     Arguments:
        mytime:     2D Array, X values
        mydata:     2D - Array, dataset to work with (X and Y)
        logscale:   Boolean, if True: perform data selection based on a log scale in x axis
        npoints:   Integer, number of points to select from the curve, default = 100
    Return:
        numpy arrays, selected x and y values
    """
    tempdata = np.copy(mydata)

    mymethod = menu.check_userinput('How to select the points? \n1: Manual\n2: Automatic', myoptions = ['1','2'])

    if mymethod == '1':
        myid = pick_points(np.transpose([mytime,mydata]), mytitles)
        logscale=''
        npoints=''
    elif mymethod == '2':
        question ='Log transform before selecting data?(yes/no)'; myoptions = ['yes','no']
        logscale = menu.check_userinput(question, myoptions = myoptions)
        npoints = int(input('How many points you want to get back?\n(type an integer)'))

        if 'n' in logscale:
            temptime = np.copy(mytime)
        elif 'y' in logscale:
            #remove initial time value equal to zero
            temptime = np.log(mytime[1:])
            tempdata = tempdata[1:]

        spacing = np.arange(temptime[0], temptime[-1], (temptime[-1]- temptime[0])/npoints)
        myid = np.zeros(spacing.shape)

        for ii in range(0,len(spacing)):

            myid[ii] = np.where(np.min(abs(spacing[ii]-temptime))== abs(spacing[ii]-temptime))[0][0]
        myid = np.unique(myid.astype(int))

    temptime = mytime[myid]
    tempdata = mydata[myid]

    if plotting == True:
        xyplot(mytime, mydata, xData_new = temptime, yData_new = tempdata, mytitle = mytitles)

    return temptime, tempdata, logscale, npoints, myid

def flipYdata(mytime, mydata, mytitles = '', plotting = False):
    """ Invert Y values of a 2D data-series. Useful to transform absolute
        hydraulic heads to drawdown
    Arguments:
        mytime:     2D Array, X values
        mydata:     2D - Array, dataset to work with (X and Y)
        mytitles:   Str, dataset identifier
        plotting:   Bool, plot results or not. Default False

    Returns:
        mytime and inverted array mydata and with respect to mydata[0]
    """
    tempdata = np.copy(mydata)
    tempdata = np.subtract(tempdata[0], tempdata)

    if plotting == True:
        xyplot(mytime, mydata, xData_new = mytime, yData_new = tempdata, mytitle = mytitles)


    return mytime, tempdata
""" --------------------------- """
""" End of file """