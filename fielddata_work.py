#-------------------------------------------------------------------------------
# Name:        fielddata
# Purpose:     deal with files generated in the field
# Author:      eesl
# Created:     20-11-2015
# Copyright:   (c) IRTG-eesl 2015
#-------------------------------------------------------------------------------
import os, numpy as np, matplotlib.pyplot as plt
import pycleaner.fielddata as pcfd
import pandas as pd, pdb
from scipy import interpolate
import pycleaner.fitfun as pff
import pycleaner.miscel
from scipy.optimize import curve_fit, fmin

#%% For tracer:
date = '02122015'
##source_path = r'C:\PhD_Emilio\Emilio_PhD\Field_Data\_RawData\Tracer\raw_data_%s'%date
mybtc_path = r'C:\PhD_Emilio\Emilio_PhD\Field_Data\_ProcessedData\TracerTest\tracer_%s'%date
##myfile_btc = '%s.txt'%date
##mydict = pcfd.load_tracer19(source_path, myfile_btc, True)
##mywells_conc = ['xxxcmt36','cmt31','3_2','cmt26', 'b6mid','2_1', 'cmt22', 'xxxcmt32', 'b6top', '1_1', 'cmt12', 'cmt16', 'b6bot', 'cmt24', '3_1', '4_4', 'b3', 'cmt35', '2_2','ambient']
###['cmt36', '3_1', 'cmt45', 'cmt26', 'b3bot', '2_1', '2_4', 'cmt46', '2_3', '1_3', 'cmt13', 'cmt16', 'b3mid', 'cmt24', '4_3', '4_4', 'b6', 'cmt35', '1_2', 'ambient']
###['cmt36','cmt31','3_2','cmt26', 'b3bot','2_1', 'cmt22', 'cmt32', '2_3', '1_1', 'cmt13', 'cmt16', 'b3mid', 'cmt24', '3_1', '3_4', 'b6', 'cmt35', '2_2','ambient']
###['cmt36','cmt31','3_2','cmt26', 'b6mid','2_1', 'cmt22', 'cmt32', 'b6top', '1_1', 'cmt12', 'cmt16', 'b6bot', 'cmt24', '3_1', '4_4', 'b3', 'cmt35', '2_2','ambient']
##pcfd.savetxt_tracer19(mybtc_path, mydict,mywells_conc)
#pdb.set_trace()
#%% For black logger:
##myfile = '151118-115103_UG.CSV'
##mypath = r'C:\Users\eemiliosl\Documents\phd\_fielddata\raw_24112015\tracer_24112015'
##fulldataset = pcfd.load_blacklogger(mypath, myfile)
##plt.plot(fulldataset.p_ch1, 'rx')

#%% For heads:
##mywells_fiber = ['1_2', 'cmt11', 'cmt14', 'cmt32', '1_4', 'cmt41', 'cmt45', 'cmt23', 'cmt21', 'cmt46', 'cmt43', '4_4']
##savepath = r'C:\PhD_Emilio\Emilio_PhD\Field_Data\_ProcessedData\PumpingTest\hydraulic_data\02122015'
##mypath_heads = [r'C:\PhD_Emilio\Emilio_PhD\Field_Data\_RawData\Pumping\HydraulicData\2015-12-02 10-00-12',
##                r'C:\PhD_Emilio\Emilio_PhD\Field_Data\_RawData\Pumping\HydraulicData\2015-12-02 15-50-14',
##                r'C:\PhD_Emilio\Emilio_PhD\Field_Data\_RawData\Pumping\HydraulicData\2015-12-02 16-44-51',
##                r'C:\PhD_Emilio\Emilio_PhD\Field_Data\_RawData\Pumping\HydraulicData\2015-12-02 18-02-38',
##                r'C:\PhD_Emilio\Emilio_PhD\Field_Data\_RawData\Pumping\HydraulicData\2015-12-02 18-08-12',
##                r'C:\PhD_Emilio\Emilio_PhD\Field_Data\_RawData\Pumping\HydraulicData\2015-12-02 20-54-14',
##                r'C:\PhD_Emilio\Emilio_PhD\Field_Data\_RawData\Pumping\HydraulicData\2015-12-03 08-41-08']
##for ii in mypath_heads:
##    pcfd.rename_files(ii, mywells_fiber)
##
##mylist = os.listdir(mypath_heads[0])
##mylist.sort()
##for ii in mylist:
##    if 'Acquisition' in ii:
##        pass
##    else:
##        mydataset1 = pcfd.load_fiberoptic(mypath_heads[0], ii)
##        mydataset2 = pcfd.load_fiberoptic(mypath_heads[1], ii)
##        mydataset3 = pcfd.load_fiberoptic(mypath_heads[2], ii)
##        mydataset4 = pcfd.load_fiberoptic(mypath_heads[3], ii)
##        mydataset5 = pcfd.load_fiberoptic(mypath_heads[4], ii)
##        mydataset6 = pcfd.load_fiberoptic(mypath_heads[5], ii)
##        mydataset7 = pcfd.load_fiberoptic(mypath_heads[6], ii)
####
##        fullset = pcfd.join_datasets(mydataset1, mydataset2, mydataset3, mydataset4, mydataset5, mydataset6, mydataset7)
##        pcfd.savetxt_fiberoptic(savepath, ii, fullset)
##
##    # Once the final files were generated:
##myfiles = os.listdir(savepath)
##myfiles.sort()
##for ii in myfiles:
##    print (ii)
##    data = pd.read_table(os.path.join(savepath, ii), header = 0)
##    plt.plot(data.time_sec, data.myhead)
##    #plt.show()
##plt.show()

#%% Laboratory measurements:

# Load field measurements, miliq, calib curve, and blanc (CPS values)
date = '02122015'
labpath = r'C:\PhD_Emilio\Emilio_PhD\Field_Data\_Laboratory\lab_%s'%date
mybtc_path = r'C:\PhD_Emilio\Emilio_PhD\Field_Data\_ProcessedData\TracerTest\tracer_%s'%date
well = 'cmt24'
injection_time = np.loadtxt(os.path.abspath(os.path.join(labpath,'tracer_inj_%s.txt'%date)), dtype = 'S')
injection_time = injection_time.astype(str)
##pdb.set_trace()
#miliq = pd.read_table(os.path.abspath(os.path.join(labpath, 'miliq.txt')), header = 0)
blanc = pd.read_table(os.path.abspath(os.path.join(labpath, 'water_no_tracer_%s.txt'%date)), header = 0)
calibcurve = pd.read_table(os.path.abspath(os.path.join(labpath, 'calib_data_%s.txt'%date)), header = 0)
fieldsamples_cps = pd.read_table(os.path.abspath(os.path.join(labpath, 'field_samples_%s.txt'%date)), header = 0)

# Subtract the miliq from calib curve and blanc from field samples:
#calibcurve['CPS'] -= miliq['CPS'][0]
##pdb.set_trace()

for col in fieldsamples_cps.columns:
    subtraction = fieldsamples_cps[col] - blanc['CPS'][0]
    for ii in range(0, len(subtraction)):
        if subtraction[ii] < 0:
            subtraction[ii] = 0

    fieldsamples_cps[col] = subtraction
##pdb.set_trace()
# Get CPS in ln scale:
##calibcurve['CPS'] = np.log(calibcurve['CPS'])
##fieldsamples_cps = np.log(fieldsamples_cps)

# Get refined values from interpolation objects:
xnew = np.arange(calibcurve['CPS'].min(), calibcurve['CPS'].max(), 20)

# Interpolate calib curve
finterp_1 = interpolate.interp1d(calibcurve['CPS'], calibcurve['conc'], kind = 'linear')
finterp_2 = interpolate.interp1d(calibcurve['CPS'], calibcurve['conc'], kind = 'cubic')
ffit_1 = np.poly1d(np.polyfit(calibcurve['CPS'], calibcurve['conc'], 3))

ynew_interp_1 = finterp_1(xnew)
ynew_interp_2 = finterp_2(xnew)
ynew_fit_1 = ffit_1(xnew)

# Plot stuff:
plt.figure(1)
plt.plot(calibcurve['CPS'], calibcurve['conc'], 'kx')
plt.plot(xnew, ynew_interp_1, 'b-',label = 'linear')
#plt.plot(xnew, ynew_interp_2, 'r-', label = 'cubic')
plt.plot(xnew, ynew_fit_1, 'g-', label = 'polyfit')
plt.ylabel('Conc [mg/l]')
plt.xlabel('CPS')
plt.title('Lab curve fitted')
plt.grid(True)
plt.legend(loc = 'best')

# Transform CPS from field samples to real concentrations
fieldsamples_conc =  fieldsamples_cps.copy()
for col in fieldsamples_conc.columns:
    for ii in range(0,len(fieldsamples_conc[col])):
        try:
            #fieldsamples_conc[col][ii] = ffit_1(fieldsamples_cps[col][ii])
            fieldsamples_conc[col][ii] = finterp_1(fieldsamples_cps[col][ii])
        except:
            pass
##pdb.set_trace()
#Normalize data:
fieldsamples_conc_norm = fieldsamples_conc.apply(lambda x: (x - np.min(x)) / (np.max(x) - np.min(x)))

# Deal with injection and sampling times:
samp_times = pd.read_table(os.path.join(labpath, 'samp_times_%s.txt'%date), header = 0)
samp_times['inj_time'] = injection_time

for col in samp_times.columns:
    for ii in range(0,len(samp_times[col])):
        try:
            samp_times[col][ii] = pd.to_datetime(samp_times[col][ii])
        except:
            pass
##pdb.set_trace()
samp_times_sec = samp_times.copy()
for col in samp_times_sec.columns[:-1]:
    samp_times_sec[col] = (samp_times_sec[col] - samp_times_sec['inj_time']).dt.seconds
    samp_times_sec[col] = samp_times_sec[col]# -60


samp_times_sec.to_csv(os.path.join(labpath, 'sampletimes_seconds_%s.txt'%date), float_format = '%.3f',sep = '\t')

#%% Check each breakthorugh and the calculated concentrations
samp_times_sec = samp_times_sec[0:-3]
fieldsamples_conc = fieldsamples_conc[0:-3]


##pdb.set_trace()
timestr = '%s_t0'%well
btc_file = '%s_modif.txt' %well
btc = np.loadtxt(os.path.join(mybtc_path,btc_file))
btc[:,0] = np.arange(0,len(btc[:,0]), 1)
# (opt 0) Normalize and scale by the max of lab measurements:
yfit1 = pcfd.interp_y(btc[:,1], samp_times_sec, fieldsamples_conc, well, mytype = 'norm' )

#%% (opt 1) Attemp to make an fmin search minimizing the distance between points
yfit2 = pcfd.interp_y(btc[:,1], samp_times_sec, fieldsamples_conc, well, mytype = 'fmin' )

#%% (opt 2) Perform a standard linear regression to get actual concentratons for all btc
yfit3 = pcfd.interp_y(btc[:,1], samp_times_sec, fieldsamples_conc, well, mytype = 'lin' )

#%% (opt 2) Perform a standard linear regression to get actual concentratons for all btc
yfit4 = pcfd.interp_y(btc[:,1], samp_times_sec, fieldsamples_conc, well, mytype = 'ls_nointerc' )


##plt.figure(3)
##plt.plot(btc[samp_times_sec[timestr],-1], fieldsamples_conc[well], 'o', label = 'regression line conc-signal')
##plt.plot(xnewtemp, ffit_conc_signal(xnewtemp))
##plt.legend(loc = 'best')
##pdb.set_trace()
plt.figure(2)
plt.plot(samp_times_sec[timestr],fieldsamples_conc[well],'bo')

plt.plot(btc[:,0],yfit1, 'r--', label = 'fit by factor=max(conc)')
plt.plot(btc[:,0],yfit2, 'b--', label = 'fimin search fit')
plt.plot(btc[:,0], yfit3, 'k--',label = 'regression fit')
plt.plot(btc[:,0], yfit4, 'c--',label = 'ls no interc')
plt.legend(loc = 'best')
plt.grid(True)
plt.show()
##pdb.set_trace()
tosave = yfit4    # btc[:,1]*facmin[0], normdata*np.max(fieldsamples_conc[well]), ynew_fit_2
np.savetxt(os.path.join(mybtc_path, btc_file.split('.')[0]+'_conc.txt'), np.transpose((btc[:,0], tosave)), fmt = '%i %6.5e', delimiter = '\t')

print('concentration file < %s > saved!'%os.path.join(mybtc_path, btc_file.split('.')[0]+'_conc.txt'))

