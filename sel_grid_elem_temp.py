import numpy as np
import matplotlib.pylab as plt
import os
import grid_manip as gm
import hgs_grok as hgs
import pycleaner.myplot as pcplt
# import sys
# sys.path.append(r'C:\Users\eemiliosl\Documents\phd\_codedev\kalmanfilter_git\myModels\_fl_synthetic')


mydir = os.path.join(os.curdir,'..', 'kalmanfilter_git', 'myModels', 'model_fortestingfunc')     # _fl_synthetic'

kkk = np.loadtxt(os.path.join(mydir, 'kkkGaussmall.dat'), usecols=(-1,))

nnodes, nelements, nodes, elements = gm.readmesh(os.path.join(mydir, 'meshtecplot.dat'), fulloutput = True)

# Settings for the wanted slice:
slice_coor = 5
fix_ax = 'x'    # 'y' or 'z'

# Find the maximum coordinate values in the x, y and z directions:
xmax = np.max(nodes[:, 0])
ymax = np.max(nodes[:, 1])
zmax = np.max(nodes[:, 2])

# Number of nodes in each grid direction:
xf_id = np.where(nodes[:, 0] == xmax)[0][0]
yf_id = np.where(nodes[:, 1] == ymax)[0][0]
zf_id = np.where(nodes[:, 2] == zmax)[0][0]

nodes_perlayer = yf_id + xf_id + 1

nx_nodes = xf_id + 1    # Number of x nodes == xmax +1
ny_nodes = nodes_perlayer / nx_nodes
nz_nodes = nnodes / (nx_nodes * ny_nodes)
# num_elements == num_nodes - 1

# Find index of strategic nodes:
# If x is fixed:
x0_id = np.where(nodes[:, 0] == slice_coor)[0][0]   # node id of the first node at slice position

sel_nodes = nodes[x0_id::nx_nodes]  # This is not really needed, just a test
# Load nodal data:
myheads, t_step = hgs.readHGSbin(os.path.join(mydir, 'fl_simo.head_pm.004'), nnodes)
formatted_heads = np.flipud(np.reshape(myheads[x0_id::nx_nodes], (-1, ny_nodes)))

plt.figure(1)
plt.imshow(formatted_heads, extent=(0, ymax, 0, zmax))
plt.colorbar()
# plt.xlim([35, 65])
# plt.xticks(sel_nodes[:, 1], fontsize=9)
# plt.yticks(sel_nodes[:, 2], fontsize=9)

# if z is fix:
z0_id = np.where(nodes[:, 2] == 6)[0][0]
sel_nodes = nodes[z0_id:z0_id + nodes_perlayer]
formatted_heads = np.flipud(np.reshape(myheads[z0_id:z0_id + nodes_perlayer], (ny_nodes, -1)))
plt.figure(2)
plt.imshow(formatted_heads, extent=(0, xmax, 0, ymax))
plt.colorbar()

# If y is fix:
y0_id = np.where(nodes[:, 1] == 5)[0][0]

y_idx = np.array([])
for ii in range(int(nz_nodes)):
    cur_idx = np.arange(y0_id + nodes_perlayer * ii, y0_id + nx_nodes + nodes_perlayer * ii, 1)
    y_idx = np.append(y_idx, cur_idx)

sel_nodes_y = nodes[y_idx.astype('int'), :]

formatted_heads = np.flipud(np.reshape(myheads[y_idx.astype('int')], (-1, nx_nodes)))
plt.figure(3)
plt.imshow(formatted_heads, extent=(0, xmax, 0, zmax))
plt.colorbar()

plt.show()

# Elemental selection:
# Fix x:
formatted_kkk = np.flipud(np.reshape(kkk[x0_id::nx_nodes-1], (-1, ny_nodes-1)))
plt.imshow(formatted_kkk, extent=(0, ymax, 0, zmax), interpolation = 'nearest', clim=(0.0004, 0.006))
plt.colorbar()

# Fix z:
plt.figure(5)
formatted_kkk = np.flipud(np.reshape(kkk[z0_id:z0_id + ((nx_nodes-1)*(ny_nodes-1))], (ny_nodes-1, -1)))
plt.imshow(formatted_kkk, extent=(0, xmax, 0, ymax), clim(0.0004, 0.006))
plt.colorbar()

# Fix y:
# y_idx_el = np.array([])
# for ii in range(int(nz_nodes-1)):
#     cur_idx = np.arange(y0_id + nodes_perlayer * ii, y0_id + nx_nodes + nodes_perlayer * ii, 1)
#     y_idx_el = np.append(y_idx, cur_idx)
#
# formatted_heads = np.flipud(np.reshape(myheads[y_idx_el.astype('int')], (-1, nx_nodes)))
# plt.figure(3)
# plt.imshow(formatted_heads, extent=(0, xmax, 0, zmax))
# plt.colorbar()


sel_values = pcplt.plot2dsections(os.path.join(mydir, 'meshtecplot.dat'), myvalues=kkk, typedata='elemental',
                                  xlim=np.array([0, 10]),
                                  ylim=np.array([5.3, 5.6]), zlim=np.array([0, 10]), dx=1, dy=1,
                                  scalemin=0.0004, scalemax=0.006,
                                  saveim=False, image_output='interp1.png',
                                  savetxt=False, ascii_output='interp1.txt')
print('pause')