#-------------------------------------------------------------------------------
# Name:        adaptiveFilters.py
# Purpose:     Adaptive filters, adapted from Trauth, 1998
# Author:      eemiliosl
# Created:     28.03.2016
# Copyright:   (c) eemiliosl 2016
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import numpy as np, os, sys, scipy.signal as sss
import scipy.fftpack as ssf, matplotlib.pyplot as plt
import scipy.linalg as ssl

def main():
##    xData = np.linspace(0, 100, 100/0.1, endpoint = True)
##    yData = np.sin(xData)
##    Data1 = np.loadtxt (r'C:\PhD_Emilio\_Schools\_Info_biogeoscis_Africa_15_16\working_material\filters_phd_Trauth\mtlb_toolbox_adaptivefilters')
    x = np.arange(0, 100+0.1, 0.1)
    yData = np.sin(x)
    np.random.seed(0)
    yn1 = yData + 0.5*np.random.randn(len(yData))
    yn2 = yData + 0.5*np.random.randn(len(yData))
    #plt.plot(x, yn1, 'r-', x, yn2, 'k-')

    k = ssl.kron(yn1[:,np.newaxis], yn1[np.newaxis, :])
    u = 1/np.max(ssl.eig(k))
    filtlngth = 11
    n_iter = 10

    varsal1, varsal2, varsal3, varsal4 = canc(yn1, yn2, u, filtlngth, n_iter)



def canc(s1, s2, u, filtlen, iters):

    #%% Quick formatting of the arrays
    n1, n2 = s2.shape
    n = n2
    index = 0
    if n1 > n2:
       s2 = s2.T
       s1 = s1.T
       n = n1
       index = 1

    #%% Initialize filter
    w = np.zeros((1,filtlen))
    e = np.zeros((1,n))

    ss1 = np.zeros((1,filtlen))
    ss2 = np.zeross((1,filtlen))

    z = np.zeros((1,n))
    y = np.zeros((1,n))
    ors = s2
    ms = np.mean(s2)* np.ones((1,n))
    s2 = s2 - ms
    s1 = s1 - ms
    ors = ors-ms

    #%% Start iterations NOTE: HAVE TO CHECK ZERO INDEX OF PYTHON
    for cur_iter in range(1, iters,1):

        for I in range(filtlen+1, n+1, 1):  # Filter loop

            for k in range(1, filtlen):
                xx[k] = x[I-k]
                ss[k] = s[I-k]

            for J in range(1,filtlen):
                ww[I-1,J] = w[J]
                y[I-1] = y[I-1] + w[J]*xx[J] # Has to be element by element product
                z[I-1] = z[I-1] + w[J]*ss[J] # Has to be element by element product

            e[I-1] = ors[I-1-(np.fix(filtlen/2.))] - y[I-1]

            for J in range(1,filtlen):
                w[J] = w[J] + 2.*u*e[I-1]*xx[J] # Has to be element by element product

        # % Ends filter loop

        for I in range(1, n): # Phase correction
            if I <= np.fix(filtlen/2.):
                yy[I]=0
                zz[I]=0
                ee[I]=0
                ww[I,:]=0
            elif I > n-np.fix(filtlen/2.):
                yy[I]=0
                zz[I]=0
                ee[I]=0
                ww[I,:]=0

            else:
                yy[I] = y[I + np.fix(filtlen/2)]
                zz[I] = z[I + np.fix(filtlen/2)]
                ww[I,:] = ww[I + fix(filtlen/2),:]
                ee[I] = np.abs(e[I + np.fix(filtlen/2)])

            yy[I] = yy[I] + ms[I]
            zz[I]=zz[I] + ms[I]
        #% Ends phase correction
        y[1:n] = np.zeros(n,)
        z[1:n] = np.zeros(size(n,))
        mer[it] = np.mean(ee[(np.fix(filtlen/2.)):(n-np.fix(filtlen/2.))]**2);
    #% End iterations

    if index == 1: #% Reformatting
        zz=zz.T
        yy=yy.T
        ee=ee.T



    print('A')

if __name__ == '__main__':
    main()
