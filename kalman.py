""" Functions available to perform Kalman Filter inversion"""

import os, time, numpy as np, datetime, shutil, sys, scipy.stats as sst, platform
import multiprocessing as mp
from itertools import repeat
import hgs_grok as hgr
from numpy.linalg import inv, solve
import dir_manip as dm
import matmult.mylikelihood as mml
import matmult.matmul_opt as mm
import grid_manip as gm


#   1- acc_rej(Likelihood_new, Likelihood_old,chi2, acceptedReal, acc_flag)
#   2- sequential_update(ii, R, Kgain, Y, nreal, nmeas, fieldmeas, modmeas, E, Y_old, acceptedReal, dY_max, dY_min, heads, concentrations)
#   3- readLastIterData(Inv_name, myType = '',my_timest = '')
#   4- checkLastIterFiles(Inv_name,my_timest = '')
#   5- expshr_param(ii, mode, Y, kkkFile, orig_indices, sel_indices, meanVal = '', what= '')
# -------------------------------------------------------------------------------

def acc_rej(Likelihood_old, Likelihood_new, chi2, acc_flag=False):
    """ Accept/reject scheme to update parameter fields in the EnKF
    Arguments:
        likelihood_new:
        likelihood_old:
        chi2:
        acc_flag:               bool, True if realization is accepted
    Returns:
        accept:                 bool, True if realization is accepted
        restart_flag:           bool, True if needed to restart the realization
    """
    # %% 1st Criteria: Evaluate the objective function:
    # -> Realization can be accepted if likelihood decreases:
    if Likelihood_new < Likelihood_old:
        acc_flag = True

    # %% 2nd Criteria: If chi2 < random number of range [0,1]
    # -> the realization can be accepted:
    elif chi2 < np.random.uniform(low=0.0, high=1.0, size=1).squeeze():
        acc_flag = True

    # Set the proper flag banner for restarting options:
    if acc_flag == True:
        restart_flag = False
    elif acc_flag == False:
        restart_flag = True

    return acc_flag, restart_flag


def step_control(Y_term_upd, dY_max):
    """ Step controler for updating the parameter field
    Arguments:
        Y_term_upd:
        dY_max:
    Returns:
        Y_term_upd:     np.array, controlled Y_term_upd
    """
    dY = np.max(np.abs(Y_term_upd))
    while dY > dY_max:
        Y_term_upd = Y_term_upd / 2.
        dY = np.max(np.abs(Y_term_upd))

    return Y_term_upd


def res_likelihood(field_data, model_data, E, R, parmatmult, mymethod='solve'):
    # %% Compute residuals between field measurements and modeled measurements:
    # Delta = fieldmeas - (modmeas_i + E_i),  (nmeas x 1)
    residual = field_data.squeeze() - (model_data + E)  # or: res_old = fieldmeas.squeeze() - (modmeas[:,ii] + E)
    try:
        residual.shape[1]
    except:  # correct the lack of one dimension...
        residual = residual[:, np.newaxis]

    # %% Compute Likelihood (1 x 1) (likelihood = Obj.func = chi2 dist because they are assumed independent and standard normal):
    #  L = (y-f(x))' R-1 (y-f(x)) ===> (fieldmeas-modeledmeas)' * inverse(PriorCovMatrix_of_MeasError) * (fieldmeas-modeledmeas)
    Likelihood = mml.likelihood(residual, R.toarray(), parall=parmatmult, method=mymethod)

    return residual, Likelihood


def update_step(ii, homedir, nreal, nmeas, nnodes, elements, modeltimes, biggrid_elem_ids,
                smallgrid_elem_ids, smallgrid_nodes, fieldmeas, modmeas, R, Kgain, Y,
                Y_old, E, dY_max, dY_min, mymode='fl_', curtimestep=0, parmatmult=False, mytype='single', ini_head = 7):
    """ Model parameter field update.
    Arguments:
        ii:                 Int, realization number (ID)
        homedir:            str, home directory. Where all the project is stored (e.g. ..../kalman_filter_git)
        nreal:              int, total number of realizations
    R:                  PriorCovMatrix_of_MeasError
    Kgain:
    Y:

    nmeas:
    fieldmeas:          np.array (nmeas x 1), field measurements to be used in the data assimilation
    modmeas:            np.array (nmeas x 1), modeled measurements to be compared with tthe field data
    E:                  np.array (nmeas x 1), random measurement error from ~ N(0, R). R = covmat (diag) of meas error
    Y_old:              np.array (nelem x 1), old parameter values
    acceptedReal:
    dY_max:
    dY_min:
    heads:
    concentrations
        Returns:
        --------
    Y_i:                Numpy array, updated parameter field for the specified realization
    modmeas_new:        Numpy array, updated model states
        Dependencies:
        -------------

    """
    # %% Directories and necessary data from the grid:
    # ---------------#
    NoReal, dir_dict, fmt_string, kfile = dm.init_dir(ii, mymode, homedir)
    # kkkFile = os.path.join(dir_dict['fld_mode'], kfile)

    # %% Start the process:
    # ---------------#
    trials = 0
    restart_flag = True

    while restart_flag is True:

        # %% Compute residuals and likelihoods:
        res_old, Likelihood_old = res_likelihood(fieldmeas, modmeas, E, R, parmatmult, mymethod='solve')

        # Compute  Kalman gain * Residuals ===> Qsy (Qyy + R)-1 [Yo - (Yui + Ei)]
        # Y_term_upd = (Qym.dot(np.linalg.inv(Qmm_new))).dot(res_old)  # (nelements x 1)
        if parmatmult is False:
            Y_term_upd = np.dot(Kgain, res_old)
        elif parmatmult is True:
            Y_term_upd = mm.matmul_locopt(Kgain, res_old, ncpu=1)

        # %% Apply the step control and update it (according to Eq 3 Schoniger 2012):
        # s_i,c (or Y_i,c) = Y_i,u + Y_term_upd
        Y_term_upd = step_control(Y_term_upd, dY_max)
        Y_i = Y[:, ii][:, np.newaxis] + Y_term_upd

        # %% Update state variables (model measurements) with forward model run
        # Transform Y to normal space [m/s] for forward simulation

        if curtimestep == len(modeltimes) - 1:
            storebin = True
        else:
            storebin = False
        # sel_indices = np.subtract(np.loadtxt(os.path.join( fld_mode,'supfile_elements.dat'),usecols =(0,)),1).astype(int)
        modmeas_new = hgr.getstates(homedir, ii, mymode, nnodes, elements, modeltimes, biggrid_elem_ids,
                                    smallgrid_elem_ids,
                                    inner_gr_nodes=smallgrid_nodes, Y_i_upd=np.exp(Y_i),
                                    curtimestep=curtimestep, storebinary=storebin, mytype=mytype, initial_head=ini_head)

        restart_flag = False
        # #%% Calculate new residuals and likelihood:
        # res_new, Likelihood_new = res_likelihood(fieldmeas, modmeas_new, E, R, parmatmult, mymethod = 'solve')
        #
        # #%% Initialize the acceptance/rejection scheme:
        # chi2 = sst.chi2.cdf(Likelihood_new,nmeas)   # Compute critical CDF value
        # acc_flag, restart_flag = acc_rej(Likelihood_old, Likelihood_new, chi2) # by default acc_flag= False
        #
        # # If realization not accepted, old parameters are adjusted with more detail
        # if acc_flag == False:
        #     Y_i = np.copy(Y[:,ii]) #Y_old # Take again the old parameters, is this ok? or better Y_old as was before. Maybe I do not need Y_old at all
        #
        # while acc_flag == False:
        #     print('Realization %d not accepted. Trying again...' %(NoReal))
        #
        #     Y_term_upd = step_control(Y_term_upd, dY_max) # Before there was dY_min
        #
        #     # Update parameter field: s_i,c or Y_i,c = Y_i,u + Y_term_upd
        #     #Y_i = Y_i + Y_term_upd
        #     try:
        #         Y_i.shape[1]
        #     except:
        #         Y_i = Y_i[:,np.newaxis]
        #     Y_i = Y_i + Y_term_upd
        #     # Update state variables (or model measurements)...
        #     modmeas_new = hgr.getstates(homedir, ii, mymode, nnodes, elements, modeltimes, biggrid_elem_ids, smallgrid_elem_ids,
        #                                 inner_gr_nodes = smallgrid_nodes, Y_i_upd = np.exp(Y_i),
        #                                 curtimestep = curtimestep, storebinary = storebin, mytype = mytype)
        #
        #     # Calculate new residuals and likelihood:
        #     res_new, Likelihood_new = res_likelihood(fieldmeas, modmeas_new, E, R, parmatmult, mymethod = 'solve')
        #
        #     # Compute the critical CDF value for acceptance or rejection
        #     chi2 = sst.chi2.cdf(Likelihood_new,nmeas)
        #     acc_flag, restart_flag = acc_rej(Likelihood_old, Likelihood_new, chi2) # by default acc_flag= False
        #
        #     #%% If not yet accepted, restart updating modifying Y using random factors between 0-1
        #     if restart_flag == True:
        #         trials = trials + 1
        #         print ('Realization %d is being restarted' %(ii+1))
        #
        #         seed = np.random.uniform(low=0.1, high=0.9, size=(nreal,1))
        #         #seed = np.random.uniform(low=0.25, high=0.75, size=(len(Y_i),1))
        #         seed = seed/np.sum(seed)
        #
        #         if parmatmult is False: Y_temp = np.dot(Y,seed)
        #         elif parmatmult is True: Y_temp = mm.matmul_locopt(Y, seed, ncpu = 1)
        #         Y[:,ii] = Y_temp.squeeze()
        #         #Y[:,ii] = (Y_i*seed).squeeze()
        #
        #         if trials == 5: # If a certain number of trials has been attemped, restart from initial values
        #             print ('Too many trials. Taking initial parameter values again...')
        #             Y[:,ii] = np.copy(Y_old)
        #             if parmatmult is False: Y_term_upd = np.dot(Kgain, res_old)
        #             elif parmatmult is True: Y_term_upd = mm.matmul_locopt(Kgain, res_old, ncpu = 1)
        #             restart_flag = False
        #             acc_flag = True
        #
        #         # forward simulation... get new modmeas
        #         modmeas = hgr.getstates(homedir, ii, mymode, nnodes, elements, modeltimes, biggrid_elem_ids, smallgrid_elem_ids,
        #                                 inner_gr_nodes = smallgrid_nodes, Y_i_upd = np.exp(Y[:,ii]),
        #                                 curtimestep = curtimestep, storebinary = storebin, mytype = mytype)
        #         Y_i = Y[:,ii]
        #
        if restart_flag is False:  # or ac_flag == True would also work
            print('Model measurements of realization %d updated.' % NoReal)

    return Y_i, modmeas_new


def caller_kfupd_parall(homedir, mymode, type_update, num_modmeas, nnodes, elements, dY_max, dY_min,
                        modmeas, Y, current_time, thetimes, biggrid_elem_ids, smallgrid_elem_ids, smallgrid_nodes,
                        selfieldmeas, R, Kgain, Y_old, E, headsFile='', parallel=False, cpus='', parmatmult=False,
                        initial_head=7.0):
    nel, nreal = Y.shape
    modmeas_upd = np.zeros(modmeas.shape)
    Y_new = np.zeros(Y.shape)

    if parallel is False:
        for zz in range(0, nreal):
            Y_new[:, zz], modmeas_upd[:, zz], zz_idx = kfupd_parall(zz, homedir, nreal, num_modmeas, nnodes, elements,
                                                                    thetimes, biggrid_elem_ids,
                                                                    smallgrid_elem_ids, smallgrid_nodes, selfieldmeas,
                                                                    modmeas, R, Kgain, Y, Y_old, E,
                                                                    dY_max, dY_min, mymode, current_time, headsFile,
                                                                    parmatmult, type_update, initial_head)
            assert zz_idx == zz, 'Oh oh, it seems there is a mess in the tracking system of realizations!!!'

    elif parallel is True:
        mypool = mp.Pool(cpus)
        full_results = mypool.starmap(kfupd_parall,
                                      zip(np.arange(0, nreal, 1), repeat(homedir), repeat(num_modmeas),
                                          repeat(nnodes), repeat(elements), repeat(thetimes), repeat(biggrid_elem_ids),
                                          repeat(smallgrid_elem_ids), repeat(smallgrid_nodes), repeat(selfieldmeas),
                                          repeat(modmeas),
                                          repeat(R), repeat(Kgain), repeat(Y), repeat(Y_old), repeat(E), repeat(dY_max),
                                          repeat(dY_min),
                                          repeat(mymode), repeat(current_time), repeat(headsFile), repeat(parmatmult),
                                          repeat(type_update), repeat(initial_head)))
        mypool.close()
        mypool.join()
        # Here I have to manipulate the list to get the proper numpy arrays

        for xx in range(0, len(full_results)):
            Y_new[:, xx] = np.asarray(full_results[xx][0])
            modmeas_upd[:, xx] = np.asarray(full_results[xx][1])
            assert xx == int(
                full_results[xx][-1]), 'Oh oh, it seems there is a mess in the id tracking system of realizations!!!'

    return Y_new, modmeas_upd


def kfupd_parall(tt, homedir, nreal, num_modmeas, nnodes, elements, thetimes, biggrid_elem_ids,
                 smallgrid_elem_ids, smallgrid_nodes, selfieldmeas, modmeas, R, Kgain, Y, Y_old, E,
                 dY_max, dY_min, mymode, current_time, headsFile, parmatmult, type_update, initial_head):
    mypath_dict = dm.gendirtree(homedir, proc_idx=tt + 1, mode=mymode)

    if not os.listdir(mypath_dict['fld_mode']):
        # hgr.prepfwd(ii, homedir, mymode, headsFile = 'finalheadsFlow_%.5d.dat'%(ii+1))
        hgr.prepfwd(tt, homedir, mymode, headsFile=headsFile)

    hgr.addt2grok(tt + 1, homedir, mypath_dict, current_time, thetimes, mode=mymode, mytype=type_update)

    Y_tt, modmeas_upd_tt = update_step(tt, homedir, nreal, num_modmeas, nnodes, elements, thetimes, biggrid_elem_ids,
                                       smallgrid_elem_ids, smallgrid_nodes, selfieldmeas, modmeas[:, tt], R, Kgain, Y,
                                       Y_old[:, tt], E[:, tt], dY_max, dY_min, mymode=mymode, curtimestep=current_time,
                                       parmatmult=parmatmult,
                                       mytype=type_update, ini_head=initial_head)

    return Y_tt.squeeze(), modmeas_upd_tt, tt


def read_iter_data(mydir, myType='', my_timest=''):
    """
    mydir:       Str, name of the project(folder) containing the results of the inversion
    myType:         Str, type of data to load. Param -> 'Y'; ModelOutputs -> 'ModMeas'; CovMatrix -> 'Qmy'
    my_timest:      Str, number of time step to take data from
    what:           Str, shrink, expand or nothing to do with the stored data
    """
    if not os.path.exists(mydir):
        sys.exit('Directory where data is stored has been set incorrectly!')

    if myType == '':
        sys.exit('No type of data defined. Restarting is not possible!')
    lst_temp = os.listdir(mydir)
    lst_temp.sort()
    lst = []

    # First filter of the strings: get those corresponding to the type of data requested (e.g. Model outputs)
    for ss in lst_temp:
        if ss.startswith(myType):
            if my_timest in ss:
                lst.append(ss)
    try:
        lst = lst[-1]
    except:
        print('Wrong cur time definition, no data was loaded!')

    if '.npy' in lst:
        mydata = np.load(os.path.join(mydir, lst))
    if '.txt' in lst:
        mydata = np.loadtxt(os.path.join(mydir, lst))
    if '.dat' in lst:
        mydata = np.loadtxt(os.path.join(mydir, lst))

    return mydata


def checkLastIterFiles(Inv_name, my_timest=''):
    """
    Inv_name:       Str, name of the project(folder) containing the results of the inversion
    my_timest:      Str, number of time step to take data from
    """

    KalmanFilterPath = 'kalmanfilter'
    # %% Define directories:
    mainPath = os.path.dirname(__file__)
    mydataFiles = os.path.abspath(os.path.join(mainPath, '..', KalmanFilterPath, 'Realizations', Inv_name))

    if not os.path.exists(mydataFiles):
        sys.exit('Directory where data is stored has been set incorrectly!')

    lst_temp = os.listdir(mydataFiles)
    lst_temp.sort()
    lst = []
    lst2 = []
    myiter = []
    # get a list of results stored from the last iteratiuon and last time step:
    for myid, ss in enumerate(lst_temp):
        if my_timest in ss:
            lst.append(ss)
            myiter.append(int(ss.split('_')[1].split('iter')[1]))
    if len(lst) > 3:
        for ii in lst:
            if ('%.3d' % (max(myiter))) in ii:
                lst2.append(ii)
        if len(lst2) == 3:
            myflag = 'Done'

    elif len(lst) == 3:
        myflag = 'Done'

    else:
        if len(lst) == 1:
            assert 'ModMeas' in lst[0], 'Unexpected error'
            myflag = 'NotDone'

    return 'Done', ('%.3d' % (max(myiter)))

##def updateStates(NoReal,Y_i, mytype):
##    """ Purpose: Process to update states with updated parameters using a forward
##    model run and the corresponding realization
##    Arguments:
##        NoReal:     Int, identifier of the realization being dealt with
##        Y_i:        Array, updated parameter field used to perform a new model run for new states
##        type:       Str, flag to define wether is the reference model or the update stage
##    Returns:
##        Array with updated modeled measurements or states
##    """
##    NoReal = NoReal + 1     # To correct the zero index from python
##    fmt_string = '%.5d' %(NoReal)
##    #%% Generate new kkk files to be used by GROK and HGS in the proper folder
##    # (search for the process folder with corresponding index, if not exists, then create it):
##    curKFile = 'kkkGaus_%.5d.dat' %NoReal
##    identifier = 'fl_'
##
##
##         # For flow (all elements):
##         #-----------------------------------------------------------------------------------------------#
##    myDir2RunFlow = os.path.join(processesPath,'process_%.5d' %(NoReal), identifier +'%.5d' %(NoReal))
##    outputFile = os.path.join(myDir2RunFlow, curKFile)
##
##    try: # If the file cannot be created is because the proper folder does not exist, hence create it:
##        np.savetxt(outputFile,np.transpose((np.arange(1,len(Y_i)+1,1),Y_i,Y_i,Y_i)), fmt='%d %.6e %.6e %.6e')
##    except:
##        Source_Folder = '_Flow_ToCopy'
##        GrokStr = 'Flowsim'
##        mode = 'flow'
##        allprocPath = processesPath
##        mainPath = KalmanFilterPath
##        curKFile, myDir2RunFlow = HGS.fwdprepare(Source_Folder, GrokStr, mainPath,allprocPath, fmt_string, identifier, mode)
##        np.savetxt(outputFile,np.transpose((np.arange(1,len(Y_i)+1,1),Y_i,Y_i,Y_i)), fmt='%d %.6e %.6e %.6e')
##
##    # Run Flow to get steady heads:
##    HGS.fwdHGS(myDir2RunFlow)
##    # Extract modeled heads:
##    myouttimes = np.loadtxt(os.path.join(KalmanFilterPath,'Model_data','_outputtimesFlow.dat'))
##    commonName = 'observation_well_flow'; fullPath = True; onlylastFile= False;
##    listOfFiles = mandir.getCommonStrDirs(myDir2RunFlow, commonName, fullPath, onlylastFile)
##    selHeads_All = np.array([])
##    for ii in listOfFiles:
##        selHeads = HGS.readModelMeas(ii,myouttimes.astype(np.int))
##        selHeads_All = np.append(selHeads_All,selHeads) # All selected heads
##
##        # For transport (inner Grid only):
##        #----------------------------------------------------------------------------------------------------#
##    identifier = 'tr_'
##    myDir2RunTrans =  os.path.join(processesPath,'process_%.5d' %(NoReal), identifier + '%.5d' %(NoReal))
##    headsFile1 = 'finalheadsFlow_%.5d.dat'%NoReal
##
##    if os.path.exists(myDir2RunTrans) == False:
##
##        Source_Folder = '_Transport_ToCopy'
##        GrokStr = 'Transpsim'
##        mode = 'transport'   # transport, heat
##        mainPath = KalmanFilterPath
##        allprocPath = processesPath
##        curKFile, myDir2RunTrans = HGS.fwdprepare(Source_Folder, GrokStr, mainPath, allprocPath,fmt_string, identifier, mode, FlowDir = myDir2RunFlow, headsFile = headsFile1)
##
##        # Extract final heads and kkk for the transient model (small grid) from the flow run (big grid):
##    myDir = os.path.join(KalmanFilterPath,'Model_data','_RandFieldsParameters.dat')
##    xlen, ylen, zlen, nxel, nyel, nzel, xlim, ylim, zlim, Y_model, Y_var, beta_Y, lambda_x, lambda_y, lambda_z, elX_size, elY_size, elZ_size = Genfi.readParam(myDir)
##
##    inputGridFile = os.path.join(myDir2RunFlow ,'meshtecplot.dat')
##    inputFile_KKK = os.path.join(myDir2RunFlow,curKFile)
##    outputFile_KKK = os.path.join(myDir2RunTrans, curKFile)
##
##    commonName = 'head_pm'; fullPath = True; onlylastFile = True
##    myHeadsFileIn = mandir.getCommonStrDirs(myDir2RunFlow, commonName, fullPath, onlylastFile)
##    myHeadsFileOut = os.path.join(myDir2RunTrans, headsFile1)
##
##    supportingFileElem = os.path.join(myDir2RunTrans, 'supfile_elements.dat')
##    supportFileNodes = os.path.join(myDir2RunTrans, 'supfile_nodes.dat')
##
##    nodes = int(mandir.findStringFile(inputGridFile, 'N='))
##    elements = int(mandir.findStringFile(inputGridFile, 'E='))
##
##    Gridf.SelectInnerGrid(inputGridFile, inputFile_KKK, outputFile_KKK, myHeadsFileIn, myHeadsFileOut,
##                    supportingFileElem, supportFileNodes, nodes, elements, elX_size, elY_size,
##                    elZ_size, xlim, ylim, zlim)
##
##    #%% Run transport model:
##    myDir2Run = myDir2RunTrans
##    HGS.fwdHGS(myDir2Run)
##
##        # Extract modeled concentrations at predefined output times and store them as binary file for python:
##    myouttimes = np.loadtxt(os.path.join(KalmanFilterPath,'Model_data','_outputtimesTrans.dat'))
##    commonName = 'observation_well_conc'; fullPath = True; onlylastFile= False;
##    listOfFiles = mandir.getCommonStrDirs(myDir2RunTrans, commonName, fullPath, onlylastFile)
##    selConc_All = np.array([])
##    for ii in listOfFiles:
##        selConc = HGS.readModelMeas(ii,myouttimes.astype(np.int))
##        selConc_All = np.append(selConc_All,selConc) # All selected concentrations
##
##
##    modmeas_new = np.r_[selHeads_All,selConc_All]
##
##    #%% Remove all created files and copy just those of interest (to save memory space) for both flow and transport:
##    commName = np.array(['.grok','.pfx','kkkGaus','.mprops','letmerun','wprops',headsFile1,'supfile','finalheads','parallelindx', 'mod']) #'meshtecplot'
##    newFolders = [myDir2RunFlow,myDir2RunTrans]
##    myPaths = [myDir2RunFlow + 'temp',myDir2RunTrans+'temp']
##
##    for yy in newFolders:
##        os.rename(yy, yy+'temp')
##        os.makedirs(yy)
##
##    for jj in range(0,len(myPaths)):
##        for curname in commName:
##            myPath = myPaths[jj]; commonName = curname; fullPath = False; onlylastFile = False
##            myfilesflow = mandir.getCommonStrDirs(myPath, commonName, fullPath, onlylastFile)
##
##            for ii in myfilesflow:
##                shutil.copy(os.path.join(myPath,ii), os.path.join(newFolders[jj],ii))
##
##        shutil.rmtree(myPath)
##
##    return modmeas_new
##
###    np.save(outputFileBin, np.transpose((np.exp(Ybasket))))
##
##
