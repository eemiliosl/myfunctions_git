""" Functions to Manipulate directories """
import shutil
import os
import numpy as np
import re
import time
import pdb


def homedir():
    return os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))


def file_info(myfile):
    """
    Exctract deault metadata from any file. ie creation/accessed/modified time, size
    Args:
        myfile:         str, filename to which extract metadata

    Returns:
        creation_time, lastmodified, lastaccessed string formatted
        filesize integer in bytes
    """
    info = os.stat(myfile)
    creation_time = time.asctime(time.localtime(info.st_ctime))
    lastmodified = time.asctime(time.localtime(info.st_mtime))
    lastaccessed = time.asctime(time.localtime(info.st_atime))
    filesize = info.st_size

    return creation_time, lastmodified, lastaccessed, filesize


def getdirs(mydir, mystr=False, fullpath=False, onlylast=False):
    """
    Get a list of folders or files in a defined directory, all wih a common string
        Arguments:
        ----------
    mydir:          str, dir of interest
    mystr:          str, common string of files. False if none
    fullpath:   bool, whether to attach full directory to file names or not
    onlylast:   bool, whether to return only the last value of the list or not
        Returns:
        --------
    mylist:         np.array type string with all folders or files of interest
    mydir:          list, directory explored
    """
    if os.path.isdir(mydir):
        mylist = os.listdir(mydir)
    else:
        mydir = str(input('Dir doesnt exist. Type a valid one: '))
        mylist = os.listdir(mydir)  # Here I can use the function to constrain user input
    new_list = []
    mylist.sort()

    # %% If filter names according to a string:
    if mystr:
        for ii in mylist:
            if mystr in ii:
                if fullpath is True:
                    new_list.append(os.path.join(mydir, ii))
                else:
                    new_list.append(ii)
        mylist = np.copy(np.array([new_list]))

    elif not mystr:
        if fullpath is True:
            for ii in mylist:
                new_list.append(os.path.join(mydir, ii))

            mylist = np.copy(np.array([new_list]))
    mylist.sort()
    new_list.sort()

    mylist = mylist.flatten()
    if onlylast is True:
        return mylist[-1], mydir
    elif not onlylast:
        return mylist, mydir


def gendirtree(homedirectory, proc_idx=-9999, mode='fl_'):
    """
    Create directory tree if does not exist, and return paths as a tuple. If individual
    folders exists, they are not overwritten. Construct also dictionary for better handling the paths
        Arguments:
        ----------
    homedir:        str, host directory
    proc_idx:       float, index of the current realization. Default is -9999 no process
                    folder and its subfolders are created
    mode:           string, either fl_ or tr_. Default 'fl_'
        Returns:
        --------
    Dictionary with the name and their corresponding paths.
    If dirtree was already created it can still create individual processes folders
    """
    assert os.path.isdir(homedirectory), 'Wrong home directory'
    fmt_string = '%.5d' % proc_idx
    fld_real = 'realizations'
    fld_allproc = 'allproc'
    fld_curproc = 'proc_%s' % fmt_string
    fld_heads = 'hydheads'
    fld_kkkfiles = 'kkkfiles'
    fld_modObs = 'modobs'
    fld_recdata = 'datarecieved'
    fld_mode = '%s%s' % (mode, fmt_string)
    mypaths = [os.path.join(homedirectory, fld_real, fld_allproc),
               os.path.join(homedirectory, fld_real, fld_allproc, fld_curproc),
               os.path.join(homedirectory, fld_real, fld_allproc, fld_curproc, fld_mode),
               os.path.join(homedirectory, fld_real, fld_heads),
               os.path.join(homedirectory, fld_real, fld_kkkfiles),
               os.path.join(homedirectory, fld_real, fld_modObs),
               os.path.join(homedirectory, fld_real, fld_recdata),
               os.path.join(homedirectory, fld_real, fld_recdata, fld_heads),
               os.path.join(homedirectory, fld_real, fld_recdata, fld_kkkfiles),
               os.path.join(homedirectory, fld_real, fld_recdata, fld_modObs)]

    for ii in mypaths:
        if '-' in ii:
            pass
        else:
            if not os.path.exists(ii):
                os.makedirs(ii)

    mypath_dict = {}
    mylabels = ['fld_allproc', 'fld_curproc', 'fld_mode', 'fld_heads', 'fld_kkkfiles',
                'fld_modobs', 'fld_recdata', 'fld_recheads', 'fld_reckkk', 'fld_recmodobs']

    for cur_id, curpath in enumerate(mypaths):
        mypath_dict[mylabels[cur_id]] = curpath

    return mypath_dict


def init_dir(ii, mymode, homedirectory):
    """
    Initialize directories and dir variables used along the whole process
        Arguments:
        ---------
    ii:         int, process id (ii is always python index)
    mymode:     str, 'fl_' or 'tr_'
    homedir:    home directory of the whole project
        Returns:
        --------
    NoReal
    dir_dict
    fmt_string
    kfile
    """
    NoReal = ii + 1  # To correct the zero index from python
    dir_dict = gendirtree(homedirectory, proc_idx=NoReal, mode=mymode)  # Get a dict with the directories
    fmt_string = '%.5d' % NoReal  # Process ID to maintain order
    kfile = 'kkkGaus_%s.dat' % fmt_string  # Name of the current kkk filename
    kkkFile = os.path.join(dir_dict['fld_mode'], kfile)

    return NoReal, dir_dict, fmt_string, kfile


def str_inWELLfile(myfile, mystr):
    """
    Find the string that follows a defined string. The string of interest should
    be able to be converted to "FLOAT" number(s). It returns also the
    line id (s) where the string was found. useful for reading observation
    well HGS output files.
        Arguments:
        ----------
    myfile:         str, full path of the file of interest
    mystr:          str, string BEFORE the one of interest
        Returns:
        --------
    String after the one given in the arguments
    """
    next_str = []
    ids = []
    with open(myfile, 'r') as fp:
        counter = 0
        for idx, line in enumerate(fp):
            match = re.search(mystr + '([^,]+)', line)
            if match:
                next_str.append(match.group(1))
    if len(next_str) == 1:
        next_str = next_str[0]
    else:
        next_str = np.asarray(next_str, dtype='float')

    if len(next_str) > 0:
        return next_str
    else:
        print('Given string not found in file')


def str_infile(myfile, mystr, index=False):
    """
    Find the string that follows a defined string. The string of interest should
    be able to be converted to "FLOAT" number(s). File format is any.
        Arguments:
        ----------
    myfile:         str, full path of the file of interest
    mystr:          str, string BEFORE the one of interest
    index:          bool, whether to retunr the line index where string was found
        Returns:
        --------
    String after the one given in the arguments. If index is True returns also
    the line indices where there was a match, as an additional array
    """
    next_str = []
    ids = []
    with open(myfile, 'r') as fp:
        counter = 0
        for idx, line in enumerate(fp):
            match = re.search(mystr + '([^,]+)', line)
            if match:
                ids.append(idx)
                next_str.append(match.group(1))
    if len(next_str) == 1:
        next_str = next_str[0]
        ids = ids[0]
    else:
        try:
            next_str = np.asarray(next_str, dtype='float')
            ids = np.asarray(ids, dtype='int')
        except:
            next_str = np.asarray(next_str, dtype='str')
            ids = np.asarray(ids, dtype='int')

    if len(next_str) > 0:
        if index is False:
            return next_str
        elif index is True:
            return ids, next_str
    else:
        print('Given string not found in file')


def numberfiles(mydir):
    """
    Count the number of files within a directory. If the folder does not exists
    It will be created automatically.
        Arguments:
        ----------
    mydir:      str, full directory location
        Returns:
        --------
    numfiles:   float, number of existing files
    """
    try:
        numfiles = len(os.listdir(mydir))
    except:
        os.makedirs(mydir)
        numfiles = 0
        print('The folder < %s > does not exist and will be created.' % mydir)

    return numfiles


def cpfolder(sourcedir, destdir):
    """ Copy a file or directory
    """
    shutil.copytree(sourcedir, destdir)


# #def collectGstatOutput(process):
# #    fileOfInterest = 'gstatOutputGauss.dat'
# #    myCodeDir = myMainDir()
# #    allgstatFields = os.path.join(myCodeDir, 'gstatFields')
# #    os.rename(os.path.join(process, fileOfInterest), os.path.join(process, 'gstatOutputGauss%s.dat' % process[-5:]))
# #    shutil.move(os.path.join(process, 'gstatOutputGauss%s.dat' % process[-5:]), os.path.join(allgstatFields))
# #    shutil.rmtree(process)

def join_data_files(mydir, dim=2, mystr=False):
    """
    Join datasets from different files to perform any kind of operation
    on all of them (e.g. rotate points)
        Arguments:
        ----------
    mydir:      str, directory where files are stored
    dim:        int, number of dimensions (columns) in the files
    mystr:      str, identifier of files of interest in case the folder contains additional files
                Default = False if all files
        Returns:
    jointdata:              np.array with joint datasets
    [mylist, lengths]:      np.array with the list of files the number of entries of each one
    """
    # %% Hands on the files to work with:
    mylist, mydir = getdirs(mydir, mystr=mystr, fullpath=False, onlylast=False)

    # %% Start loading each file and track the num of entries of each file:
    alldata = np.zeros((1, dim))
    lengths = np.array([])

    for curFile in mylist:
        curdata = np.loadtxt(os.path.join(mydir, curFile))
        alldata = np.concatenate((alldata, curdata))
        lengths = np.append(lengths, curdata.shape[0])

    return np.delete(alldata, 0, axis=0), [mylist, lengths]


def rmfiles(myfiles, fmt_string, process_path):
    """
    Remove unnecessary files to save disk space
        Arguments:
        ----------
    myfiles:        List, strings to identify all files to be KEPT
    fmt_string:     str, process ID
    process_path:   str, directory to perform the deletion
        Returns:
        --------
    mypaths:        str, full path of file(s) of the process in execution, not deleted ????
    """
    # Read all folders within proper process number:
    mypaths, dummydir = getdirs(process_path, mystr=fmt_string, fullpath=True, onlylast=False)
    idx = []
    for jj in range(0, len(mypaths)):
        mylist = os.listdir(mypaths[jj])
        mylist.sort()
        full_indices = np.arange(0,len(mylist), 1)

        for curname in myfiles:
            mycurfile, dummydir = getdirs(mypaths[jj], mystr=curname, fullpath=False, onlylast=False)

            if len(mycurfile) == 1:
                idx.append(np.where(mycurfile == mylist)[0])

            elif len(mycurfile) > 1:
                for ii in mycurfile:
                    idx.append(np.where(np.array(ii) == mylist)[0])

        toremove_indices = np.delete(full_indices, idx)

        try:
            for zz in toremove_indices:
                os.remove(os.path.join(mypaths[jj], mylist[zz]))
        except (IOError, PermissionError, AssertionError):
            print('Not all unnecessary files deleted, remove manually...')
            pass
    return mypaths


def fmtobspoints(points, outputfile):
    """
    Create a file with the strings "make observation point" integrated in the list
    it handles only three dimensional coordinates
    """
    f = open(outputfile, 'w')

    for ii in range(0, len(points)):
        f.write(
            'make observation point\nwell%.6d\n%s\t%s\t%s\n' % (ii + 1, points[ii, 0], points[ii, 1], points[ii, 2]))
    f.close()


def renameFiles(mydir, newstr, str2trim):
    """
    Rename the files in a directory, keeping the initial part of the string of each file
        Arguments:
    mydir:          str, directory containing the files to be renamed. Add r'mydir' if using Windows
    newstr:         str, new string to be added to all files
    str2trim:       str, characters to delete from the sring (end part of the string)
        Returns:
    Renamed files in defined directory
    """
    orig_list = os.listdir(mydir)
    orig_list.sort()
    str2trim = len(str2trim)

    for ii in orig_list:
        newname = ii[0:-str2trim] + newstr
        os.rename(os.path.join(mydir, ii), os.path.join(mydir, newname))


def readFieldData():
    print('Under construction')
