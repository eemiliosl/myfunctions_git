"""
Functions to deal with GROK and HGS
Created on Wed Mar 25 20:39:31 2015
@author: eesl
"""
import sys
import os
import numpy as np
import datetime
import shutil
import subprocess as sp
import platform
import multiprocessing as mp
from itertools import repeat
import dir_manip as dm
import grid_manip as gm
import gen_fields as gf
from statistics import mean
import matplotlib.pylab as plt
import hgs_grok as hg

# import kalman as klm
# Modified in the version of git: 14 of september, 2015


def getstates(homedir, ii, mode, nnodes, elements, modeltimes, biggrid_elem_ids, smallgrid_elem_ids,
              inner_gr_nodes='', Y_i_upd='', curtimestep=0, genfield=False, plot=False,
              storebinary=False, mytype='single', initial_head=7.0):
    """
    Update states(model measurements) with updated parameters using a forward model
    run for the corresponding realization. Give the parameter values NOT in LOG SCALE!!!
        Arguments:
        ----------
    homedir:
    ii:                 int, process index
    mode:               str, type of model run. 'fl_' or 'tr_'
    elements:           int, number of elements in the grid
    nnodes:             int, number of nodes in the grid
    biggrid_elem_ids:      int, indices of the original mesh grid
    smallgrid_elem_ids:  int, new indices taken from the original mesh grid list (ind_from_orig)
    inner_gr_nodes:     int, new grid nodes (new_nodes)
    Y_i_upd:            np.array, parameter values of the model
    genfield:           bool, generate a gaussian field or not
    plot:               bool, plot the generated fields or not
        Returns:
        --------
    modmeas:      np.array of updated modeled measurements (or states). Fortran order ([no_meas*no_obsPoints] x 1 )
    """
    # %% Initialize necessary variables and directories:
    Noreal, dir_dict, fmt_string, curKFile = dm.init_dir(ii, mode, homedir)

    processesPath = dir_dict['fld_allproc']
    mainPath = os.path.abspath(os.path.join(processesPath, '..', '..'))
    Model_data_dir = os.path.join(mainPath, 'Model_data')
    cur_processpath = dir_dict['fld_curproc']
    headsFile1 = 'finalheadsFlow_%s.dat' % fmt_string
    randparam = '_RandFieldsParameters.dat'
    randfile = os.path.join(Model_data_dir, randparam)

    headfieldsPath = dir_dict['fld_heads']
    ModObsPath = dir_dict['fld_modobs']
    dest_kkkFolderBinary = dir_dict['fld_kkkfiles']
    binkkkfile = os.path.abspath(os.path.join(dest_kkkFolderBinary, curKFile.split('.')[0]))
    curDestDir = dir_dict['fld_mode']
    if not os.listdir(curDestDir):
        prepfwd(ii, homedir, mode, headsFile=headsFile1)

    files2keep = np.array(
        ['.grok', '.pfx', 'kkkGaus', '.mprops', 'letme', 'wprops', 'finalheads', 'parallelindx', 'meshwell', '.png',
         '.lic'])

    # %% First Step: Generate new kkk files to be used by GROK and HGS in the proper folder
    # ---------------------#
    assert os.path.exists(curDestDir), 'Something is wrong in the definition of the tree directory!'
    assert os.path.exists(Model_data_dir), 'Model data directory does not exist or not in proper location!'

    # %% Decide whether to create random fields or not:
    # Load random field parameter data from the main file:
    xlen, ylen, zlen, nxel, nyel, nzel, xlim, ylim, zlim, Y_model, Y_var, beta_Y, lambda_x, lambda_y, lambda_z = gf.readParam(
        randfile)
    if genfield is True:
        # Generate the field: returns K in m/s. Generate it for the smaller-inner grid.....
        Y = gf.genFields_FFT_3d(xlim[1] - xlim[0], ylim[1] - ylim[0], zlim[1] - zlim[0], nxel, nyel, nzel, Y_model,
                                Y_var, beta_Y, lambda_x, lambda_y, lambda_z, plot, mydir=curDestDir)
    elif genfield is False:
        assert len(Y_i_upd) > 0, 'No parameters were provided!'
        Y = np.copy(Y_i_upd)

    if 'fl_' in mode:  # Always expand the parameter vector for forward model run when flow
        # Y = gm.expshr_param(Y, orig_elem_ind, elem_ind_fromorig, what= 'expand')
        Y = gm.expshr_param(Y, biggrid_elem_ids, smallgrid_elem_ids, what='expand')

    if 'tr_' in mode:  # If transport, load the corresponding initial head field and store in cur_proc folder
        if len(Y) > elements:
            Y = Y[1:]
        if curtimestep == 0:
            headfield = np.load(os.path.join(headfieldsPath, headsFile1.split('.')[0] + '.npy'))
            np.savetxt(os.path.join(curDestDir, headsFile1), np.transpose(headfield))
    # pdb.set_trace()
    assert len(Y) == elements, 'There is a problem with parameter vector dimensions!'
    np.savetxt(os.path.join(curDestDir, curKFile), np.transpose((np.arange(1, len(Y) + 1, 1), Y.squeeze(), Y.squeeze(),
                                                                 Y.squeeze())), fmt='%d %.6e %.6e %.6e')
    # %% Second Step: Run GROK and HGS :
    fwdHGS(curDestDir, fmt_string, mytype=mode)

    # %% Third step: Read new model outputs at specified output times, store them as bin files.
    # If running flow, extract in addition final heads:
    if 'single' in mytype:
        mytimes = modeltimes[curtimestep][np.newaxis]
    elif 'cum' in mytype:
        mytimes = modeltimes[0:curtimestep + 1]
    elif 'full' in mytype:
        mytimes = modeltimes

    all_modout = getmodout(curDestDir, mode=mode, outputtime=mytimes, initial_head=initial_head)

    if mode == 'fl_':  # Save final heads for future transport run, and shrink parameter vector
        Y = gm.expshr_param(Y, biggrid_elem_ids, smallgrid_elem_ids, what='shrink')
        if (storebinary is True) and ((curtimestep + 1) == len(modeltimes)):
            headfile, dummypaths = dm.getdirs(curDestDir, mystr='head_pm', fullpath=True, onlylast=True)
            headfield, dummytime = readHGSbin(headfile, nnodes)
            headfield = headfield[inner_gr_nodes - 1]  # This has not been checked (it seems to be correct)
            np.save(os.path.join(headfieldsPath, headsFile1.split('.')[0]), headfield)

    # %% Fourth: save stuff in the corresponding directories:
    if (storebinary is True) and (curtimestep == 0):
        np.save(os.path.join(ModObsPath, 'mod%s%s' % (mode, fmt_string)), all_modout)
        np.save(binkkkfile, np.transpose(Y))  # Copy of the kkk-file for future sending session to the server

    #  Fifth: Remove unnecessary files:
    dummypaths = dm.rmfiles(files2keep, mode + fmt_string, cur_processpath)
    del dummypaths
    # %% Pre-process the model outputs (states) prior to return them.
    try:
        modmeas = all_modout.flatten('F')  # Fortran convention is always used in this process:
    except:
        raise Exception('Something is wrong with the model outputs, check e.g. cur time step in grok!')

    return modmeas  # Y not even needed to return. I am not really modifying it here (just expanding vector if flow)


def caller_getstates(Y, nnodes, elements, homedir, mymode, fieldmeas_len, current_time, thetimes,
                     type_update, biggrid_elem_ids='', smallgrid_elem_ids='', smallgrid_nodes='',
                     headsFile=False, parallel=False, cpus='', genfield=False, plot=False,
                     storebinary=False, initial_head=7.0):
    """ Support function to call getstates with the option of doing it in parallel
    Arguments:
        Y:              numpy 2D array, parameter array for all realizations
        nnodes:         int, number of nodes in the grid
        elements:       int, number of elements in the grid
        homedir:        str, home directory (e.g. kalmanfilter_git)
        mymode:         str, 'fl_ or 'tr_'
        fieldmeas_len:  int, number of field observations (
        current_time:
        thetimes:
        type_update:
        biggrid_elem_ids:
        smallgrid_elem_ids:
        smallgrid_nodes:
        headsFile:
        parallel
        cpus:
        genfield:
        plot:
        storebinary:
        initial_head
    Returns:
        modmeas, numpy 2D array with all modeled measurements from all realizations
    """

    nel, nreal = Y.shape
    modmeas = np.zeros((fieldmeas_len, nreal))
    if parallel is False:
        for ii in range(0, nreal):
            modmeas_temp, idx_dummy = getstates_parall(ii, Y, nnodes, elements, homedir, headsFile, mymode,
                                                         current_time, thetimes, type_update, biggrid_elem_ids,
                                                         smallgrid_elem_ids, smallgrid_nodes,
                                                         genfield, plot, storebinary, initial_head)
            # if (len(modmeas_temp) != fieldmeas_len) or (len(modmeas_temp) == 0):
            #     modmeas[:, ii] = np.zeros(fieldmeas_len, )
            # else:
            modmeas[:, ii] = modmeas_temp

    elif parallel is True:
        mypool = mp.Pool(cpus)
        modmeas_temp = mypool.starmap(getstates_parall,
                                      zip(np.arange(0, nreal, 1), repeat(Y), repeat(nnodes), repeat(elements),
                                          repeat(homedir), repeat(headsFile), repeat(mymode), repeat(current_time),
                                          repeat(thetimes),
                                          repeat(type_update), repeat(biggrid_elem_ids), repeat(smallgrid_elem_ids),
                                          repeat(smallgrid_nodes), repeat(genfield), repeat(plot), repeat(storebinary),
                                          repeat(initial_head)))
        mypool.close()
        mypool.join()

        for ii in range(0, len(modmeas_temp)):
            # if (len(modmeas_temp[ii][0]) != fieldmeas_len) or (len(modmeas_temp[ii][0]) == 0):
            #     modmeas[:, modmeas_temp[ii][-1]] = np.zeros(fieldmeas_len, )
            # else:
            modmeas[:, modmeas_temp[ii][-1]] = np.asarray(modmeas_temp[ii][0])

    return modmeas


def getstates_parall(ii, Y, nnodes, elements, homedir, headsFile, mymode, current_time,
                     thetimes, type_update, biggrid_elem_ids, smallgrid_elem_ids, smallgrid_nodes,
                     genfield, plot, storebinary, initial_head):
    """ Function that calls getstates for the corresponding member of the ensemble. Prepares the
        cur_process folder, and add updates the time in the grok file.
    Arguments:
        initial_head:
        storebinary:
        plot:
        genfield:
        ii:
        Y:
        nnodes:
        elements:
        homedir:
        headsFile:
        mymode:
        current_time:
        thetimes:
        type_update:
        biggrid_elem_ids:
        smallgrid_elem_ids:
        smallgrid_nodes:
    Returns:
        modeled observations and index of the current member of the ensemble
    """
    mypath_dict = dm.gendirtree(homedir, proc_idx=ii + 1, mode=mymode)
    headsFile_str = "finalheadsFlow_%.5d.dat" % (ii + 1)

    if not os.listdir(mypath_dict['fld_mode']):
        prepfwd(ii, homedir, mymode, headsFile=headsFile_str)

    addt2grok(ii + 1, homedir, mypath_dict, current_time, thetimes, mode=mymode, mytype=type_update)
    modmeas = getstates(homedir, ii, mymode, nnodes, elements, thetimes, biggrid_elem_ids, smallgrid_elem_ids,
                        inner_gr_nodes=smallgrid_nodes, Y_i_upd=np.exp(Y[:, ii]), curtimestep=current_time,
                        genfield=genfield, plot=plot, storebinary=storebinary, mytype=type_update,
                        initial_head=initial_head)  # here I have an error
    return modmeas, ii


def fwdHGS(mydir, mystr, mytype='', hsplot=False):
    """
    Run GROK and HGS and if stated, run also hsplot
        Arguments:
        ----------
    mydir:          str, full path directory where GROK and HGS should be run
    mystr:          str, flag to print no. of realization in the cmd
    mytype:         str, flag to print flow or transport identifier in the cmd
    hsplot:         bool, (optional) if false, hspot is not executed
    """
    print('Process No. %s: running GROK (%s)... ' % (mystr, mytype))
    qq = sp.Popen('grok', shell=True, cwd=mydir, stdout=sp.PIPE, stderr=sp.PIPE)
    output_qq, error_qq = qq.communicate()
    print('Process No. %s: running PHGS (%s)... ' % (mystr, mytype))
    qq_2 = sp.Popen('phgs', cwd=mydir, stdout=sp.PIPE, stderr=sp.PIPE)
    output_qq_2, error_qq_2 = qq_2.communicate()

    if hsplot is True:
        print('Process No. %s: running HSPLOT(%s)... ' % (mystr, mytype))
        qq_3 = sp.Popen('hsplot', cwd=mydir, stdout=sp.PIPE, stderr=sp.PIPE)
        output_qq_3, error_qq_3 = qq_3.communicate()
        if platform.system() == 'Windows':
            qq_3.kill()
    if platform.system() == 'Windows':
        qq.kill()
        qq_2.kill()
    print('Successful fwd model run, Process No. %s (%s)' % (mystr, mytype))


def rmodout(myfile, mytimes, mymode='fl_', initial_head=7.0):
    """
    Support for the <getmodout> function (to extract model measurements)
        Arguments:
        ----------
    myfile:     str, fullpath with the name of the file to be loaded
    mytimes:    np array,  float or int array with times of interest
    mymode:     str, model run mode. Default 'fl_' for flow model
        Returns:
        --------
    mydata:     np.array, with model outputs at specific times (nmeas x 1)
    """
    try:
        # Observation point HGS format
        myTime, mymodmeas = np.loadtxt(myfile, skiprows=2, usecols=(0, 1), unpack=True)
    except:
        # Observation well HGS format:
        mymodmeas = []
        idx_line, myTime = dm.str_infile(myfile, 'SOLUTIONTIME=', index=True)

        if isinstance(idx_line, int) is True:
            myTime = np.asarray(myTime, dtype=float)
            mymodmeas = np.loadtxt(myfile, skiprows=3, usecols=(0,))
            mymodmeas = np.mean(mymodmeas)

        if isinstance(idx_line, int) is False:
            with open(myfile, 'r') as f:
                mylines = f.readlines()

            for yy in range(0, len(idx_line), 1):
                if yy < len(idx_line) - 1:
                    myvalues = np.asarray(mylines[idx_line[yy] + 1: idx_line[yy + 1]])
                elif yy == len(idx_line) - 1:
                    myvalues = np.asarray(mylines[idx_line[yy] + 1:])

                try:
                    mymodmeas.append(mean([float(cur_line.split(' ')[2]) for cur_line in myvalues]))
                except:
                    mymodmeas.append(mean([float(cur_line.split(' ')[3]) for cur_line in myvalues]))

        if (isinstance(mymodmeas, int) or isinstance(mymodmeas, np.float64)) is True:
            mymodmeas = np.asarray(mymodmeas)
        if isinstance(mytimes, int) is True:
            mytimes = np.expand_dims(mytimes, 1)

            # %% First transform the data to get desire results: either drawdown or cdf of btcs:
    if mymode == 'fl_':
        mymodmeas_transf = np.subtract(initial_head, mymodmeas)
    elif mymode == 'tr_':
        mymodmeas_transf = np.copy(mymodmeas)
        zero_id = np.where(mymodmeas < 0)
        if len(zero_id[0]) > 0:
            try:
                mymodmeas_transf[zero_id[0]] = 0
            except IndexError:
                mymodmeas_transf[np.newaxis][zero_id[0]] = 0
        mymodmeas_transf = np.cumsum(mymodmeas_transf)  # Not yet normalized!

    # %% Mask to get only the elements of interest:
    if not isinstance(mymodmeas_transf, np.ndarray):
        mymodmeas_transf = np.expand_dims(mymodmeas_transf, axis=0)
    mymodmeas_transf = mymodmeas_transf[np.where(np.in1d(myTime, mytimes))]

    return mymodmeas_transf


def getmodout(mypath, mode='', outputtime='', initial_head=7.0):
    """
    Read model output from a file at specific output times.
    Time values will be integers. Corrects negative concentrations when
    working in tranport mode.
        Arguments:
        ----------
    mypath:         str, directory where model outputs are located
    mode:           Str, 'fl_' or 'tr_'
    outputtime:     output times of interest
        Returns:
        --------
    all_modout:   2D np array, observed values at defined times
                   size of the array = [No times x No Obs Points]. Fortran order
    """
    # assert outputtime != '', 'No output times provided'
    if not mode:
        print('Model outputs not extracted. No running mode provided')
    elif mode == 'fl_':
        mystrs = '_flow'
    elif mode == 'tr_':
        mystrs = '_conc'

    # Get a list of the files of all observation points:
    files, dummydir = dm.getdirs(mypath, mystr=mystrs, fullpath=True, onlylast=False)

    # Start collecting them in a single 2D array
    all_modout = np.array([])

    for ii in files:
        selMeas = rmodout(ii, outputtime, mymode=mode, initial_head=initial_head)
        # selMeas = rmodout(ii, outputtime.astype(np.int))
        all_modout = np.append(all_modout, selMeas)  # All selected heads

    # Protect the process of reading outputs against failure of HGS: e.g. no more mass stored => all conc = 0
    numobs = len(outputtime) * len(files)
    if (len(all_modout) != numobs) or (len(all_modout) == 0):
        all_modout = np.zeros(numobs, )

    all_modout = np.reshape(all_modout, (-1, len(files)), order='F')

    return all_modout


def readHGSbin(myfile, nnodes):
    """
    Read a HGS output bin file. The function has been tested with
    subsurface hydraulic heads and overland heads.
        Arguments:
        ----------
    myfile:     str, filename *full path(
    nnodes:     int, number of nodes in the mesh
        Returns:
        --------
    n_param:    np.array with the nodal parameter values
    time:       float, model time that corresponds to the param field values
    """
    with open(myfile, "rb") as f:
        pad = np.fromfile(f, dtype='uint8', count=4)
        time = np.fromfile(f, dtype='uint8', count=80)

        tt = [''.join(chr(t)) for t in time]
        time = float(''.join(tt))

        pad = np.fromfile(f, dtype='uint8', count=4)
        pad = np.fromfile(f, dtype='uint8', count=4)

        param = np.fromfile(f, dtype='float64', count=nnodes)
        pad = np.fromfile(f, dtype='uint8', count=4)

    return param, time


def CreatePrefixBatch(mydir, myString):
    """
    Create a new Prefix file for running GROK and HGS
        Arguments:
        ----------
    mydir:      str, directory where the file should be located
    myString:   str, string to add to the prefix file
        Returns:
        --------
    The batch file batch.pfx with problem prefix
    """
    prefFile = 'batch.pfx'
    if prefFile in os.listdir(mydir):
        os.remove(os.path.join(mydir, prefFile))

    prefix = open(os.path.join(mydir, prefFile), 'w')
    prefix.write(myString)
    prefix.close()


def Createletmerun(mydir, myString, hsplot=False):
    """
    Create a letmerun batch file for running GROK, HGS and HSPLOT
        Argument:
        ---------
    mydir:      str, directory where the file should be located
    myString:   str, name of the GROK file to be executed
    hsplot:     bool, True: include instruction to run hsplot
                             False: just include grok and phgs
        Returns:
        --------
    The batch file letmerun.bat
    """
    batchfile = 'letmerun.bat'
    if batchfile in os.listdir(mydir):
        os.remove(os.path.join(mydir, batchfile))

    letmerun = open(os.path.join(mydir, batchfile), 'w')
    letmerun.write('@echo off\n\ndel *' + myString + 'o* > nul\n\n')
    letmerun.write('grok\nphgs\n')

    if hsplot == True:
        letmerun.write('hsplot\n')

    letmerun.close()


def prepfwd(ii, homedir, mymode, headsFile=''):
    """
    Prepare folder for forward model run. Meaning: copy the source folder with
    all its files, into a folder that includes process number id (number of realization)
        Arguments:
        ----------
    ii:             int, process id
    homedir:        str, home directory of the whole project
    mymode:         str, fl_ or tr_, defines flow or transport
    headsFile:      str, name of the final(from heads) or initial(for transport) heads
        Returns:
        --------
    Creates corresponding directories for a forward model run
    """

    # %% Create process folder if does not exist:
    Noreal, dir_dict, fmt_string, curKFile = dm.init_dir(ii, mymode, homedir)
    SourceDir = os.path.abspath(os.path.join(homedir, mymode + 'ToCopy'))

    # Dictionary of files which name is to be updated:
    if mymode == 'fl_':
        StrFiles2Replace = {'kkkGaus.dat': curKFile}
    elif mymode == 'tr_':
        StrFiles2Replace = {'kkkGaus.dat': curKFile, 'finalheadsFlow.dat': headsFile}

    prepareGROK(SourceDir, dir_dict['fld_mode'], mymode, fmt_string, StrFiles2Replace)

    return Noreal, dir_dict, fmt_string, curKFile


def prepareGROK(SourceDir, DestDir, mymode, fmt_string, StrFiles2Replace, hsplot=False):
    """
    Support for the <prepfwd>, this is the function that does copy the folder. It copies all
    dependencies, adds an identifier to each file, creates a batch file with proper prefix
    to make GROK run automatically, and replace corresponding file-names within GROK file
        Arguments:
        ----------
    SourceDir:          str, folder to be copied (full path)
    DestDir:            str, destination directory
    mymode:             str, fl_ or tr_, defines flow or transport
    fmt_string:         str, Identifier to assign to each new copied folder
    StrFiles2Replace:   dict, files to be replaced within GROK (eg.
                        {'kkk_file.dat': cur_kkkFile,'InitialHeads.head_pm.001': 'iniHeads_%s.head_pm'}) Not full path
    hsplot:             bool, default is False: not include hsplot in the batch file sequence
        Returns:
        --------
    Creates a brand new folder with the necessary files to make a forward HGS run using a
    current KKK field
    """
    # %% Copy directories and files:
    try:
        shutil.copytree(SourceDir, DestDir)
    except:
        myfiles = os.listdir(SourceDir)
        for item in myfiles:
            if 'groks' in os.path.join(DestDir, item):
                pass
            else:
                try:
                    shutil.copy2(os.path.join(SourceDir, item), os.path.join(DestDir, item))
                except:
                    pass

    # %% Create a new GROk file with the KKK file updated:
    new_str = mymode + 'sim' + ('_%s.grok' % fmt_string)
    Out_grokFile = os.path.join(DestDir, new_str)

    if not os.path.isfile(Out_grokFile):

        In_grokFile = os.path.join(DestDir, mymode + 'sim' + '.grok')

        # %% Copy heads file if working with transport:
        # No!!! This will be done in the get states function, here just the name
        # is replaced in the GROK file

        InFileObj = open(In_grokFile)
        InFile = InFileObj.read()
        OutFile = open(Out_grokFile, 'w')
        for i in list(StrFiles2Replace.keys()):
            InFile = InFile.replace(i, StrFiles2Replace[i])

        OutFile.write(InFile)
        OutFile.close()
        InFileObj.close()
        os.remove(In_grokFile)

        # %% Create the prefix and "let me run " batch files with the proper string:
        CreatePrefixBatch(DestDir, new_str.split('.')[0])
        Createletmerun(DestDir, new_str.split('.')[0], hsplot=hsplot)


def rdparam_bin(myPath):
    """ Read parameter fields from binary files
    Arguments:
        myPath:     Str, directory where the information is stored
    Returns:
        nel:        Integer, number of elements of the model grid
        nreal:      Integer, number of realizations in the ensemble
        Y:          Numpy array, 2D Array with the proper data
    """
    myFiles = os.listdir(myPath)
    myFiles.sort()
    bb = 0

    for kk in range(0, len(myFiles)):

        cur_file = os.path.join(myPath, myFiles[kk])

        if 'kkk' in cur_file:
            if bb == 0:
                Y = np.log(np.load(cur_file))
                bb += 1
            else:
                kdata = np.log(np.load(cur_file))
                Y = np.c_[Y, kdata]  # Parameter field matrix (nel,nreal)

    nel = Y.shape[0]  # Number of elements in the domain
    nreal = Y.shape[1]  # Number of realizations in the Ensemble

    return nel, nreal, Y


def rdmodelout_bin(myPath, mymode='fl_'):
    """ Read Modeled outputs from binary files. Either heads or concentrations.
    Arguments:
        myPath:             Str, directory where the information is stored
        mymode:             str, type of data to read
    Returns:
        num_modout:              Integer, number of model outputs
        modoutput:       Numpy array, modeled measurement values (ndata x nrealizations)

    Note: The matrices of measurements are flattened following Fortran
        convention (column-wise or column major). If work with both types of
        measurements, heads are listed first and then concentrations
    """
    myFiles = os.listdir(myPath)
    myFiles.sort()
    hh = 0
    cc = 0
    dd = 0

    for curid, kk in enumerate(myFiles):

        if mymode in kk:
            cur_file = os.path.join(myPath, myFiles[curid])

            if curid == 0:
                modoutput = np.load(cur_file).flatten('F')

            elif curid > 0:
                cur_modout = np.load(cur_file).flatten('F')
                modoutput = np.c_[modoutput, cur_modout]

    num_modout = modoutput.shape[0]  # Number of observations

    return num_modout, modoutput


def addt2grok(zz, homedir, mypath_dict, cur_timestep, modeltimes, mode='fl_', mytype='single'):
    """ Add/subtract new output times to GROK. The times are taken from the
    '_outputtimesFlowUpdate.dat' and/or '_outputtimesTransUpdate.dat'
    Arguments:
        zz:
        homedir:
        mypath_dict:
        cur_timestep:
        modeltimes:
        mode:       'fl_'
        mytype = 'single' cumulative
    Returns:
        GROK file with updated time step
    """
    # %% Directories and file names:

    mygrokfile, dummy = dm.getdirs(mypath_dict['fld_mode'], mystr='%ssim_' % (mode), fullpath=True)
    mygrokfile = mygrokfile[0]
    tempgrokfile = mygrokfile + 'temp'

    # %% Working with the time steps to modify in grok:
    if 'single' in mytype:
        mytimes = modeltimes[cur_timestep][np.newaxis]
    elif 'cum' in mytype:
        mytimes = modeltimes[0:cur_timestep + 1]
    elif 'full' in mytype:
        mytimes = modeltimes

    writing = True
    with open(mygrokfile) as f:

        with open(tempgrokfile, 'w') as out:
            for line in f:
                if writing:
                    if "output times" in line:
                        writing = False
                        out.write("output times")
                        for ii in range(0, len(mytimes)):
                            out.write('\n%s' % (mytimes[ii]))
                        buffer = [line]
                        out.write("\nend\n")
                    else:
                        out.write(line)
                elif "end" in line:
                    writing = True
                else:
                    buffer.append(line)
            else:
                if not writing:
                    # There wasn't a closing "event", so write buffer contents
                    out.writelines(buffer)
            out.close()
            f.close()
    os.remove(mygrokfile)
    os.rename(tempgrokfile, mygrokfile)

    print('grok file < %s > updated...' % (os.path.split(str(mygrokfile))[-1]))


def run_refmodel(mainPath, curKFile, Flowdir, modelDataFolder, mymodel, mymode='fl_', runmodel=True, std_dev=0, ini_head=0, ret_modout=True):
    """ Perform all sequence to obtain a synthetic model with heterogeneous fields and extracting model outputs. It is
    possible to add measurement noise to model outputs.
    Args:
        mainPath:   str, directory where the model sub-directory is located, eg ../myModels
        curKFile:   str, filename containing K parameters, eg kkkGaus.dat
        Flowdir:    str, folder name where flow synthetic model is stored, eg _fl_26062015_synthetic
        modelDataFolder: str, folder name containing model input data, eg ModelData_26062015_synthetic
        mymodel:    str, folder of the model that is wished to run eg _fl_26062015_synthetic or tr_...
        mymode:     str, 'fl_':flow , 'tr_':transport. Default fl_
        runmodel:   bool, whether it is required to run the HGS model. Default is True
        std_dev:    float, measurement noise std deviation added to model outputs. Default 0 (no noise)
        ini_head:   float, initial head value in case of running flow model. Default 0
        ret_modout: bool, if True returns an array with model outputs
    Returns:    model outputs stored in a file, plus all output generated from GROK, PHGS and HSPLOT.
                If ret_modout is True returns an array with model outputs and an array with model times
    Input example:
        mainPath = r'C:\PhD_Emilio\Emilio_PhD\_CodeDev_bitbucket\kalmanfilter_git\myModels'
        curKFile = 'kkkGaus.dat'
        mymode = 'fl_'  # 'tr_'
        mymodel = '_fl_26062015_synthetic'
        Flowdir = '_fl_26062015_synthetic'
        modelDataFolder = 'ModelData_26062015_synthetic'
        std_dev = 3.0e-2  # As an example assumed a std error measurement of 1.0 mm on each measurement 1.5e-3, tr = 2.5e-6
        runmodel = True
        ini_head = 20.0
    """
    print('You have to have at least the following files ready:')
    print('Grok file, model parameters file, outputtimes, meshtecplot')

    # Create additional directories
    curDestDir = os.path.join(mainPath, mymodel)
    grid_filedir = os.path.join(curDestDir, 'meshtecplot.dat')
    myParameterDir = os.path.join(mainPath, modelDataFolder, '_RandFieldsParameters.dat')
    curFlowdir = os.path.join(mainPath, Flowdir)
    # Read parameters
    xlen, ylen, zlen, nxel, nyel, nzel, xlim, ylim, zlim, Y_model, Y_var, beta_Y, lambda_x, lambda_y, lambda_z = gf.readParam(
        myParameterDir)

    # Read model mesh components
    # Big grid:
    nnodes_big, elements_big = gm.readmesh(os.path.join(curFlowdir, 'meshtecplot.dat'), fulloutput=False)
    biggrid_elem_ids = np.arange(1, elements_big + 1, 1)

    nnodes, elements = gm.readmesh(grid_filedir, fulloutput=False)
    smallgrid_nodes, smallgrid_elem = gm.sel_grid(os.path.join(curFlowdir, 'meshtecplot.dat'), xlim, ylim, zlim)
    # smallgrid_elem_ids = np.arange(1, nnodes + 1, 1)

    # If K field does not exists, make a random realizationRead parameter field setup:
    if not os.path.isfile(os.path.join(curFlowdir, 'kkkGaus.dat')):
        kkkvalues = gf.genFields_FFT_3d(xlim[1] - xlim[0], ylim[1] - ylim[0], zlim[1] - zlim[0], nxel, nyel, nzel,
                                        Y_model, Y_var, beta_Y, lambda_x, lambda_y, lambda_z, plot=True,
                                        mydir=curDestDir)
        kkk = gm.expshr_param(kkkvalues, biggrid_elem_ids, smallgrid_elem, what='expand')
        np.savetxt(os.path.join(curFlowdir, curKFile), np.transpose((np.arange(1, len(kkk) + 1, 1), kkk, kkk, kkk)),
                   fmt='%d %.6e %.6e %.6e')

    if not os.path.isfile(os.path.join(curDestDir, 'kkkGaus.dat')):
        if mymode is 'tr_':
            kkkvalues = np.loadtxt(os.path.join(curFlowdir, 'kkkGaus.dat'), usecols=(1,))
            kkkvalues = kkkvalues[smallgrid_elem -1 ]
            #kkkvalues = gm.expshr_param(kkkvalues, biggrid_elem_ids, smallgrid_elem, what='shrink')
            #kkkvalues = kkkvalues[1:]
            np.savetxt(os.path.join(curDestDir, curKFile),
                       np.transpose((np.arange(1, len(kkkvalues) + 1, 1), kkkvalues, kkkvalues, kkkvalues)),
                       fmt='%d %.6e %.6e %.6e')

    if not os.path.isfile(os.path.join(curDestDir, "finalheadsFlow.dat")):
        if mymode is 'tr_':
            headsFile = dm.getdirs(curFlowdir, mystr='head_pm', fullpath=True, onlylast=True)

            myheadsFlow, mytime = hg.readHGSbin(headsFile[0], nnodes_big)
            myheadsTransport = myheadsFlow[smallgrid_nodes - 1]
            np.savetxt(os.path.join(curDestDir, "finalheadsFlow.dat"), np.transpose(myheadsTransport))

    # Run the model if required
    if runmodel is True:
        hg.fwdHGS(curDestDir, 'reference', mytype=mymode, hsplot=True)

    # Get the "field" observations:
    # -----------------------------

    # Extract modeled observations:
    myouttimes = np.loadtxt(os.path.join(mainPath, modelDataFolder, '_outputtimes%s.dat' % mymode))
    selObs_All = hg.getmodout(curDestDir, mode=mymode, outputtime=myouttimes, initial_head=ini_head)

    if std_dev != 0:
        # Add gaussian measurement noise:
        num_modmeas = selObs_All.shape[0]
        meanNoise = np.zeros((num_modmeas,))  # Mean value of white noise equal to zero (num_modmeas x 1)
        R = ssp.dia_matrix((np.multiply(np.eye(num_modmeas, num_modmeas), std_dev ** 2)))  # sparse Matrix (nmeas x nmeas)
        E = np.empty((num_modmeas, selObs_All.shape[1]))
        for ii in range(0, selObs_All.shape[1], 1):
            # E[:,ii] = np.expand_dims(np.random.multivariate_normal(mean,R.toarray()),axis = 1)
            E[:, ii] = np.random.multivariate_normal(meanNoise, R.toarray())

        noisy_data = selObs_All + E
    else:
        noisy_data = selObs_All

    # For concentrations
    if mymode is 'tr_':
        zero_id = np.where(noisy_data < 0)
        noisy_data[zero_id[0], zero_id[1]] = 0

    # Save artificial field measurements:
    np.savetxt(os.path.join(mainPath, modelDataFolder, 'Field%s.dat' % mymodel), noisy_data, fmt='%10.9e')
    print('Reference model done and field observations stored')

    if ret_modout is True:
        return myouttimes, noisy_data


# Hasta aqui! 09 June 2016


def syncModel2Data(heads, concentrations):
    """ Synchronize the number of model outputs with the number of
    time-steps-updates to perform
    Arguments:
        heads:          Boolean, include head data or not
        concentrations: Boolean, include conc. data or not
    Returns:
        len(cur_heads_time):    Zero if heads == False
        len(cur_conc_time):     Zero if concentration == False
        curTimes:               Number of CURRENT output times dealing with
    Dependencies:
        dm.myMainDir()
    """
    myDir = dm.myMainDir()
    KalmanFilterPath = 'kalmanfilter'

    if heads == True:
        cur_heads_time = np.loadtxt(
            os.path.join(myDir, '..', KalmanFilterPath, 'Model_data', '_outputtimesFlowUpdate.dat'))
        try:
            len(cur_heads_time)
        except:
            cur_heads_time = np.expand_dims(cur_heads_time, 1)

    if concentrations == True:
        cur_conc_time = np.loadtxt(
            os.path.join(myDir, '..', KalmanFilterPath, 'Model_data', '_outputtimesTransUpdate.dat'))
        try:
            len(cur_conc_time)
        except:
            cur_conc_time = np.expand_dims(cur_conc_time, 1)

    # %% Synchronize the number of model outputs with the number
    #  of time-steps-updates to perform:
    try:
        curTimes = len(cur_heads_time) + len(cur_conc_time)
    except:
        if heads == True:
            curTimes = len(cur_heads_time)
        if concentrations == True:
            curTimes = len(cur_conc_time)

    try:
        return len(cur_heads_time), len(cur_conc_time), curTimes
    except:
        if heads == True:
            return len(cur_heads_time), 0, curTimes
        if concentrations == True:
            return 0, len(cur_conc_time), curTimes


def writeinGrok(myInDir, myOutDir, myoutfile, probdescFile, grid_coordFile, gridmethod,
                Transient, Transport, Echo2Output, FD, CV, porousfile, kfile, heads_init, transport_init, heads_bc,
                TimeStepCtrl, well_propsFile, pumpwellsFile, tracer_injFile, outputimesFile, obswellsFile):
    """ Type in the main parts of a grok file
    Arguments:
        myInDir:        Str, directory where input files are located
        myOutDir:       Str, directory where GROK file is/will be located
        myoutfile:      Str, name assigned to the GROK file
        probdescFile:   Str, filename containing description of the project
        grid_coordFile: Str, filename containing grid coordinates
        gridmethod:     Str, method to generate grid-'HGS_uniform' 'HGS_grade' 'GB_file'
        Transient:      Boolean, run transient mode or not
        Transport:      Boolean, run transport mode or not
        Echo2Output:    Boolean, create echo output file or not
        FD:             Boolean, finite difference mode or not
        CV:             Boolean, control volume mode or not
        porousfile:     Str, porous media properties file
        kfile:          Str, KKK filename for homogeneous fields, if homogeneous type ''
        heads_init:     Tuple [mode, arg]:
                        mode:'output',arg: string file name
                        mode: 'onevalue', arg: number
                        mode: 'raster', arg: string file name
                        mode:'txt', arg: string file name
        transport_init: Tuple [mode, arg]
                        mode:'output',arg: string file name
                        mode: 'onevalue', arg: number.
                        If transport is False, just type ''
        heads_bc:       tuple [X1, X2], X corrdinate of the X plane to be selected to assign constant head equals to initial value.
                        tuple ['all', 'head equals initial']: all heads stay the same
                        (In the future should support Y and Z planes and possibility
                        to assign boundary conditions from GB files)
        TimeStepCtrl:   Boolean, et the default time step controlers or not
        well_propsFile: Str, filename containing well construction details
        pumpwellsFile:  Str, filename containing well coordinates, rates, etc.
        tracer_injFile: Str, filename containing tracr injection data, if Transport is False, type ''
        outputimesFile: Str, filename containing defined output times
        obswellsFile:   Str, filename containing observation well information

    Returns:
        A new grok file
    """
    # %% PROBLEM DESCRIPTION:
    # Read problem description content from file:
    with open(os.path.join(myInDir, probdescFile), "r") as myfile:
        banner = myfile.read()  # .replace('\n', '')
    # Generate the grok file:
    Ofile = open(os.path.join(myOutDir, myoutfile), 'w')  # Generate the file
    Ofile.write(banner % (datetime.datetime.now()))  # %(time.strftime("%d/%m/%Y"))
    Ofile.flush()
    del banner
    print('<Problem description> written ok...')

    # %% GRID GENERATION: gb or directly in HGS
    banner = '\n\n!---  Grid generation\n'
    Ofile.write('%s' % banner)
    del banner
    if gridmethod == 'HGS_grade':
        Ofile.write('generate blocks interactive\n')
        dim = ['x', 'y', 'z']

        try:
            coordinates = np.loadtxt(os.path.join(myInDir, grid_coordFile))
        except IOError('Unable to load instructions'):
            raise

        if len(dim) != len(coordinates):
            raise IndexError('number of lines in coordinate file must be equal to length of dim vector')

        for cur_dim in range(0, len(dim), 1):
            Ofile.write('grade %s\n' % dim[cur_dim])

            for cur_val in range(0, coordinates.shape[1], 1):
                Ofile.write('%.8e\t' % coordinates[cur_dim, cur_val])
            Ofile.write('\n')

        Ofile.write('end generate blocks interactive\nend\n')
        Ofile.write('Mesh to tecplot\nmeshtecplot.dat\n')

    Ofile.flush()
    print('<Grid generation> written ok...')

    # %% SIMULATION PARAMETERS GROK:
    units = 'units: kilogram-metre-second'
    banner = '\n\n!---  General simulation parameters\n'

    Ofile.write('%s' % banner)
    Ofile.write('%s\n' % units)

    if FD == True: Ofile.write('Finite difference mode\n')
    if CV == True: Ofile.write('Control volume\n')
    if Transient == True: Ofile.write('Transient flow\n')
    if Transport == True: Ofile.write('do Transport\n')
    if Echo2Output == True: Ofile.write('Echo to output\n')

    # %% POROUS MEDIA PROPERTIES:
    Ofile.write('\n!--- Porous media properties\n')
    Ofile.write('use domain type\nporous media\nproperties file\n')
    Ofile.write('./%s\n\nclear chosen zones\nchoose zones all\nread properties\nporous medium\n' % (porousfile))

    if len(kfile) > 0:
        Ofile.write('\nclear chosen elements\nchoose elements all\nRead elemental k from file\n./%s\n' % (kfile))

    if Transport == True:
        name = 'fluorescein'
        difCoef = 1.000e-10
        Ofile.write('\n!--- Solute properties\n')
        Ofile.write('solute\n name\n %s\n free-solution diffusion coefficient\n %.3e\nend solute\n' % (name, difCoef))

    Ofile.flush()
    print('<Simulation param and porous media written ok...')

    # %% INITIAL CONDITIONS:
    del banner
    banner = 'clear chosen nodes\nchoose nodes all\n'
    Ofile.write('\n!--- IC\n!--- Flow:\n%s' % banner)

    if heads_init[0] == 'output':
        try:
            Ofile.write('initial head from output file\n')
            Ofile.write('%s\n' % (heads_init[1]))
        except TypeError('Innapropiate argument type in heads'):
            raise
    elif heads_init[0] == 'onevalue':
        Ofile.write('Initial head\n')
        try:
            Ofile.write('%.3e\n' % (heads_init[1]))
        except TypeError('Innapropiate argument type in heads'):
            raise
    elif heads_init[0] == 'raster':
        try:
            Ofile.write('Map initial head from raster\n')
            Ofile.write('%s\n' % (heads_init[1]))
        except TypeError('Innapropiate argument type in heads'):
            raise
    elif heads_init[0] == 'txt':
        try:
            Ofile.write('Initial head from file\n')
            Ofile.write('%s\n' % (heads_init[1]))
        except TypeError('Innapropiate argument type in heads'):
            raise

    if Transport == True:
        if transport_init[0] == 'onevalue':
            Ofile.write('\n!--- Transport:\n%s' % banner)
            Ofile.write('Initial concentration\n%.3e' % transport_init[1])

        elif transport_init[0] == 'output':
            try:
                Ofile.write('Initial concentration from output file\n')
                Ofile.write('%s\n' % (transport_init[1]))
            except TypeError('Innapropiate argument type in transport_init'):
                raise

    Ofile.flush()
    print('<Initial conditions written ok...')

    # %% BOUNDARY CONDITIONS:
    del banner
    banner = 'clear chosen nodes\nuse domain type\nporous media\n'
    Ofile.write('\n\n!--- BC\n!--- Flow:\n%s' % banner)

    if heads_bc[0] == 'all':
        Ofile.write('\nchoose nodes all\n')
    else:
        Ofile.write('\nchoose nodes x plane\n%.8e\n1.e-5\n' % (heads_bc[0]))
        Ofile.write('\nchoose nodes x plane\n%.8e\n1.e-5\n' % (heads_bc[1]))

    Ofile.write('\ncreate node set\nconstant_head_boundary\n')
    Ofile.write('\nBoundary condition\n type\n head equals initial\n node set\n constant_head_boundary\nEnd\n')

    Ofile.flush()
    print('<Boundary conditions written ok...')

    # %% TIME STEP CONTROLS:
    if TimeStepCtrl == True:
        flowsolver_conv_ = 1.0e-6
        flowsolver_max_iter = 500
        # head_control = 0.01
        initial_timestep = 1.0e-5
        min_timestep = 1.0e-9
        max_timestep = 100.0
        max_timestep_mult = 2.0
        min_timestep_mult = 1.0e-2
        underrelax_factor = 0.5

        Ofile.write('\n!--- Timestep controls\n')
        Ofile.write('flow solver convergence criteria\n%.2e\n' % (flowsolver_conv_))
        Ofile.write('flow solver maximum iterations\n%d\n' % (flowsolver_max_iter))
        # Ofile.write('head control\n%.2e\n'%(head_control))
        Ofile.write('initial timestep\n%.2e\n' % (initial_timestep))
        Ofile.write('minimum timestep\n%.2e\n' % (min_timestep))
        Ofile.write('maximum timestep\n%.2e\n' % (max_timestep))
        Ofile.write('maximum timestep multiplier\n%.2e\n' % (max_timestep_mult))
        Ofile.write('minimum timestep multiplier\n%.2e\n' % (min_timestep_mult))
        Ofile.write('underrelaxation factor\n%.2f\n' % (underrelax_factor))
        Ofile.write('No runtime debug\n\n')
        Ofile.flush()
        print('<Default time step controllers written ok...')

    # %% WELL DEFINITION:

    if pumpwellsFile:
        Ofile.write('\n!--- Pumping wells\n\n')
        try:
            wells_data = np.genfromtxt(os.path.join(myInDir, pumpwellsFile), comments="#", delimiter='',
                                       dtype='|S12, f8, f8, f4, f4, f4, f8, f8, f8, f8')
            len(wells_data)
        except (IOError, TypeError):
            print('There was an error with the <wells> file')
            print('File must contain at least two lines, if only one entrance, just duplicate it')
            Ofile.flush()
            Ofile.close()
            raise

        if wells_data[0] == wells_data[1]:
            length_wellsdata = 1
        else:
            length_wellsdata = len(wells_data)

        for ii in range(0, len(wells_data)):
            wellID = wells_data[ii][0]
            X = wells_data[ii][1]
            Y = wells_data[ii][2]
            top = wells_data[ii][3]
            bot = wells_data[ii][4]
            ext_point = wells_data[ii][5]
            pumprate_ini = wells_data[ii][6]
            pumprate_end = wells_data[ii][7]
            time_ini = wells_data[ii][8]
            time_end = wells_data[ii][9]

            Ofile.write('!-Well: %s\nuse domain type\nwell\nproperties file\n%s\n\n' % (wellID, well_propsFile))
            Ofile.write('clear chosen zones\nclear chosen segments\nchoose segments polyline\n2\n')
            Ofile.write('%s, %s, %s ! xyz of top of well\n' % (X, Y, top))
            Ofile.write('%s, %s, %s ! xyz of bottom of well\n' % (X, Y, bot))
            Ofile.write('new zone\n%d\n\n' % (ii + 1))
            Ofile.write('clear chosen zones\nchoose zone number\n%d\n' % (ii + 1))
            Ofile.write(
                'read properties\ntheis well\nclear chosen nodes\nchoose node\n%s, %s, %s\n' % (X, Y, ext_point))
            Ofile.write(
                'create node set\n %s\n\nboundary condition\n  type\n  flux nodal\n\n  name\n  %s\n  node set\n  %s\n' % (
                wellID, wellID, wellID))
            Ofile.write('\n  time value table\n  %f   %f\n  %f   %f\n end\nend\n\n' % (
            time_ini, pumprate_ini, time_end, pumprate_end))

        Ofile.flush()
        print('<Pumping wells written ok...')
    if not pumpwellsFile:
        print('<Pumping wells not included in grok>')

    # %% TRACER INJECTION:
    if Transport == True:
        Ofile.write('\n!--- Tracer injection\n\n')

        try:
            tracer_data = np.genfromtxt(os.path.join(myInDir, tracer_injFile), comments="#", delimiter='',
                                        dtype='f8, f8, f4, f4, f4, f4')
            len(tracer_data)
        except:
            print('File must contain at least two lines, if only one entrance, just duplicate it')
            print('There was an error with the Tracer Injection File')
            Ofile.flush()
            Ofile.close()
            raise
        if tracer_data[0] == tracer_data[1]:
            length_injdata = 1
        else:
            length_injdata = len(tracer_data)

        for ii in range(0, length_injdata):
            X = tracer_data[ii][0]
            Y = tracer_data[ii][1]
            Z = tracer_data[ii][2]
            time_ini = tracer_data[ii][3]
            time_end = tracer_data[ii][4]
            massflux = tracer_data[ii][5]

            Ofile.write(
                'clear chosen zones\nclear chosen segments\nclear chosen nodes\n\nuse domain type\nporous media\n')
            Ofile.write('\nchoose node\n')
            Ofile.write('%s, %s, %s\n' % (X, Y, Z))
            Ofile.write('\nspecified mass flux\n1\n%s,%s,%s\n\n' % (time_ini, time_end, massflux))

        Ofile.flush()
        print('<Tracer injection written ok...')

    # %% OUTPUT TIMES:
    if outputimesFile:
        try:
            times = np.loadtxt(os.path.join(myInDir, outputimesFile))
        except IOError:
            print('Check the directories of the output-times file')
            raise
        Ofile.write('!--- Outputs\noutput times\n')
        for ii in range(0, len(times)):
            Ofile.write('%s\n' % times[ii])

        Ofile.write('end\n' % times[ii])
        Ofile.flush()
        print('<Output times written ok...')
    if not outputimesFile:
        print('<No output times written in grok...>')

        # %% OBSERVATION WELLS:
    if obswellsFile:
        try:
            obs_wellsData = np.genfromtxt(os.path.join(myInDir, obswellsFile), comments="#", delimiter='',
                                          dtype='|S12, f8, f8, f4')
            len(obs_wellsData)
        except (IOError, TypeError, ValueError, OSError):
            print('Check the directories of the grok and output-times files')
            print('observation wells file must contain at least two lines, if only one entrance, just duplicate it')
            raise

        Ofile.write('\n!--- Observation wells\nuse domain type\nporous media\n\n')
        if obs_wellsData[0] == obs_wellsData[1]:
            length_obsData = 1
        else:
            length_obsData = len(obs_wellsData)

        for ii in range(0, length_obsData):
            ID = obs_wellsData[ii][0]
            X = obs_wellsData[ii][1]
            Y = obs_wellsData[ii][2]
            Z = obs_wellsData[ii][3]

            Ofile.write('make observation point\n')
            Ofile.write('%s\n%f  %f  %f\n' % (ID, X, Y, Z))
        Ofile.flush()
        print('<Observation wells written ok...')

    if not obswellsFile:
        print('<No observation wells written in grok...>')

    Ofile.close()
    print('GROK file finished!')


def timeStepUpdate(heads, concentrations, update_heads, update_concentrations, subtract=False):
    """ Add/subtract a time value in the outputtimes_xxx.dat files
    Arguments:
        heads:                  Boolean, add time step to heads or not
        concentrations:         Boolean, add time step to concentrations or not
        update_heads:           Integer, how many time steps should be added to heads
        update_concentrations:  Integer, how many time steps should be added to concentrations
        subtract:               Str, if it is wanted to delete last time step from file (for restart mode). Default False
    Returns:
        Length of the current times being analyzed
    """
    KalmanFilterPath = 'kalmanfilter'

    myCodeDir = dm.myMainDir()
    myPath = os.path.join(myCodeDir, '..', KalmanFilterPath, 'Model_data')

    if heads == True:
        AllTimes = np.loadtxt(os.path.join(myPath, '_outputtimesFlow.dat'))
        len_curTimes = len(np.loadtxt(os.path.join(myPath, '_outputtimesFlowUpdate.dat')))

        if subtract == False:
            len_curTimes += update_heads
            with open(os.path.join(myPath, '_outputtimesFlowUpdate.dat'), 'a') as mytext:
                mytext.write('\n%s' % AllTimes[len_curTimes - 1])  # to correct the zero index from python
            mytext.close()
        elif subtract == True:
            len_curTimes -= update_heads
            w = open(os.path.join(myPath, '_outputtimesFlowUpdate.dat'), 'w')
            for zz in range(0, len_curTimes):
                if zz < len_curTimes - 1:  # -1 is to avoid writting an empty line
                    w.write('%s\n' % AllTimes[zz])
                else:
                    w.write('%s' % AllTimes[zz])
            w.close()

    if concentrations == True:
        AllTimes1 = np.loadtxt(os.path.join(myPath, '_outputtimesTrans.dat'))
        len_curTimes1 = len(np.loadtxt(os.path.join(myPath, '_outputtimesTransUpdate.dat')))

        if subtract == False:
            len_curTimes1 += update_concentrations
            with open(os.path.join(myPath, '_outputtimesTransUpdate.dat'), 'a') as mytext:
                mytext.write('\n%s' % AllTimes1[len_curTimes1 - 1])  # to correct the zero index from python
            mytext.close()
        if subtract == True:
            len_curTimes1 -= update_concentrations
            w = open(os.path.join(myPath, '_outputtimesTransUpdate.dat'), 'w')
            for zz in range(0, len_curTimes1):
                if zz < len_curTimes1 - 1:  # -3 is to avoid writting an empty line
                    w.write('%s\n' % AllTimes1[zz])
                else:
                    w.write('%s' % AllTimes1[zz])
            w.close()

    print('Times updated...')

    if (heads == True) and (concentrations == True):
        return len_curTimes + len_curTimes1
    elif (concentrations == False) and (heads == True):
        return len_curTimes
    elif (concentrations == True) and (heads == False):
        return len_curTimes1


def setParam2value():
    print('construction...')
