""" Load datasets from a hdf5 database and process it with all available functions,
    This might be more useful in Ipython notebook... """

# Define initial settings:
import os, time as timepy                           # routines for file system operation
import numpy as np, platform                  # these are routines for numerical operations
import matplotlib.pyplot as plt     # these are plotting routines
import scipy.stats as st            # statistical routines, like distributions etc...
import scipy.spatial as sp          # spatial routines, like distance calculation
import h5py, pandas as pd, sys
import pycleaner.menu as pyclm
import pycleaner.caller as caller
import pdb

def main():

    #%% Lunch initial menu:
    mytimefile, inputFiles, inputsource, skiprow, columns2use = pyclm.MainMenu()
    logstore = pyclm.check_userinput('Store transformation process in a single log file?', myoptions = ['yes','y','no','n'])

    #%% Read source and target databases:
    if inputsource == 'txt':
        #loginfo=()
        for ii in inputFiles:
            time = timepy.strftime('%d-%m-%Y %H:%M:%S') # Get the date-time of logging
            loginfo = 30*'-' + '\n\nLog info for: ' + os.path.join(os.path.split(os.path.split(ii)[0])[-1], os.path.split(ii)[-1]) + '\nDate: ' + time + '\nComputer: %s\n' %platform.node() + 30*'-' + '\n' # Get the file that we are working with
            #logfile=open('logfile.txt','a')
            #logfile.write(ii+'\n'+time+'\n')
            #logfile.close()
            if len(mytimefile) > 0:
                orig_time = np.loadtxt(mytimefile)
                orig_value = np.loadtxt(ii)

            elif len(mytimefile) == 0:
                #pdb.set_trace()
                try:
                    orig_time, orig_value = np.loadtxt(ii, usecols = (columns2use[0], columns2use[1]), skiprows = skiprow, unpack = True)
                except:
                    data = pd.read_table(ii, header = 0)
                    orig_time, orig_value = data.time_sec.values, data.myhead.values
                ##orig_time = np.arange(0,len(orig_value)*0.4,0.4)

            joint_dataset, recordDevice, loginfo = caller.procData(orig_time, orig_value, os.path.split(ii)[-1], loginfo, os.path.split(ii)[0])

            if recordDevice == 0:
                sys.exit('Process exited')

            if os.path.exists(os.path.join(ii.split('.')[0] + '_modif.txt' )):
                    overwrite = input('File exists, overwrite? (y/n)')

            if (not os.path.exists(os.path.join(ii.split('.')[0] + '_modif.txt' )) ) or (overwrite == 'y'):
                # Store log info either in a single or individual files:
                if 'n' in logstore:
                    mylogpath = os.path.join(os.path.split(ii)[0], '_log_' + os.path.split(ii)[1] + '.txt')
                    writemode = 'w'
                elif 'y' in logstore:
                    mylogpath = os.path.join(os.path.split(ii)[0], '_log_full_' + '.txt')
                    writemode = 'a'

                logfile = open(mylogpath, writemode)
                logfile.write(loginfo)
                logfile.flush()
                logfile.close()
                np.savetxt(os.path.join(ii.split('.')[0] + '_modif.txt' ), joint_dataset, fmt = '%.16e')
            else:
                print ('Changes not saved')
                pass

    elif inputsource == 'hdf5':
        outputDatabase = '%s_processed.%s' %(inputFiles.split('.')[0],inputFiles.split('.')[1])
        fIn = h5py.File(inputFiles, 'r')
        fout = h5py.File(outputDatabase, 'a') # 'a': Open read-write (create if doesn't exist)

        #%% Define the strings of interest
        # (for an efficient loop over the elements within the list)
        # Always define two elements in the list
        #str1 = ['txt', 'Time[date format]']         # str1 = ['txt', 'Time[date format]']; str1 = ['Channel', 'b_Time']
        str1 = str(input('First string to filter entries \n(e.g. txt, Channel-> those containing Y values):'))
        str2 =str(input('Second string to filter entries \n(e.g. Time[date format], b_Time):'))
        str1 = [str1, str2]

        testtype = pyclm.check_userinput('Type of test? (pt, st, tt)', myoptions = ['pt', 'st', 'tt'])
        Nodevice, device, typeData = pyclm.testMenu(level = testtype)

        listofNames = []
        fIn.visit(listofNames.append)
        for cur_str in listofNames:
            print (cur_str)

        #%% Separate groups and subgroups from datasets:
        datasetlist = []
        groupslist = []
        Datelist = []

        for cur_str in listofNames:

            if str1[0] in cur_str:
                datasetlist.append(cur_str)
            elif (str1[0] not in cur_str) and (str1[1] not in cur_str) and ('/' in cur_str):
                groupslist.append(cur_str)
                print ('GROUP:', cur_str)
            elif str1[1] in cur_str:
                Datelist.append(cur_str)

        groupslist = np.array(groupslist, dtype = 'str')
        datasetlist = np.array(datasetlist, dtype = 'str')
        Datelist = np.array(Datelist, dtype = 'str')

        #%% Initialize databases:

        # Creation of same groups and their dependencies in the new database for processed data,
        # Start reading datasets:
        # This has to be redefined every time depending on the structure of the database to work with:
        for cur_dataset in datasetlist:
        ##cur_dataset = datasetlist[0]
            sepstrgs = cur_dataset.split('/')
            assert len(sepstrgs) == 3, 'The code only supports three levels on the database'
            group_string = sepstrgs[0]+'/'+ sepstrgs[1]
            atts_group = fIn[group_string].attrs.items()

            try:
                fout.create_group(group_string)
                for xx in range(0,len(atts_group),1):
                    fout[group_string].attrs[atts_group[xx][0]] = atts_group[xx][1]
            except:
                print ('Group exists...')
                pass
            fout.flush()

            dataset = np.array(fIn[cur_dataset])
            assert dataset.shape[1] == 2 or dataset.shape[1] == 4, 'The code only supports either two or four columns in the dataset'

            if (typeData == 'heads') or all(typeData == 'concentration' and device != '19channel'):
                assert dataset.shape[1] == 2, 'Data should be in two columns only (time, heads)'
                orig_time = dataset[:,0]
                orig_value = dataset[:,1]

            elif (typeData == 'concentration') and (device == '19channel'):
                assert dataset.shape[1] == 4, 'Data should be in four columns only (Gain, Signal, Result, Conc)'
                for yy in Datelist:
                    if group_string in yy:
                        orig_time = np.array(fIn[yy])
                    pass

                orig_value = dataset[:,2]

            # Here all the functions are called within a while loop
            joint_dataset, recordDevice = caller(orig_time, orig_value, cur_dataset)
            ########################################################################

            if recordDevice == 0:
                fout.flush()
                fout.close()
                fIn.close()
                with open('Data_processDiary.txt', "a") as text_file:
                    text_file.write("\nDate: %s, Input database: %s, Dataset: %s" %(inputFiles,timepy.strftime("%c"), cur_dataset))
                sys.exit('Process exited')

            try:
                # Create dataset:
                fout[group_string].create_dataset(sepstrgs[-1], data = joint_dataset, compression="gzip")
                # Add metadata:
                atts_dataset = fIn[group_string + '/' + sepstrgs[-1]].attrs.items()

                for zz in range(0,len(atts_dataset),1):
                    fout[group_string + '/' + sepstrgs[-1]].attrs[atts_dataset[zz][0]] = atts_dataset[zz][1]
                fout[group_string + '/' + sepstrgs[-1]].attrs['State'] = 'Processed'
                print ('Dataset %s has been fully processed and stored' %(cur_dataset))

            except:
                is_valid = 0
                while not is_valid :
                    Issue = str(input('Dataset exists. Replace it? (yes,no):'))
                    if (Issue == 'no') or (Issue == 'n') or (Issue == 'yes') or (Issue == 'y'):
                        is_valid = 1 # set it to 1 to validate input and to terminate the while..not loop
                    else:
                        print ('%s is not a valid answer!'%(Issue))
                if Issue == 'yes' or Issue == 'y':
                    del fout[group_string + '/' + sepstrgs[-1]]
                    fout.flush()
                    fout[group_string].create_dataset(sepstrgs[-1], data = joint_dataset, compression="gzip")
                elif Issue == 'no' or Issue == 'n':
                    print ('Last modifications not saved, going to the next dataset...')

            fout.flush()
        fout.close()
        fIn.close()

if __name__ == '__main__':
    main()
