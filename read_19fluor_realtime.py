"""
Created on Mon Dec  1 19:16:38 2014
Scrip for real-ime plotting of the 19-channel fluorometer
@author: emilio
"""
import sys, os, numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

#%% Define file name and directory
check = False
while check is False:
    myFile = str((input('Type full directory: ')))
    if os.path.isfile(myFile) is True:
        check = True
        FileName = os.path.split(myFile)[-1]
    else:
        print('Wrong directory, try again!')

pickdefault = str(input('Pick default properties? (y/n)'))

#%% Type of plot: all in one, or each LED channel in one individual graph
#%% Define some additional settings: This code can plot a maximum of two different values (y axes)
if pickdefault == 'n':
    myplot = str(input('Type of plot. \n(1) All-in-one, (2) Individual\nDeafult = 1: '))
    plotinterval = str(input('Plot interval in mseconds. \nDefault = 10000: '))
    cols2plot = str(input('Column to plot?\n[Gain,Signal,Result,Conc]\nDefault = Conc: '))
    # ['Gain', 'Signal', 'Result', 'Conc'] result = (Signal/Gain)
    xlim1 = str(input('X axis ini value?\nDefault = 0: '))
    ylim1 = str(input('Y axis ini value?\nDefault = -10: '))
    ylim2 = str(input('Y axis end value?\nDefault = 4500: '))
    time_freq = str(input('Recording time interval?\nDefault = 1: '))
elif pickdefault == 'y':
    myplot = ''
    plotinterval = ''
    cols2plot = ''
    xlim1 = ''
    ylim1 = ''
    ylim2 = ''
    time_freq = ''

if (myplot == '') or (pickdefault == 'y'):
    myplot = '1'
if (plotinterval == '') or (pickdefault == 'y'):
    plotinterval = 10000
else:
    plotinterval = plotinterval.astype(int)

if (cols2plot == '') or (pickdefault == 'y'):
    cols2plot = 'Conc'

if (xlim1 == '') or (pickdefault == 'y') :
    xlim1 = 0000
else:
    xlim1 = int(xlim1)

if (ylim1 == '') or (pickdefault == 'y') :
    ylim1 = -10.
else:
    ylim1 = float(ylim1)

if (ylim2 == '') or (pickdefault == 'y') :
    ylim2 = 4500.
else:
    ylim2 = float(ylim2)

if (time_freq == '') or (pickdefault == 'y') :
    time_freq = 1.
else:
    time_freq = float(time_freq) #(f = n/period) (Hz)


ylim = [ylim1, ylim2]#[-500, 5000]
#ylim2 = [500, 800]#[-50, 500]

################################################################################
# Do not modify lines below if you do not know what you are doing!
################################################################################

if myplot == '2':
    # Create the plots with corresponding twin axes:
    fig, axs = plt.subplots(4,5, facecolor='w', edgecolor='k') # 20 different graphs
    fig.subplots_adjust(hspace = .3, wspace=0.4)
    plt.suptitle(FileName)
    axs = axs.ravel()

elif myplot == '1':
    mymarkers = ['kd', 'rd', 'gd', 'bd', 'cd','k.', 'r.', 'g.', 'b.', 'c.', 'ks', 'rs', 'gs', 'bs', 'cs', 'k^', 'r^', 'g^', 'b^', 'c^']
    fig, axs = plt.subplots(1,1, facecolor='w', edgecolor='k') # 20 different graphs
    plt.suptitle(FileName)
    #axs = axs.ravel()
    #axstwin = axs.twinx()

def animate(i):
    """ Real time plotting for the 19-channel fluorometer
    Arguments:
        i: Int, interval (time lapse) for plot updating [miliseconds]
    Returns:
        graph with the real-time measurements of 19-channel fluorometer
    """

    # Settings (they are fixed, so ideally should not be changed):
    No_channels = 20
    Col_perChannel = 4
    Col_values = np.array(['Gain', 'Signal', 'Result', 'Conc'])
    datasetcols = np.arange(3,No_channels*Col_perChannel + 3,1)
    flag = 'readagain'

    mytol = 0
    while flag == 'readagain':

        try:
            #date, hour, time = np.loadtxt(myFile, usecols = (0,1,2), dtype = 'str', unpack = True) If I am to make it efficient, this is not needed!
            dataset = np.loadtxt(myFile, usecols = (datasetcols), dtype = 'float')
            flag = 'readsuccess'
        except IOError:
            raise Exception ('Incorrect filename and/or directory!')
        except KeyboardInterrupt:
            raise
        except:
            print('Something went wrong, reading file again...')
            pass

        if flag == 'readsuccess':
            # Store whole file in a dictionary:
            mydict = {}

            counter = 0
            for jj in range(0, No_channels,1):

                for ii in range(0,len(Col_values),1):

                    temp1 = Col_values[ii] + '_ch_%.2d'%(jj+1)
                    mydict[temp1] = dataset[:,counter]
                    counter = counter + 1

            time =np.arange(0, len(dataset), time_freq)

            # Start plotting stuff:
            try:
                if myplot == '2':
                    for curFig in range(0,20):

                        axs[curFig].clear()
                        minusnoise = mydict['%s_ch_%.2d' %(cols2plot, curFig+1)] - mydict['%s_ch_20' %(cols2plot)]
                        axs[curFig].plot(time,minusnoise, 'ro', markersize = 2, mew = 0.09)
                        #axs[curFig].plot(time,mydict['%s_ch_%.2d' %(cols2plot[0], curFig+1)], 'ro', markersize = 2, mew = 0.09)
                        if curFig < 19:
                            axs[curFig].set_title('LED %.2d'%(curFig+1))
                        elif curFig == 19:
                            axs[curFig].set_title('LED: Ambient')
                        axs[curFig].set_xlabel('Time[s]', fontsize = 7)
                        axs[curFig].set_ylabel('%s [mV???]'%(cols2plot), fontsize = 7)
                        axs[curFig].yaxis.label.set_color('red')
                        axs[curFig].tick_params(axis = 'y', colors = 'red', labelsize = 7)
                        axs[curFig].tick_params(axis = 'x', labelsize = 7)
                        axs[curFig].grid(True)
                        #axs[curFig].set_ylim(ylim)


                elif myplot == '1':
                    axs.clear()
                    for curFig in range(0,20):
                        if curFig < 19:
                            axs.plot(time,mydict['%s_ch_%.2d' %(cols2plot,curFig+1)]-mydict['%s_ch_20' %(cols2plot)], mymarkers[curFig], label = 'LED %.2d'%(curFig+1),  markersize = 3, mew = 0.09)
                        elif curFig == 19:
                            axs.plot(time,mydict['%s_ch_%.2d' %(cols2plot, curFig+1)], mymarkers[curFig], label = 'Ambient',  markersize = 3, mew = 0.09)

                    axs.set_xlabel('Time[s]', fontsize = 7)
                    axs.set_ylabel('%s [mV???]'%(cols2plot), fontsize = 7)
                    axs.yaxis.label.set_color('red')
                    axs.tick_params(axis = 'y', colors = 'red', labelsize = 7)
                    axs.tick_params(axis = 'x', labelsize = 7)
                    axs.grid(True)
                    axs.set_ylim(ylim)
                    axs.set_xlim([xlim1,time[-1]])
                    axs.legend(loc = 'best', numpoints = 1 )
                flag = 'done'

            except KeyboardInterrupt:
                raise
            except:
                mytol = mytol +1
                assert mytol < 20, 'Too many attempts to read the file, something is wrong with the code or file location'

                print('Reading file again (trial %s)...'%mytol)
                flag = 'readagain'
                pass

# Call the animation command for a constant plotting
##animate(1)
ani = animation.FuncAnimation(fig, animate, interval = plotinterval  )
figManager = plt.get_current_fig_manager()
figManager.window.showMaximized()
plt.show()