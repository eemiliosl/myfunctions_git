function [head,t] = ReadHeadOlf(filename,n)

%filename = 'smodel_1o.head_olf.720';

fid = fopen(filename,'rb');

if fid < 0
    error('Could not open file: ',filename);
end

pad=fread(fid, 4, 'uchar'); % first header
time=fread(fid, 80, 'uchar');
t = str2num(char(time)');
pad=fread(fid, 4, 'uchar'); 
pad = fread(fid,4,'uchar'); % starting real*8
head = fread(fid,n,'real*8');
pad = fread(fid,4,'uchar'); % ending real*8
% store = head(end);
% head(2:end) = head(1:end-1);
% head(1) = store;

fclose(fid);
