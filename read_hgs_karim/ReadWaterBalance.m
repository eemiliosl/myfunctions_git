function [t,precip,infilt , exfilt ,et]= ReadWaterBalance( filename )
%UNTITLED9 Summary of this function goes here
%   Detailed explanation goes here
infilt = [];
exfilt = [];
et = [];
t = [];
precip = [];
%filename = 'd:\\regulars\\smodel_0o.water_balance.dat';
fid = fopen(filename,'rt');
if fid < 0
    error(strcat('could not open file:  ',filename));
end
fgets(fid);fgets(fid);fgets(fid);  % skip 3 header lines.
while ~ feof(fid)
    tline = fgets(fid);
    tokens = tokenize(tline,' ');
    t = [t tokens(1)];
    et = [et tokens(end)];
    exfilt = [exfilt tokens(end - 4)];
    infilt = [infilt tokens(end-5)];
    precip = [precip tokens(end-13)];
end
fclose(fid); % close the file 

function tokens = tokenize(text,delimeter)
tokens = [];
while length(text) > 0
    [token,text] = strtok(text,delimeter);
    tokens = [tokens, str2num(token)];
end

return 

