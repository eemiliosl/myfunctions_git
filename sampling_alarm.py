# -*- coding: utf-8 -*-
"""
Created on Mon Apr 18 15:52:07 2016
@author: admin
"""
import pandas as pd
import msvcrt
import datetime
import winsound
import numpy as np
print('For a better performance, run it from the command line.....')
print('Hit any key to stop execution')


path = r'C:\PhD_Emilio\Emilio_PhD\Field_Data\TracerSampling_042016_b.xls'

Freq = 2500     # Set Frequency To 2500 Hertz
Dur = 1000      # Set Duration To 1000 ms == 1 second
safety_seconds = 60.
go = True

idsx_done = np.array([])
idsy_done = np.array([])
pumpCh = pd.read_excel(path, sheetname="XX.XX.XX", header=1, skiprows=3, skip_footer=26, parse_cols="E")
while True:
    try:
        table = pd.read_excel(path, sheetname="XX.XX.XX", header=3, skiprows=2, skip_footer=26, parse_cols="Q:X")
    except (FileNotFoundError, PermissionError):
        pass

    now = datetime.datetime.now()
    now_safe = datetime.datetime.now() + datetime.timedelta(seconds=safety_seconds)

    for ii in range(0, table.shape[1]):
        for jj in range(0, table.shape[0]):

            try:
                if table.iloc[jj, ii] == datetime.time(0, 0):
                    pass
                else:
                    if now_safe >= datetime.datetime.combine(datetime.date.today(), table.iloc[jj, ii]):
                        if len(idsx_done) == 0:
                            idsx_done = np.array([jj])
                            idsy_done = np.array([ii])
                            winsound.Beep(Freq, Dur)
                            print('Time to sample LED %d: %s' % (jj + 1, table.iloc[jj, ii]))
                            print('-----> Sample number %d, Pump-ch %s' % (ii + 1, pumpCh.iloc[jj + 1, 0]))

                        elif len(idsx_done) > 0:
                            check = np.zeros(len(idsx_done))    # array to see which are already sampled
                            for nn in range(len(idsx_done)):
                                check[nn] = np.any(idsx_done[nn] == jj and idsy_done[nn] == ii)
                            if np.any(check):
                                pass
                            else:

                                idsx_done = np.hstack([idsx_done, jj])
                                idsy_done = np.hstack([idsy_done, ii])
                                winsound.Beep(Freq, Dur)
                                print('Time to sample LED %d: %s' % (jj + 1, table.iloc[jj, ii]))
                                print('-----> Sample number %d, Pump-ch %s' % (ii + 1, pumpCh.iloc[jj + 1, 0]))

            except:
                pass
    # stopping the loop
    if msvcrt.kbhit():
        break
