# -*- coding: utf-8 -*-
"""
Created on Sun Jun 21 21:01:19 2015
@author: emilio
"""
import numpy as np
import time
import scipy
import multiprocessing as mp
import os
import hgs_grok as hgr
import grid_manip as gm
import gen_fields as gf


def GenReal(str2pass):
    """ Run realizations with the possibility of doing it in parallel """
    # Create variables with the proper part of the string
    Str2Assign, homedir, mode, Ini_ensemble, genfields, grid_filedir, initial_head, mytype = list2var(str2pass)

    # Read field and mesh parameters:
    xlen, ylen, zlen, nxel, nyel, nzel, xlim, ylim, zlim, Y_model, Y_var, beta_Y, lambda_x, lambda_y, lambda_z =\
        gf.readParam(os.path.join(homedir, 'Model_data', '_RandFieldsParameters.dat'))

    # %%
    try:
        NoReal = np.asarray(Str2Assign).astype('int') - 1
    except:
        NoReal = np.asarray(Str2Assign).astype('str')

    # %% Some headers for multiprocessing module:
    scipy.random.seed()
    time.sleep(np.random.uniform(low=1.0, high=1.5, size=1))  # Process_created = mp.Process()
    Process_current = mp.current_process()
    print('Process No. %s started by %s...' % (Str2Assign, Process_current.name))

    # %%  mesh data:
    meastimes = '_outputtimes%s.dat' % mode
    modeltimes = np.loadtxt(os.path.join(homedir, 'Model_data', meastimes))
    nnodes, elements = gm.readmesh(grid_filedir, fulloutput=False)

    if mode == 'fl_':
        biggrid_elem_ids = np.arange(1, elements + 1, 1)
        smallgrid_nodes, smallgrid_elem = gm.sel_grid(grid_filedir, xlim, ylim, zlim)

    elif mode == 'tr_':
        biggrid_elem_ids = ''
        smallgrid_elem = ''
        smallgrid_nodes = ''
    # smallgrid_elem = biggrid_elem_ids # This has to be fixed for the transport case!

    # %% Run function to get states:
    modmeas = hgr.getstates(homedir, NoReal, mode, nnodes, elements, modeltimes, biggrid_elem_ids, smallgrid_elem,
                            inner_gr_nodes=smallgrid_nodes, Y_i_upd='', curtimestep=0, genfield=genfields, plot=False,
                            storebinary=True, mytype=mytype, initial_head=np.float(initial_head))

    print('Exiting:', Process_current.name)
    # If initialization of ensemble, return only the index
    if Ini_ensemble is True:
        return Str2Assign
    # If not initialization of ensemble, return also model outputs and parameters
    elif Ini_ensemble is False:
        return Str2Assign, modmeas


def list2var(str2pass):
    """
    Change a composite list to several variables using '-' as a symbol to
    separate the strings...
    """

    Str2Assign = str2pass.split('-')[0]
    homedir = str2pass.split('-')[1]
    mode = str2pass.split('-')[2]
    Ini_ensemble = bool(str2pass.split('-')[3])
    genfields = bool(str2pass.split('-')[4])
    grid_filedir = str2pass.split('-')[5]
    initial_head = str2pass.split('-')[6]
    mytype = str2pass.split('-')[7]

    return Str2Assign, homedir, mode, Ini_ensemble, genfields, grid_filedir, initial_head, mytype
